package dgapp.baggaarm.login;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.TextView;

import dgapp.baggaarm.R;
import dgapp.baggaarm.utils.BaseActivity;

public class LoginActivityBindingControls extends BaseActivity {

    protected Button mSignInButton;
    protected Button mSignUpButton;
    protected TextView mTandC;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        bindingControls();
    }

    void bindingControls() {
        mSignInButton = (Button) findViewById(R.id.btn_sign_in);
        mSignUpButton = (Button) findViewById(R.id.btn_sign_up);
        mTandC = (TextView) findViewById(R.id.tv_tandc);
    }
}
