package dgapp.baggaarm.login.signin.forgot_password;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import dgapp.baggaarm.R;
import dgapp.baggaarm.service.AuthenticationService;
import dgapp.baggaarm.utils.BaseActivity;
import dgapp.baggaarm.utils.Utils;

public class ForgotPasswordActivity extends BaseActivity {

    EditText mEmail;
    Button mSend;

    AuthenticationService mService;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        mService = AuthenticationService.getInstance(getApplicationContext());

        bindingControls();
        setupControlEvents();
    }

    void bindingControls() {
        mEmail = (EditText) findViewById(R.id.edt_email);
        mSend = (Button) findViewById(R.id.btn_send);
    }

    void setupControlEvents() {
        mEmail.addTextChangedListener(textWatcher);

        mSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String email = mEmail.getText().toString();

                closeKeyboard();
                showLoading();
                mService.resetPassword(email, new AuthenticationService.AuthenticationCallback() {
                    @Override
                    public void onCompleted() {
                        ForgotPasswordActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideLoading();

                                String title = getString(R.string.button_notice);
                                String message = getString(R.string.forgot_password_sent);

                                final AlertDialog dialog = new AlertDialog.Builder(ForgotPasswordActivity.this)
                                        .setTitle(title)
                                        .setMessage(message)
                                        .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();
                                                Intent intent = new Intent();
                                                intent.putExtra("email", email);
                                                setResult(RESULT_OK, intent);
                                                finish();
                                            }
                                        }).create();
                                if (!dialog.isShowing()) {
                                    dialog.show();
                                }
                            }
                        });
                    }

                    @Override
                    public void onFail(final String message) {
                        ForgotPasswordActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideLoading();
                                String errorMessage = null;
                                if (message.equals(AuthenticationService.ERROR_WEAK_PASSWORD))
                                    errorMessage = getString(R.string.sign_in_error_week_password);
                                else if (message.equals(AuthenticationService.ERROR_INVALID_EMAIL))
                                    errorMessage = getString(R.string.sign_in_error_email_invalid);
                                else if (message.equals(AuthenticationService.ERROR_USER_NOT_FOUND))
                                    errorMessage = getString(R.string.sign_in_error_not_exist);
                                else if (message.equals(AuthenticationService.ERROR_NETWORK))
                                    errorMessage = getString(R.string.sign_in_error_network);
                                else if (message.equals(AuthenticationService.ERROR_WRONG_PASSWORD))
                                    errorMessage = getString(R.string.sign_in_error_wrong_password);
                                else if (message.equals(AuthenticationService.ERROR_EMAIL_ALREADY_IN_USE))
                                    errorMessage = getString(R.string.sign_in_error_already_in_use);

                                showErrorDialog(errorMessage);
                            }
                        });
                    }
                });
            }
        });
    }

    private void closeKeyboard() {
        Utils.closeKeyboard(ForgotPasswordActivity.this, mEmail.getWindowToken());
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            String mail = mEmail.getText().toString();

            if (mail.isEmpty()) {
                mSend.setEnabled(false);
                return;
            }

            mSend.setEnabled(true);
        }
    };
}
