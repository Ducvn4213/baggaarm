package dgapp.baggaarm.login.signin.unverified;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.Button;
import android.widget.TextView;

import dgapp.baggaarm.R;
import dgapp.baggaarm.utils.BaseActivity;

public class UnverifiedActivityBindingControl extends BaseActivity {

    protected TextView mTitle;
    protected Button mDidVerify;
    protected Button mResend;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reverify);

        bindingControls();
    }

    void bindingControls() {
        mTitle = (TextView) findViewById(R.id.tv_title);
        mDidVerify = (Button) findViewById(R.id.btn_did_verify);
        mResend = (Button) findViewById(R.id.btn_resend);
    }
}
