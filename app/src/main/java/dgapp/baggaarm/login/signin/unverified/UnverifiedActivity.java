package dgapp.baggaarm.login.signin.unverified;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import dgapp.baggaarm.R;
import dgapp.baggaarm.service.AuthenticationService;

public class UnverifiedActivity extends UnverifiedActivityBindingControl {

    AuthenticationService mService;

    String mEmail;
    String mPassword;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mService = AuthenticationService.getInstance(getApplicationContext());

        setupControlEvents();
        init();
    }

    void setupControlEvents() {
        mResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLoading();
                mService.resendVerifyEmail(mEmail, mPassword, new AuthenticationService.AuthenticationCallback() {
                    @Override
                    public void onCompleted() {
                        UnverifiedActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideLoading();
                                String title = getString(R.string.button_notice);
                                String message = getString(R.string.verify_resend);
                                showDialog(title, message);
                            }
                        });
                    }

                    @Override
                    public void onFail(String error) {
                        UnverifiedActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideLoading();
                                //TODO
                            }
                        });
                    }
                });
            }
        });

        mDidVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLoading();
                mService.checkVerified(mEmail, mPassword, new AuthenticationService.AuthenticationCallback() {
                    @Override
                    public void onCompleted() {
                        UnverifiedActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideLoading();
                                setResult(RESULT_OK, null);
                                finish();
                            }
                        });
                    }

                    @Override
                    public void onFail(final String message) {
                        UnverifiedActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideLoading();
                                String errorMessage = null;
                                if (message.equals(AuthenticationService.ERROR_WEAK_PASSWORD))
                                    errorMessage = getString(R.string.sign_in_error_week_password);
                                else if (message.equals(AuthenticationService.ERROR_INVALID_EMAIL))
                                    errorMessage = getString(R.string.sign_in_error_email_invalid);
                                else if (message.equals(AuthenticationService.ERROR_USER_NOT_FOUND))
                                    errorMessage = getString(R.string.sign_in_error_not_exist);
                                else if (message.equals(AuthenticationService.ERROR_NETWORK))
                                    errorMessage = getString(R.string.sign_in_error_network);
                                else if (message.equals(AuthenticationService.ERROR_WRONG_PASSWORD))
                                    errorMessage = getString(R.string.sign_in_error_wrong_password);
                                else if (message.equals(AuthenticationService.ERROR_EMAIL_ALREADY_IN_USE))
                                    errorMessage = getString(R.string.sign_in_error_already_in_use);
                                else if (message.equals(AuthenticationService.UNVERIFIED))
                                    errorMessage = getString(R.string.sign_in_error_unverified);

                                showErrorDialog(errorMessage);
                            }
                        });
                    }
                });
            }
        });
    }

    void init() {
        mEmail = getIntent().getStringExtra("email");
        mPassword = getIntent().getStringExtra("password");

        String titleFormat = getString(R.string.re_verify_title_1);
        String title = String.format(titleFormat, mEmail);

        mTitle.setText(title);
    }
}
