package dgapp.baggaarm.login.signin;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import dgapp.baggaarm.R;
import dgapp.baggaarm.login.signin.forgot_password.ForgotPasswordActivity;
import dgapp.baggaarm.login.signin.unverified.UnverifiedActivity;
import dgapp.baggaarm.service.AuthenticationService;
import dgapp.baggaarm.service.callback.CommonCallback;
import dgapp.baggaarm.utils.Utils;

public class SignInActivity extends SignInActivityBindingControl {

    private final int FORGOT_PASSWORD_REQUEST_CODE = 1111;
    private final int UNVERIFIED_REQUEST_CODE = 2222;

    AuthenticationService mService;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mService = AuthenticationService.getInstance(getApplicationContext());

        setupControlEvents();
    }

    void setupControlEvents() {
        mForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignInActivity.this, ForgotPasswordActivity.class);
                startActivityForResult(intent, FORGOT_PASSWORD_REQUEST_CODE);
            }
        });

        mSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeKeyboard();
                handleSignin();
            }
        });

        mEmail.addTextChangedListener(textWatcher);
        mPassword.addTextChangedListener(textWatcher);
    }

    private void closeKeyboard() {
        Utils.closeKeyboard(SignInActivity.this, mEmail.getWindowToken());
        Utils.closeKeyboard(SignInActivity.this, mPassword.getWindowToken());
    }

    void handleSignin() {
        final String email = mEmail.getText().toString();
        final String pass = mPassword.getText().toString();
        showLoading();
        mService.login(email, pass, new AuthenticationService.AuthenticationCallback() {
            @Override
            public void onCompleted() {
                SignInActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        setResult(RESULT_OK, null);
                        finish();
                    }
                });
            }

            @Override
            public void onFail(final String message) {
                SignInActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        String errorMessage = null;

                        if (message.equals(AuthenticationService.UNVERIFIED)) {
                            errorMessage = getString(R.string.sign_in_error_unverified);

                            final AlertDialog dialog = new AlertDialog.Builder(SignInActivity.this)
                                    .setTitle(R.string.button_notice)
                                    .setMessage(errorMessage)
                                    .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            Intent intent = new Intent(SignInActivity.this, UnverifiedActivity.class);
                                            intent.putExtra("email", email);
                                            intent.putExtra("password", pass);
                                            startActivityForResult(intent, UNVERIFIED_REQUEST_CODE);
                                            dialogInterface.dismiss();
                                        }
                                    }).create();
                            if (!dialog.isShowing()) {
                                dialog.show();
                            }
                            return;
                        }

                        if (message.equals(AuthenticationService.ERROR_WEAK_PASSWORD))
                            errorMessage = getString(R.string.sign_in_error_week_password);
                        else if (message.equals(AuthenticationService.ERROR_INVALID_EMAIL))
                            errorMessage = getString(R.string.sign_in_error_email_invalid);
                        else if (message.equals(AuthenticationService.ERROR_USER_NOT_FOUND))
                            errorMessage = getString(R.string.sign_in_error_not_exist);
                        else if (message.equals(AuthenticationService.ERROR_NETWORK))
                            errorMessage = getString(R.string.sign_in_error_network);
                        else if (message.equals(AuthenticationService.ERROR_WRONG_PASSWORD))
                            errorMessage = getString(R.string.sign_in_error_wrong_password);
                        else if (message.equals(AuthenticationService.ERROR_EMAIL_ALREADY_IN_USE))
                            errorMessage = getString(R.string.sign_in_error_already_in_use);
                        else if (message.equals(AuthenticationService.BLOCKED))
                            errorMessage = getString(R.string.sign_in_blocked);

                        showErrorDialog(errorMessage);
                    }
                });
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == FORGOT_PASSWORD_REQUEST_CODE && resultCode == RESULT_OK) {
            String email = data.getStringExtra("email");
            mEmail.setText(email);
            mPassword.setText("");
        }

        if (requestCode == UNVERIFIED_REQUEST_CODE && resultCode == RESULT_OK) {
            setResult(RESULT_OK, null);
            finish();
        }
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            String fname = mEmail.getText().toString();
            String lname = mPassword.getText().toString();

            if (fname.isEmpty() || lname.isEmpty()) {
                mSignIn.setEnabled(false);
                return;
            }

            mSignIn.setEnabled(true);
        }
    };
}
