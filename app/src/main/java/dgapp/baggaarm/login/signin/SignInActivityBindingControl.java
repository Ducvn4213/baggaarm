package dgapp.baggaarm.login.signin;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;

import dgapp.baggaarm.R;
import dgapp.baggaarm.utils.BaseActivity;

public class SignInActivityBindingControl extends BaseActivity {

    protected EditText mEmail;
    protected EditText mPassword;
    protected Button mForgotPassword;
    protected Button mSignIn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        bindingControls();
    }

    void bindingControls() {
        mEmail = (EditText) findViewById(R.id.edt_email);
        mPassword = (EditText) findViewById(R.id.edt_password);
        mForgotPassword = (Button) findViewById(R.id.btn_forgot_password);
        mSignIn = (Button) findViewById(R.id.btn_sign_in);
    }
}
