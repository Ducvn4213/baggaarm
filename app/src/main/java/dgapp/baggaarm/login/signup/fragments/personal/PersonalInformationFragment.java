package dgapp.baggaarm.login.signup.fragments.personal;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import dgapp.baggaarm.R;
import dgapp.baggaarm.login.signup.fragments.SignUpBaseFragment;
import dgapp.baggaarm.utils.GenderPicker;

public class PersonalInformationFragment extends SignUpBaseFragment implements DatePickerDialog.OnDateSetListener {

    Button mChooseBirthDay;
    Button mContinue;
    TextView mTitle;
    EditText mBirthDay;
    GenderPicker mGender;

    String mLastName;

    Calendar myCalendar = Calendar.getInstance();

    GenderPicker.GENDER gender = GenderPicker.GENDER.none;
    String birthDay = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_up_personal_information, container, false);

        bindingControls(view);
        setupControlEvents();

        myCalendar.set(Calendar.YEAR, 1990);
        myCalendar.set(Calendar.MONTH, 0);
        myCalendar.set(Calendar.DAY_OF_MONTH, 1);

        return view;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        myCalendar.set(Calendar.YEAR, year);
        myCalendar.set(Calendar.MONTH, month);
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        String myFormat = getString(R.string.date_format);
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);

        birthDay = sdf.format(myCalendar.getTime());
        String date = birthDay;
        date = date.replace("-20", " 20");
        date = date.replace("-19", " 19");
        date = date.replace("-", " tháng ");

        mBirthDay.setText(date);
        checkUI();
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible) {
            init();
        }
    }

    void bindingControls(View view) {
        mContinue = (Button) view.findViewById(R.id.btn_continue);
        mChooseBirthDay = (Button) view.findViewById(R.id.btn_choose_birth_day);
        mBirthDay = (EditText) view.findViewById(R.id.edt_birth_day);
        mTitle = (TextView) view.findViewById(R.id.tv_title);
        mGender = (GenderPicker) view.findViewById(R.id.gp_gender);
    }

    void setupControlEvents() {
        mChooseBirthDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(getContext(), PersonalInformationFragment.this, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        mGender.setOnGenderChangeListener(new GenderPicker.OnGenderChangeListener() {
            @Override
            public void onGenderChange(GenderPicker.GENDER gender) {
                PersonalInformationFragment.this.gender = gender;
                checkUI();
            }
        });

        mContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getParentActivity().setPersonalInfo(gender, birthDay);

                onNext();
            }
        });
    }

    void init() {
        mLastName = getParentActivity().dispatcherGetLastName();

        String titleFormat = getString(R.string.sign_up_personal_title);
        String title = String.format(titleFormat, mLastName);
        mTitle.setText(title);
    }

    void checkUI() {
        if (gender == GenderPicker.GENDER.none || birthDay == null) {
            mContinue.setEnabled(false);
            return;
        }

        mContinue.setEnabled(true);
    }
}
