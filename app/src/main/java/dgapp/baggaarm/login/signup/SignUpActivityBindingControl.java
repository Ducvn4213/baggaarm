package dgapp.baggaarm.login.signup;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import dgapp.baggaarm.R;
import dgapp.baggaarm.utils.NonSwipeViewPager;

public class SignUpActivityBindingControl extends AppCompatActivity {

    protected NonSwipeViewPager mContainer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        bindingControls();
    }

    void bindingControls() {
        mContainer = (NonSwipeViewPager) findViewById(R.id.vp_container);
    }
}
