package dgapp.baggaarm.login.signup.fragments.verify;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import dgapp.baggaarm.R;
import dgapp.baggaarm.login.signup.fragments.SignUpBaseFragment;
import dgapp.baggaarm.service.AuthenticationService;

public class VerifyFragment extends SignUpBaseFragment {

    TextView mTitle;
    Button mDidVerify;
    Button mResend;

    AuthenticationService mService = AuthenticationService.getInstance(getContext());

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_up_verify, container, false);

        bindingControls(view);
        setupControlEvents();

        return view;
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible) {
            init();
        }
    }

    void bindingControls(View view) {
        mTitle = (TextView) view.findViewById(R.id.tv_title);
        mDidVerify = (Button) view.findViewById(R.id.btn_did_verify);
        mResend = (Button) view.findViewById(R.id.btn_resend);
    }

    void setupControlEvents() {
        mResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLoading();
                AuthenticationService.SignUpData signUpData = getParentActivity().getSignUpData();
                mService.resendVerifyEmail(signUpData.email, signUpData.password, new AuthenticationService.AuthenticationCallback() {
                    @Override
                    public void onCompleted() {
                        hideLoading();
                        String title = getString(R.string.button_notice);
                        String message = getString(R.string.verify_resend);
                        showDialog(title, message);
                    }

                    @Override
                    public void onFail(String error) {
                        hideLoading();
                        //TODO
                    }
                });
            }
        });

        mDidVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLoading();
                AuthenticationService.SignUpData signUpData = getParentActivity().getSignUpData();
                mService.checkVerified(signUpData.email, signUpData.password, new AuthenticationService.AuthenticationCallback() {
                    @Override
                    public void onCompleted() {
                        hideLoading();
                        getParentActivity().signUpCompleted();
                    }

                    @Override
                    public void onFail(String message) {
                        hideLoading();
                        String errorMessage = null;
                        if (message.equals(AuthenticationService.ERROR_WEAK_PASSWORD))
                            errorMessage = getString(R.string.sign_in_error_week_password);
                        else if (message.equals(AuthenticationService.ERROR_INVALID_EMAIL))
                            errorMessage = getString(R.string.sign_in_error_email_invalid);
                        else if (message.equals(AuthenticationService.ERROR_USER_NOT_FOUND))
                            errorMessage = getString(R.string.sign_in_error_not_exist);
                        else if (message.equals(AuthenticationService.ERROR_NETWORK))
                            errorMessage = getString(R.string.sign_in_error_network);
                        else if (message.equals(AuthenticationService.ERROR_WRONG_PASSWORD))
                            errorMessage = getString(R.string.sign_in_error_wrong_password);
                        else if (message.equals(AuthenticationService.ERROR_EMAIL_ALREADY_IN_USE))
                            errorMessage = getString(R.string.sign_in_error_already_in_use);
                        else if (message.equals(AuthenticationService.UNVERIFIED))
                            errorMessage = getString(R.string.sign_in_error_unverified);

                        showErrorDialog(errorMessage);
                    }
                });
            }
        });
    }

    void init() {
        String name = getParentActivity().dispatcherGetLastName();
        String email = getParentActivity().dispatcherGetEmail();

        String titleFormat = getString(R.string.sign_up_verify_title_1);
        String title = String.format(titleFormat, name, email);
        mTitle.setText(title);
    }
}
