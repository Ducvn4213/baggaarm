package dgapp.baggaarm.login.signup.fragments.major;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatSpinner;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import dgapp.baggaarm.R;
import dgapp.baggaarm.login.signup.fragments.SignUpBaseFragment;
import dgapp.baggaarm.login.signup.fragments.major.adapter.MajorListAdapter;
import dgapp.baggaarm.service.UniversityService;
import dgapp.baggaarm.service.model.firebase.FB_Speciality;

public class MajorInformationFragment extends SignUpBaseFragment {

    AppCompatSpinner mSeason;
    ListView mMajor;

    List<FB_Speciality> mMajorListData;
    MajorListAdapter mMajorAdapter;
    Button mContinue;

    String mMajorCode;
    String mSeasonValue;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_up_major_and_season_information, container, false);

        bindingControls(view);
        setupControlEvents();

        return view;
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible) {
            init();
        }
    }

    void bindingControls(View view) {
        mSeason = (AppCompatSpinner) view.findViewById(R.id.spn_season);
        mMajor = (ListView) view.findViewById(R.id.lv_major_list);
        mContinue = (Button) view.findViewById(R.id.btn_continue);
    }

    void setupControlEvents() {
        mMajor.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mMajorAdapter.setSelectionIndex(position);
                mMajorCode = mMajorListData.get(position).ID;

                mContinue.setEnabled(true);
            }
        });

        mContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getParentActivity().setMajor(mMajorCode);
                getParentActivity().setSeason(Integer.parseInt((String) mSeason.getSelectedItem()));
                onNext();
            }
        });
    }

    void init() {
        String universityID = getParentActivity().dispatcherGetUniversityID();

        mMajorListData = UniversityService.getInstance().getMajorsByUID(universityID);
        mMajorAdapter = new MajorListAdapter(getParentActivity(), mMajorListData);
        mMajor.setAdapter(mMajorAdapter);

        List<String> seasonList = new ArrayList<>();
        String[] items = getResources().getStringArray(R.array.array_season);
        if (seasonList.size() == 0) {
            Collections.addAll(seasonList, items);
        }

        mSeasonValue = seasonList.get(0);
        ArrayAdapter<String> pm_adapter = new ArrayAdapter<>(getParentActivity(), R.layout.layout_spinner_item_center,
                R.id.tv_item_text, seasonList);
        pm_adapter.setDropDownViewResource(R.layout.layout_spinner_dropdown_item_center);
        mSeason.setAdapter(pm_adapter);
    }
}
