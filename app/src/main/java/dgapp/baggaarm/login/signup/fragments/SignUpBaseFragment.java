package dgapp.baggaarm.login.signup.fragments;

import android.support.v4.app.Fragment;

import dgapp.baggaarm.login.signup.SignUpActivity;
import dgapp.baggaarm.utils.BaseFragment;

public class SignUpBaseFragment extends BaseFragment {

    protected void onNext() {
        SignUpActivity activity = (SignUpActivity) getActivity();
        activity.moveNext();
    }

    protected SignUpActivity getParentActivity() {
        return (SignUpActivity) getActivity();
    }
}
