package dgapp.baggaarm.login.signup.fragments.university.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import dgapp.baggaarm.R;
import dgapp.baggaarm.service.model.firebase.FB_University;

public class UniversityListAdapter extends ArrayAdapter<FB_University> {

    private Context mContext;
    private List<FB_University> mData;
    private static LayoutInflater mInflater = null;

    private int selectionIndex = -1;
    private boolean isShowNameOnly = false;

    public UniversityListAdapter(Context context, List<FB_University> data) {
        super(context, R.layout.layout_univisity_item, data);

        mContext = context;
        mData = data;

        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return mData.size();
    }

    public long getItemId(int position) {
        return position;
    }

    public void setSelectionIndex(int index) {
        this.selectionIndex = index;
        notifyDataSetInvalidated();
    }

    public void showNameOnly() {
        isShowNameOnly = true;
        notifyDataSetInvalidated();
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if(view == null) {
            view = mInflater.inflate(R.layout.layout_univisity_item, null);
        }

        FB_University data = mData.get(position);

        TextView code = (TextView) view.findViewById(R.id.tv_university_code);
        TextView separator = (TextView) view.findViewById(R.id.tv_separator);
        TextView name = (TextView) view.findViewById(R.id.tv_university_name);

        if (isShowNameOnly) {
            separator.setVisibility(View.GONE);
            code.setVisibility(View.GONE);
        }
        else {
            separator.setVisibility(View.VISIBLE);
            code.setVisibility(View.VISIBLE);
        }

        code.setText(data.ID);
        name.setText(data.fullName);

        if (this.selectionIndex == position) {
            view.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
        }
        else {
            view.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorWhite));
        }

        return view;
    }
}
