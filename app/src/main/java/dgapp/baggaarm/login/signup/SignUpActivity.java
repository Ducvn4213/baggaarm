package dgapp.baggaarm.login.signup;

import android.os.Bundle;
import android.support.annotation.Nullable;
import dgapp.baggaarm.login.signup.adapter.SignUpFragmentAdapter;
import dgapp.baggaarm.utils.Utils;

public class SignUpActivity extends SignUpActivityDispatcherData {

    SignUpFragmentAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initData();
    }

    void initData() {
        mAdapter = new SignUpFragmentAdapter(getSupportFragmentManager());
        mContainer.setAdapter(mAdapter);
    }

    public void moveNext() {
        Utils.hideKeyboard(SignUpActivity.this);
        int index = mContainer.getCurrentItem() + 1;
        if (index < mAdapter.getCount()) {
            mContainer.setCurrentItem(index, true);
        }
    }

    public void signUpCompleted() {
        setResult(RESULT_OK, null);
        finish();
    }

    @Override
    public void onBackPressed() {
        int index = mContainer.getCurrentItem();
        if (index == 0 || index == (mContainer.getChildCount() - 1)) {
            super.onBackPressed();
            return;
        }

        index = index - 1;
        mContainer.setCurrentItem(index);
    }
}
