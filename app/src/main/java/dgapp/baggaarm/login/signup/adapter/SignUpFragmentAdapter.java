package dgapp.baggaarm.login.signup.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import dgapp.baggaarm.login.signup.fragments.base.BaseInformationFragment;
import dgapp.baggaarm.login.signup.fragments.major.MajorInformationFragment;
import dgapp.baggaarm.login.signup.fragments.personal.PersonalInformationFragment;
import dgapp.baggaarm.login.signup.fragments.required.RequiredInformationFragment;
import dgapp.baggaarm.login.signup.fragments.university.UniversityInformationFragment;
import dgapp.baggaarm.login.signup.fragments.verify.VerifyFragment;

public class SignUpFragmentAdapter extends FragmentStatePagerAdapter {

    public SignUpFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new BaseInformationFragment();
            case 1:
                return new PersonalInformationFragment();
            case 2:
                return new UniversityInformationFragment();
            case 3:
                return new MajorInformationFragment();
            case 4:
                return new RequiredInformationFragment();
            case 5:
                return new VerifyFragment();
            default: break;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 6;
    }
}
