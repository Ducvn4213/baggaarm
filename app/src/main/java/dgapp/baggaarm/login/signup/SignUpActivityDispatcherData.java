package dgapp.baggaarm.login.signup;

import android.os.Bundle;
import android.os.MemoryFile;
import android.support.annotation.Nullable;

import dgapp.baggaarm.login.signup.adapter.SignUpFragmentAdapter;
import dgapp.baggaarm.service.AuthenticationService;
import dgapp.baggaarm.service.model.firebase.FB_StudyInformation;
import dgapp.baggaarm.utils.GenderPicker;

public class SignUpActivityDispatcherData extends SignUpActivityBindingControl {

    String mLastName;
    String mFirstName;

    GenderPicker.GENDER mGender;
    String mBirthDay;

    String mUniversityCode;
    String mUniversityEmail;
    String mMajorCode;
    int mSeason;

    String mEmail;
    String mPassword;

    public void setUniversity(String code) {
        mUniversityCode = code;
    }

    public void setUniversityEmail(String email) {
        mUniversityEmail = email;
    }

    public void setMajor(String code) {
        mMajorCode = code;
    }

    public void setSeason(int season) {
        mSeason = season;
    }

    public void setName(String lastName, String firstName) {
        mFirstName = firstName;
        mLastName = lastName;
    }

    public void setPersonalInfo(GenderPicker.GENDER gender, String birthDay) {
        mGender = gender;
        mBirthDay = birthDay;
    }

    public void setRequiredInfo(String email, String pass) {
        mEmail = email;
        mEmail = "ducgaogao4213@gmail.com";
        mPassword = pass;
    }



    public String dispatcherGetLastName() {
        return mLastName;
    }

    public String dispatcherGetUniversityID() {
        return mUniversityCode;
    }

    public String dispatcherGetUniversityEmail() {
        return mUniversityEmail;
    }

    public String dispatcherGetEmail() {
        return mEmail;
    }

    public AuthenticationService.SignUpData getSignUpData() {
        AuthenticationService.SignUpData data = new AuthenticationService.SignUpData();

        FB_StudyInformation studyInformation = new FB_StudyInformation();
        studyInformation.universityID = mUniversityCode;
        studyInformation.specialityID = mMajorCode;
        studyInformation.startYear = mSeason;

        data.birthday = mBirthDay;
        data.displayName = mFirstName + " " + mLastName;
        data.email = mEmail;
        data.password = mPassword;
        data.firstName = mFirstName;
        data.surname = mLastName;
        data.gender = mGender == GenderPicker.GENDER.other ? 2 : mGender == GenderPicker.GENDER.male ? 0 : 1;
        data.studyInformation = studyInformation;

        return data;
    }
}
