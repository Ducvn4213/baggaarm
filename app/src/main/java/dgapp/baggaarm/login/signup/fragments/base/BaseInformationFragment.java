package dgapp.baggaarm.login.signup.fragments.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import dgapp.baggaarm.R;
import dgapp.baggaarm.login.signup.fragments.SignUpBaseFragment;
import dgapp.baggaarm.utils.Utils;

public class BaseInformationFragment extends SignUpBaseFragment {

    EditText mFirstName;
    EditText mLastName;
    Button mContinue;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_up_base_information, container, false);

        bindingControls(view);
        setupControlEvents();

        return view;
    }

    void bindingControls(View view) {
        mContinue = (Button) view.findViewById(R.id.btn_continue);
        mFirstName = (EditText) view.findViewById(R.id.edt_firstname);
        mLastName = (EditText) view.findViewById(R.id.edt_lastname);
    }

    void setupControlEvents() {
        mContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fname = mFirstName.getText().toString();
                String lname = mLastName.getText().toString();
                getParentActivity().setName(lname, fname);

                closeKeyboard();
                onNext();
            }
        });

        mFirstName.addTextChangedListener(textWatcher);
        mLastName.addTextChangedListener(textWatcher);
    }

    private void closeKeyboard() {
        Utils.closeKeyboard(getContext(), mFirstName.getWindowToken());
        Utils.closeKeyboard(getContext(), mLastName.getWindowToken());
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            String fname = mFirstName.getText().toString();
            String lname = mLastName.getText().toString();

            if (fname.isEmpty() || lname.isEmpty()) {
                mContinue.setEnabled(false);
                return;
            }

            mContinue.setEnabled(true);
        }
    };
}
