package dgapp.baggaarm.login.signup.fragments.university;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import dgapp.baggaarm.R;
import dgapp.baggaarm.login.signup.fragments.SignUpBaseFragment;
import dgapp.baggaarm.login.signup.fragments.university.adapter.UniversityListAdapter;
import dgapp.baggaarm.main.home.child.university.UniversityFragment;
import dgapp.baggaarm.service.UniversityService;
import dgapp.baggaarm.service.model.firebase.FB_University;

public class UniversityInformationFragment extends SignUpBaseFragment {

    List<FB_University> mListData;
    UniversityListAdapter mAdapter;


    ListView mUniversityList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_up_university_information, container, false);

        bindingControls(view);
        setupControlEvents();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        init();
    }

    void bindingControls(View view) {
        mUniversityList = (ListView) view.findViewById(R.id.lv_university_list);
    }

    void setupControlEvents() {
        mUniversityList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mAdapter.setSelectionIndex(position);
                getParentActivity().setUniversity(mListData.get(position).ID);
                getParentActivity().setUniversityEmail(mListData.get(position).email);
                onNext();
            }
        });
    }

    void init() {
        mListData = UniversityService.getInstance().getUniversitiesData();
        mAdapter = new UniversityListAdapter(getContext(), mListData);
        mUniversityList.setAdapter(mAdapter);
    }
}
