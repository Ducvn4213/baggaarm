package dgapp.baggaarm.login.signup.fragments.required;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import dgapp.baggaarm.R;
import dgapp.baggaarm.login.signup.fragments.SignUpBaseFragment;
import dgapp.baggaarm.service.AuthenticationService;
import dgapp.baggaarm.utils.Utils;

public class RequiredInformationFragment extends SignUpBaseFragment {

    EditText mEmail;
    EditText mPassword;
    TextView mEmailInfo;
    Button mVerify;

    AuthenticationService mService = AuthenticationService.getInstance(getContext());

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_up_required_information, container, false);

        bindingControls(view);
        setupControlEvents();

        return view;
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible) {
            init();
        }
    }

    void bindingControls(View view) {
        mEmail = (EditText) view.findViewById(R.id.edt_email);
        mPassword = (EditText) view.findViewById(R.id.edt_password);
        mVerify = (Button) view.findViewById(R.id.btn_verify);
        mEmailInfo = (TextView) view.findViewById(R.id.tv_email_info);
    }

    void setupControlEvents() {
        mEmail.addTextChangedListener(textWatcher);
        mPassword.addTextChangedListener(textWatcher);

        mVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = mEmail.getText().toString() + mEmailInfo.getText().toString();
                String pass = mPassword.getText().toString();

                getParentActivity().setRequiredInfo(email, pass);
                register();
            }
        });
    }

    void init() {
        String emailInfo = getParentActivity().dispatcherGetUniversityEmail();
        mEmailInfo.setText(emailInfo);
    }

    void register() {
        closeKeyboard();
        showLoading();
        AuthenticationService.SignUpData data = getParentActivity().getSignUpData();
        mService.register(data, new AuthenticationService.AuthenticationCallback() {
            @Override
            public void onCompleted() {
                hideLoading();
                onNext();
            }

            @Override
            public void onFail(String message) {
                hideLoading();
                String errorMessage = null;
                if (message.equals(AuthenticationService.ERROR_WEAK_PASSWORD))
                    errorMessage = getString(R.string.sign_in_error_week_password);
                else if (message.equals(AuthenticationService.ERROR_INVALID_EMAIL))
                    errorMessage = getString(R.string.sign_in_error_email_invalid);
                else if (message.equals(AuthenticationService.ERROR_USER_NOT_FOUND))
                    errorMessage = getString(R.string.sign_in_error_not_exist);
                else if (message.equals(AuthenticationService.ERROR_NETWORK))
                    errorMessage = getString(R.string.sign_in_error_network);
                else if (message.equals(AuthenticationService.ERROR_WRONG_PASSWORD))
                    errorMessage = getString(R.string.sign_in_error_wrong_password);
                else if (message.equals(AuthenticationService.ERROR_EMAIL_ALREADY_IN_USE))
                    errorMessage = getString(R.string.sign_in_error_already_in_use);

                showErrorDialog(errorMessage);
            }
        });
    }

    private void closeKeyboard() {
        Utils.closeKeyboard(getContext(), mEmail.getWindowToken());
        Utils.closeKeyboard(getContext(), mPassword.getWindowToken());
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            String mail = mEmail.getText().toString();
            String pass = mPassword.getText().toString();

            if (mail.isEmpty() || pass.isEmpty()) {
                mVerify.setEnabled(false);
                return;
            }

            mVerify.setEnabled(true);
        }
    };
}
