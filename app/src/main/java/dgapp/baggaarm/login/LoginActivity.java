package dgapp.baggaarm.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Spannable;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Config;
import android.view.View;
import android.widget.TextView;

import dgapp.baggaarm.MainActivity;
import dgapp.baggaarm.R;
import dgapp.baggaarm.login.signin.SignInActivity;
import dgapp.baggaarm.login.signup.SignUpActivity;
import dgapp.baggaarm.service.Configs;
import dgapp.baggaarm.utils.WebViewActivity;

public class LoginActivity extends LoginActivityBindingControls {

    private final int SIGN_IN_REQUEST_CODE = 111;
    private final int SIGN_UP_REQUEST_CODE = 222;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupControlEvents();
    }

    void setupControlEvents() {
        mSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, SignInActivity.class);
                startActivityForResult(intent, SIGN_IN_REQUEST_CODE);
            }
        });

        mSignUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivityForResult(intent, SIGN_UP_REQUEST_CODE);
            }
        });

        mTandC.setMovementMethod(LinkMovementMethod.getInstance());
        String myString = getString(R.string.login_tandc);
        final int i1 = myString.indexOf("Chính");
        int i2 = myString.indexOf(", Điều");
        int i3 = myString.indexOf("vụ") + 1;
        mTandC.setText(myString, TextView.BufferType.SPANNABLE);
        Spannable mySpannable = (Spannable) mTandC.getText();
        ClickableSpan myClickableSpan = new ClickableSpan()
        {
            @Override
            public void onClick(View widget) {
                Intent intent = new Intent(LoginActivity.this, WebViewActivity.class);
                intent.putExtra("url", Configs.TERM_LINK);
                startActivity(intent);
            }
        };

        ClickableSpan myClickableSpan2 = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                Intent intent = new Intent(LoginActivity.this, WebViewActivity.class);
                intent.putExtra("url", Configs.CONDITION_LINK);
                startActivity(intent);
            }
        };
        mySpannable.setSpan(myClickableSpan, i1, i2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mySpannable.setSpan(myClickableSpan2, i2 + 2, i3 + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SIGN_IN_REQUEST_CODE && resultCode == RESULT_OK) {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }

        if (requestCode == SIGN_UP_REQUEST_CODE && resultCode == RESULT_OK) {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }
    }
}
