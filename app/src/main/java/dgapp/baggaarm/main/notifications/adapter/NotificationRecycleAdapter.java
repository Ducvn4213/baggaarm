package dgapp.baggaarm.main.notifications.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import dgapp.baggaarm.R;
import dgapp.baggaarm.service.model.firebase.FB_Event;
import dgapp.baggaarm.service.model.firebase.FB_Notification;
import dgapp.baggaarm.utils.EventItem;

public class NotificationRecycleAdapter extends RecyclerView.Adapter<NotificationRecycleAdapter.MyViewHolder> {

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView image;
        public TextView message;

        public MyViewHolder(View view) {
            super(view);

            image = (ImageView) view.findViewById(R.id.iv_image);
            message = (TextView) view.findViewById(R.id.tv_message);
        }
    }

    public ArrayList<FB_Notification> mData;

    public NotificationRecycleAdapter(ArrayList<FB_Notification> data) {
        this.mData = data;
    }

    @Override
    public NotificationRecycleAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_event_item_in_list, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
//        holder.item.setData(position, mData.get(position));
//        holder.item.setDelegate(itemDelegate);
//        holder.item.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                onItemClickListener.onItemClickListener(position);
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void add(FB_Notification data, int position) {
        insert(data, position);
    }

    public void add(FB_Notification data) {
        int thePos = mData.size();
        insert(data, thePos);
    }

    public void insert(FB_Notification data, int position) {
        this.mData.add(position, data);
        notifyItemInserted(position);
    }

    public void remove(int position) {
        mData.remove(position);
        notifyItemRemoved(position);
    }

    public FB_Notification getItem(int pos) {
        return mData.get(pos);
    }

    public void clear() {
        int size = mData.size();
        mData.clear();
        notifyItemRangeRemoved(0, size);
    }

    public void clearWithoutReloadUI() {
        mData.clear();
    }

    public void addAll(ArrayList<FB_Event> data) {
        int startIndex = data.size();
        data.addAll(startIndex, data);
        notifyItemRangeInserted(startIndex, data.size());
    }

    public interface OnUpdateValueFail {
        void onUpdateValueFail();
    }

    public interface OnItemClickListener {
        void onItemClickListener(int position);
    }

    OnUpdateValueFail onUpdateValueFail;
    OnItemClickListener onItemClickListener;

    public void setOnUpdateValueFail(OnUpdateValueFail callback) {
        this.onUpdateValueFail = callback;
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.onItemClickListener = listener;
    }

    private ItemDelegate itemDelegate = new ItemDelegate();

    private class ItemDelegate implements EventItem.EventItemDelegate {

        @Override
        public void onInterestingPrepareToChange(int index, boolean value) {
            mData.get(index).isInteresting = value;
            if (value) {
                mData.get(index).interestingCount += 1;
            }
            else {
                mData.get(index).interestingCount -= 1;
            }
            notifyDataSetChanged();
        }

        @Override
        public void onSavesPrepareToChange(int index, boolean value) {
            mData.get(index).isSaved = value;
            if (value) {
                mData.get(index).saveCount += 1;
            }
            else {
                mData.get(index).saveCount -= 1;
            }
            notifyDataSetChanged();
        }

        @Override
        public void onWillGoPrepareToChange(int index, boolean value) {
            mData.get(index).isWillGo = value;
            if (value) {
                mData.get(index).willGoCount += 1;
            }
            else {
                mData.get(index).willGoCount -= 1;
            }
            notifyDataSetChanged();
        }

        @Override
        public void onInterestingChanged(int index, boolean value) {
            //Do nothing if value actually changed
        }

        @Override
        public void onSavesChanged(int index, boolean value) {
            //Do nothing if value actually changed
        }

        @Override
        public void onWillGoChanged(int index, boolean value) {
            //Do nothing if value actually changed
        }

        @Override
        public void onInterestingChangeFail(int index, boolean oldValue) {
            mData.get(index).isInteresting = !oldValue;
            if (!oldValue) {
                mData.get(index).interestingCount += 1;
            }
            else {
                mData.get(index).interestingCount -= 1;
            }
            notifyDataSetChanged();
            onUpdateValueFail.onUpdateValueFail();
        }

        @Override
        public void onSavesChangedFail(int index, boolean oldValue) {
            mData.get(index).isSaved = !oldValue;
            if (!oldValue) {
                mData.get(index).saveCount += 1;
            }
            else {
                mData.get(index).saveCount -= 1;
            }
            notifyDataSetChanged();
            onUpdateValueFail.onUpdateValueFail();
        }

        @Override
        public void onWillGoChangedFail(int index, boolean oldValue) {
            mData.get(index).isWillGo = !oldValue;
            if (!oldValue) {
                mData.get(index).willGoCount += 1;
            }
            else {
                mData.get(index).willGoCount -= 1;
            }
            notifyDataSetChanged();
            onUpdateValueFail.onUpdateValueFail();
        }
    }
}
