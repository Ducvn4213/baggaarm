package dgapp.baggaarm.main.notifications;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.malinskiy.superrecyclerview.SuperRecyclerView;

import dgapp.baggaarm.R;

public class NotificationsFragment extends Fragment {

    ImageButton mSettings;
    SuperRecyclerView mNotificationList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notifications, container, false);

        bindingControls(view);
        setupControlEvents();

        return view;
    }

    void bindingControls(View view) {
        mSettings = (ImageButton) view.findViewById(R.id.ib_settings_notification);
        mNotificationList = (SuperRecyclerView) view.findViewById(R.id.srv_notification_list);
    }

    void setupControlEvents() {

    }
}
