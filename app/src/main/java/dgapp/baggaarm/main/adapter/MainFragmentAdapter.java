package dgapp.baggaarm.main.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import dgapp.baggaarm.main.account.AccountFragment;
import dgapp.baggaarm.main.contacts.ContactsFragment;
import dgapp.baggaarm.main.home.HomeFragment;
import dgapp.baggaarm.main.notifications.NotificationsFragment;

public class MainFragmentAdapter extends FragmentStatePagerAdapter {

    public MainFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new HomeFragment();
            case 1:
                return new ContactsFragment();
            case 2:
                return new NotificationsFragment();
            case 3:
                return new AccountFragment();
            default: break;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }
}

