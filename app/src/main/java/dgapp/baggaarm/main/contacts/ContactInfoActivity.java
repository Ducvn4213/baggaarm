package dgapp.baggaarm.main.contacts;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import dgapp.baggaarm.R;
import dgapp.baggaarm.utils.BaseActivity;
import dgapp.baggaarm.utils.WebViewActivity;

public class ContactInfoActivity extends BaseActivity {

    String id, name, avatar, relationship, face_id, university, major, year;

    TextView mName;
    TextView mUniversityName;
    TextView mMajor;
    TextView mSeason;
    ImageView mAvatar;

    ImageButton mBack;

    FirebaseStorage storage = FirebaseStorage.getInstance();
    StorageReference storageRef = storage.getReference();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_info);

        bindingControls();
        setupControlEvents();
        initData();
    }

    void bindingControls() {
        mName = (TextView) findViewById(R.id.tv_name);
        mUniversityName = (TextView) findViewById(R.id.tv_university_name);
        mMajor = (TextView) findViewById(R.id.tv_major);
        mSeason = (TextView) findViewById(R.id.tv_season);
        mAvatar = (ImageView)  findViewById(R.id.iv_avatar);

        mBack = (ImageButton) findViewById(R.id.ib_back);
    }

    void setupControlEvents() {
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactInfoActivity.this.finish();
            }
        });
    }

    void initData() {
        Intent intent = getIntent();
        id = intent.getStringExtra("id");
        name = intent.getStringExtra("name");
        avatar = intent.getStringExtra("avatar");
        university = intent.getStringExtra("university");
        major = intent.getStringExtra("major");
        year = intent.getStringExtra("year");

        mName.setText(name);

        mUniversityName.setText(university);
        mMajor.setText(major);
        mSeason.setText("Khóa " + year);

        if (!avatar.isEmpty()) {
            int startIndex = avatar.indexOf("profileImages");
            if (startIndex < 0) {
                Picasso.with(getBaseContext())
                        .load(avatar)
                        .into(new Target() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                mAvatar.setImageBitmap(bitmap);
                            }

                            @Override
                            public void onBitmapFailed(Drawable errorDrawable) {
                                mAvatar.setImageDrawable(ContextCompat.getDrawable(getBaseContext(), R.mipmap.ic_avatar));
                            }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {
                                //TODO
                            }
                        });
            }
            else {
                int endIndex = avatar.indexOf(".jpg");
                String child = avatar.substring(startIndex, endIndex) + ".jpg";
                storageRef.child(child).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        Picasso.with(getBaseContext()).cancelRequest(mAvatar);
                        Picasso.with(getBaseContext()).load(uri).into(mAvatar);
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        mAvatar.setImageDrawable(ContextCompat.getDrawable(getBaseContext(), R.mipmap.ic_avatar));
                    }
                });
            }
        }
        else {
            mAvatar.setImageDrawable(ContextCompat.getDrawable(getBaseContext(), R.mipmap.ic_avatar));
        }

        if (!avatar.isEmpty()) {
            Picasso.with(ContactInfoActivity.this).load(avatar).into(mAvatar);
        }
    }
}
