package dgapp.baggaarm.main.contacts;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatSpinner;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import dgapp.baggaarm.R;
import dgapp.baggaarm.service.ContactService;
import dgapp.baggaarm.service.UniversityService;
import dgapp.baggaarm.service.model.firebase.FB_Speciality;
import dgapp.baggaarm.service.model.firebase.FB_University;
import dgapp.baggaarm.utils.BaseActivity;

public class FilterContactsActivity extends BaseActivity {

    ImageButton mBack;
    AppCompatSpinner mUniversityList;
    AppCompatSpinner mMajorList;
    AppCompatSpinner mSeasonList;
    AppCompatSpinner mGenderList;
    Button mStart;

    UniversityService mUniversityService = UniversityService.getInstance();

    List<FB_University> mUniversityData;
    List<FB_Speciality> mMajorData;
    List<String> mSeasonData;
    List<String> mGenderData;

    int mSelectedUniversityIndex = -1;
    int mSelectedMajorIndex = -1;
    int mSelectedSeasonIndex = -1;
    int mSelectedGenderIndex = -1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts_filter);

        bindingControls();
        setupControlsEvent();
        init();
    }

    void bindingControls() {
        mBack = (ImageButton) findViewById(R.id.ib_back);
        mUniversityList = (AppCompatSpinner) findViewById(R.id.spn_university);
        mMajorList = (AppCompatSpinner) findViewById(R.id.spn_major);
        mSeasonList = (AppCompatSpinner) findViewById(R.id.spn_season);
        mGenderList = (AppCompatSpinner) findViewById(R.id.spn_gender);
        mStart = (Button) findViewById(R.id.btn_start);
    }

    void setupControlsEvent() {
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mUniversityList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mSelectedUniversityIndex = i - 1;
                if (i == 0) {
                    if (mSelectedMajorIndex > 0) {
                        init();
                    }
                    return;
                }
                initMajorSpinnerWithUniversity(mUniversityData.get(i - 1).ID);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //TODO
            }
        });

        mMajorList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mSelectedMajorIndex = i - 1;
                checkEnoughData();
                if (i == 0) {
                    initSeasonSpinner();
                    initGenderSpinner();
                    return;
                }

                enableSeasonSpinner();
                enableGenderSpinner();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //TODO
            }
        });

        mSeasonList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mSelectedSeasonIndex = i;
                if (i != 0) {
                    checkEnoughData();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mGenderList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mSelectedGenderIndex = i;
                if (i != 0) {
                    checkEnoughData();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String uid = mUniversityData.get(mSelectedUniversityIndex).ID;
                String mid = mMajorData.get(mSelectedMajorIndex).ID;
                String sy = mSelectedSeasonIndex < 1 ? "" : mSeasonData.get(mSelectedSeasonIndex);
                String gd = mSelectedGenderIndex < 1 ? "" : (mSelectedGenderIndex + 1) + "";
                ContactService.getInstance().applyFilters(uid, mid, sy, gd);
                finish();
            }
        });
    }

    void init() {
        //initUniversitySpinner
        initUniversitySpinner();
        //initMajorSpinner
        initMajorSpinner();
        //initSeasonSpinner
        initSeasonSpinner();
        //initGenderSpinner
        initGenderSpinner();
    }

    void checkEnoughData() {
        if (mSelectedUniversityIndex > -1 && mSelectedMajorIndex > -1) {
            mStart.setEnabled(true);
            return;
        }

        mStart.setEnabled(false);
    }

    void initUniversitySpinner() {
        mUniversityData = UniversityService.getInstance().getUniversitiesData();
        List<String> seasonList = new ArrayList<>();
        seasonList.add(getString(R.string.contact_filter_university_index_zero));

        for (FB_University rau : mUniversityData) {
            seasonList.add(rau.fullName);
        }

        ArrayAdapter<String> pm_adapter = new ArrayAdapter<>(FilterContactsActivity.this, R.layout.layout_spinner_item_center,
                R.id.tv_item_text, seasonList);
        pm_adapter.setDropDownViewResource(R.layout.layout_spinner_dropdown_item_center);
        mUniversityList.setAdapter(pm_adapter);
    }

    void initMajorSpinner() {
        List<String> seasonList = new ArrayList<>();
        seasonList.add(getString(R.string.contact_filter_major_index_zero));

        ArrayAdapter<String> pm_adapter = new ArrayAdapter<>(FilterContactsActivity.this, R.layout.layout_spinner_item_center_gray,
                R.id.tv_item_text, seasonList);
        pm_adapter.setDropDownViewResource(R.layout.layout_spinner_dropdown_item_center_gray);
        mMajorList.setAdapter(pm_adapter);
        mMajorList.setEnabled(false);
    }

    void initSeasonSpinner() {
        mSeasonData = new ArrayList<>();
        mSeasonData.add(getString(R.string.contact_filter_season_index_zero));
        String[] items = getResources().getStringArray(R.array.array_season);
        if (mSeasonData.size() == 0) {
            Collections.addAll(mSeasonData, items);
        }

        ArrayAdapter<String> pm_adapter = new ArrayAdapter<>(FilterContactsActivity.this, R.layout.layout_spinner_item_center_gray,
                R.id.tv_item_text, mSeasonData);
        pm_adapter.setDropDownViewResource(R.layout.layout_spinner_dropdown_item_center_gray);
        mSeasonList.setAdapter(pm_adapter);
        mSeasonList.setEnabled(false);
    }

    void initGenderSpinner() {
        mGenderData = new ArrayList<>();
        mGenderData.add(getString(R.string.contact_filter_gender_index_zero));

        String[] items = getResources().getStringArray(R.array.array_gender);
        if (mGenderData.size() == 0) {
            Collections.addAll(mGenderData, items);
        }

        ArrayAdapter<String> pm_adapter = new ArrayAdapter<>(FilterContactsActivity.this, R.layout.layout_spinner_item_center_gray,
                R.id.tv_item_text, mGenderData);
        pm_adapter.setDropDownViewResource(R.layout.layout_spinner_dropdown_item_center_gray);
        mGenderList.setAdapter(pm_adapter);
        mGenderList.setEnabled(false);
    }

    void initMajorSpinnerWithUniversity(String id) {
        mMajorData = UniversityService.getInstance().getMajorsData();

        List<String> seasonList = new ArrayList<>();
        seasonList.add(getString(R.string.contact_filter_major_index_zero));

        for (FB_Speciality ras : mMajorData) {
            seasonList.add(ras.fullName);
        }

        ArrayAdapter<String> pm_adapter = new ArrayAdapter<>(FilterContactsActivity.this, R.layout.layout_spinner_item_center,
                R.id.tv_item_text, seasonList);
        pm_adapter.setDropDownViewResource(R.layout.layout_spinner_dropdown_item_center);
        mMajorList.setAdapter(pm_adapter);
        mMajorList.setEnabled(true);
    }

    void enableSeasonSpinner() {
        mSeasonData = new ArrayList<>();
        mSeasonData.add(getString(R.string.contact_filter_season_index_zero));
        String[] items = getResources().getStringArray(R.array.array_season);

        Collections.addAll(mSeasonData, items);

        ArrayAdapter<String> pm_adapter = new ArrayAdapter<>(FilterContactsActivity.this, R.layout.layout_spinner_item_center,
                R.id.tv_item_text, mSeasonData);
        pm_adapter.setDropDownViewResource(R.layout.layout_spinner_dropdown_item_center);
        mSeasonList.setAdapter(pm_adapter);
        mSeasonList.setEnabled(true);
    }

    void enableGenderSpinner() {
        mGenderData = new ArrayList<>();
        mGenderData.add(getString(R.string.contact_filter_gender_index_zero));

        String[] items = getResources().getStringArray(R.array.array_gender);

        Collections.addAll(mGenderData, items);

        ArrayAdapter<String> pm_adapter = new ArrayAdapter<>(FilterContactsActivity.this, R.layout.layout_spinner_item_center,
                R.id.tv_item_text, mGenderData);
        pm_adapter.setDropDownViewResource(R.layout.layout_spinner_dropdown_item_center);
        mGenderList.setAdapter(pm_adapter);
        mGenderList.setEnabled(true);
    }
}
