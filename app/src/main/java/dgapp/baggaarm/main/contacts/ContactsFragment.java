package dgapp.baggaarm.main.contacts;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import dgapp.baggaarm.R;
import dgapp.baggaarm.main.account.follow.FollowerInfoActivity;
import dgapp.baggaarm.main.account.follow.adapter.FollowerListAdapter;
import dgapp.baggaarm.main.contacts.adapter.ContactRecycleAdapter;
import dgapp.baggaarm.main.home.child.event.adapter.EventRecycleAdapter;
import dgapp.baggaarm.service.AuthenticationService;
import dgapp.baggaarm.service.ContactService;
import dgapp.baggaarm.service.UniversityService;
import dgapp.baggaarm.service.model.firebase.FB_User;
import dgapp.baggaarm.utils.WebViewActivity;

public class ContactsFragment extends Fragment {

    ImageButton mFilter;
    SearchView mSearchView;
    TextView mUniversityName;
    SuperRecyclerView mContactList;
    ContactRecycleAdapter mAdapter;

    ProgressBar mProgress;

    ContactService mService = ContactService.getInstance();

    boolean preventReloadData = false;
    int currentPage = 0;
    FB_User curUser;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contacts, container, false);

        curUser = AuthenticationService.getInstance(getContext()).getCurrentUser();

        bindingControls(view);
        setupUI();
        setupControlEvents();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!preventReloadData) {
            loadData();
        }

        preventReloadData = false;
    }

    void bindingControls(View view) {
        mSearchView = (SearchView) view.findViewById(R.id.sv_search);
        mUniversityName = (TextView) view.findViewById(R.id.tv_university_name);
        mContactList = (SuperRecyclerView) view.findViewById(R.id.srv_contact_list);
        mProgress = (ProgressBar) view.findViewById(R.id.pb_progress);
        mFilter = (ImageButton) view.findViewById(R.id.ib_settings_notification);
    }

    void setupUI() {
        View v = mSearchView.findViewById(android.support.v7.appcompat.R.id.search_plate);
        v.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorSettingsBackground));

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mContactList.setLayoutManager(mLayoutManager);

        mAdapter = new ContactRecycleAdapter(getContext(), new ArrayList<FB_User>());
        mContactList.setAdapter(mAdapter);
    }

    void setupControlEvents() {
        mFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), FilterContactsActivity.class);
                startActivity(intent);
            }
        });

        mAdapter.setOnItemClickListener(new EventRecycleAdapter.OnItemClickListener() {
            @Override
            public void onItemClickListener(int position) {
                preventReloadData = true;

                FB_User user = mAdapter.getItem(position);

                Intent intent = new Intent(getActivity(), ContactInfoActivity.class);

                intent.putExtra("id", user.id);
                intent.putExtra("name", user.displayName);
                intent.putExtra("avatar", user.profileImageURL);
                intent.putExtra("university", UniversityService.getInstance().getUniversityName(user.studyingInformation.universityID));
                intent.putExtra("major", UniversityService.getInstance().getMajorName(user.studyingInformation.specialityID));
                intent.putExtra("year", user.studyingInformation.startYear + "");

                startActivity(intent);
            }
        });

        mAdapter.setOnRequestOpenFacebook(new FollowerListAdapter.OnFollowerItemNeedViewFacebook() {
            @Override
            public void onRequestOpen(String link, String name) {
                Intent intent = new Intent(getActivity(), WebViewActivity.class);
                intent.putExtra("url", link);
                intent.putExtra("title", name);
                startActivity(intent);
            }
        });

        mAdapter.setOnRequestViewMoreResults(new ContactRecycleAdapter.OnRequestViewMoreResults() {
            @Override
            public void onRequestViewMoreResults() {
                preventReloadData = true;
                mService.setCurrentSearchQuery(mSearchView.getQuery().toString());
                Intent intent = new Intent(getActivity(), ViewMoreContactsActivity.class);
                startActivity(intent);
            }
        });

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.isEmpty()) {
                    mContactList.setupMoreListener(new OnMoreListener() {
                        @Override
                        public void onMoreAsked(int numberOfItems, int numberBeforeMore, int currentItemPos) {
                            loadMoreEvents();
                        }}, 2);
                }
                else {
                    mContactList.removeMoreListener();
                }
                long callbackID = (new Date()).getTime();
                mService.getContactsWithName(callbackID, curUser.studyingInformation.universityID, newText, new ContactService.FilterContactsCallback() {
                    @Override
                    public void onFilterSuccess(final List<FB_User> data) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            public void run() {
                                mProgress.setVisibility(View.GONE);
                                mContactList.setVisibility(View.VISIBLE);

                                mAdapter.clear();

                                if (data.size() > 7) {
                                    data.add(null);
                                }

                                mAdapter.addAll((ArrayList<FB_User>) data);
                                mAdapter.notifyDataSetChanged();
                            }
                        });
                    }

                    @Override
                    public void onFail() {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            public void run() {
                                mProgress.setVisibility(View.GONE);
                                mContactList.setVisibility(View.VISIBLE);

                                mAdapter.clear();
                                mAdapter.notifyDataSetChanged();
                            }
                        });
                    }
                });
                return false;
            }
        });

        mContactList.setupMoreListener(new OnMoreListener() {
            @Override
            public void onMoreAsked(int numberOfItems, int numberBeforeMore, int currentItemPos) {
                loadMoreEvents();
            }}, 2);
    }

    void loadMoreEvents() {
        mService.getContactsWithUniversityID(curUser.studyingInformation.universityID,
                (mAdapter.getItemCount() - 1) + "", new ContactService.FilterContactsCallback() {
            @Override
            public void onFilterSuccess(final List<FB_User> data) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public void run() {
                        mProgress.setVisibility(View.GONE);
                        mContactList.setVisibility(View.VISIBLE);

                        if (data.size() < 20) {
                            mContactList.removeMoreListener();
                        }

                        mAdapter.addAll((ArrayList<FB_User>) data);
                        mAdapter.notifyDataSetChanged();
                    }
                });
            }

            @Override
            public void onFail() {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public void run() {
                        mProgress.setVisibility(View.GONE);
                        mContactList.setVisibility(View.VISIBLE);

                        mAdapter.clear();
                        mAdapter.notifyDataSetChanged();
                    }
                });
            }
        });
    }

    void loadData() {
        String filterUID = mService.getCurrentFilterUniversityID();
        if (filterUID != null && !filterUID.trim().isEmpty()) {
            mUniversityName.setText(UniversityService.getInstance().getUniversityName(filterUID));
            mService.getContactsWithAppliedFilters(new ContactService.FilterContactsCallback() {
                @Override
                public void onFilterSuccess(final List<FB_User> data) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        public void run() {
                            mProgress.setVisibility(View.GONE);
                            mContactList.setVisibility(View.VISIBLE);

                            mAdapter.clear();
                            if (data.size() < 20) {
                                mContactList.removeMoreListener();
                            }

                            mAdapter.addAll((ArrayList<FB_User>) data);
                            mAdapter.notifyDataSetChanged();
                        }
                    });
                }

                @Override
                public void onFail() {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        public void run() {
                            mProgress.setVisibility(View.GONE);
                            mContactList.setVisibility(View.VISIBLE);

                            mAdapter.clear();
                            mAdapter.notifyDataSetChanged();
                        }
                    });
                }
            });
        }
        else {
            mUniversityName.setText(UniversityService.getInstance().getUniversityName(curUser.studyingInformation.universityID));
            mService.getContactsWithUniversityID(curUser.studyingInformation.universityID, currentPage + "", new ContactService.FilterContactsCallback() {
                @Override
                public void onFilterSuccess(final List<FB_User> data) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        public void run() {
                            mProgress.setVisibility(View.GONE);
                            mContactList.setVisibility(View.VISIBLE);

                            mAdapter.clear();
                            if (data.size() < 20) {
                                mContactList.removeMoreListener();
                            }

                            mAdapter.addAll((ArrayList<FB_User>) data);
                            mAdapter.notifyDataSetChanged();
                        }
                    });
                }

                @Override
                public void onFail() {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        public void run() {
                            mProgress.setVisibility(View.GONE);
                            mContactList.setVisibility(View.VISIBLE);

                            mAdapter.clear();
                            mAdapter.notifyDataSetChanged();
                        }
                    });
                }
            });
        }
    }
}
