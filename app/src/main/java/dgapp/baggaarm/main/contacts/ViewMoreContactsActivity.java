package dgapp.baggaarm.main.contacts;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.malinskiy.superrecyclerview.SuperRecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import dgapp.baggaarm.R;
import dgapp.baggaarm.main.contacts.adapter.ContactRecycleAdapter;
import dgapp.baggaarm.service.AuthenticationService;
import dgapp.baggaarm.service.ContactService;
import dgapp.baggaarm.service.UniversityService;
import dgapp.baggaarm.service.model.firebase.FB_Speciality;
import dgapp.baggaarm.service.model.firebase.FB_University;
import dgapp.baggaarm.service.model.firebase.FB_User;
import dgapp.baggaarm.utils.BaseActivity;

public class ViewMoreContactsActivity extends BaseActivity {

    ImageButton mBack;

    SuperRecyclerView mContactList;
    ContactRecycleAdapter mAdapter;

    ProgressBar mProgress;

    ContactService mService = ContactService.getInstance();
    FB_User curUser;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_more_contacts);

        curUser = AuthenticationService.getInstance(getBaseContext()).getCurrentUser();

        bindingControls();
        setupControlsEvent();
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();

        loadData();
    }

    void bindingControls() {
        mBack = (ImageButton) findViewById(R.id.ib_close);
        mContactList = (SuperRecyclerView) findViewById(R.id.srv_contact_list);
        mProgress = (ProgressBar) findViewById(R.id.pb_progress);
    }

    void setupControlsEvent() {
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    void init() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getBaseContext());
        mContactList.setLayoutManager(mLayoutManager);

        mAdapter = new ContactRecycleAdapter(getBaseContext(), new ArrayList<FB_User>());
        mContactList.setAdapter(mAdapter);
    }

    void loadData() {
        mService.getContactsIDWithCurrentFilter(curUser.studyingInformation.universityID, new ContactService.FilterContactsCallback() {
            @Override
            public void onFilterSuccess(final List<FB_User> data) {
                ViewMoreContactsActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mProgress.setVisibility(View.GONE);
                        mContactList.setVisibility(View.VISIBLE);

                        mAdapter.clear();

                        mAdapter.addAll((ArrayList<FB_User>) data);
                        mAdapter.notifyDataSetChanged();
                    }
                });
            }

            @Override
            public void onFail() {
                ViewMoreContactsActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mProgress.setVisibility(View.GONE);
                        mContactList.setVisibility(View.VISIBLE);

                        mAdapter.clear();
                        mAdapter.notifyDataSetChanged();
                    }
                });
            }
        });
    }
}
