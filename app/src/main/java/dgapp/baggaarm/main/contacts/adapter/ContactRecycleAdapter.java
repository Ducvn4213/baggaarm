package dgapp.baggaarm.main.contacts.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;

import dgapp.baggaarm.R;
import dgapp.baggaarm.main.account.follow.adapter.FollowerListAdapter;
import dgapp.baggaarm.main.home.child.event.adapter.EventRecycleAdapter;
import dgapp.baggaarm.service.UniversityService;
import dgapp.baggaarm.service.model.firebase.FB_User;

public class ContactRecycleAdapter extends RecyclerView.Adapter<ContactRecycleAdapter.MyViewHolder> {

    public static interface OnRequestViewMoreResults {
        void onRequestViewMoreResults();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout container;
        public ImageView avatar;
        public TextView name;
        public TextView info;
        public Button viewFacebook;
        public Button viewMore;
        public View mainView;

        public MyViewHolder(View view) {
            super(view);

            mainView = view;
            container = (LinearLayout) view.findViewById(R.id.ll_container);
            avatar = (ImageView) view.findViewById(R.id.iv_avatar);
            name = (TextView) view.findViewById(R.id.tv_name);
            info = (TextView) view.findViewById(R.id.tv_info);
            viewFacebook = (Button) view.findViewById(R.id.btn_view_facebook);
            viewMore = (Button) view.findViewById(R.id.btn_view_more);

            setIsRecyclable(false);
        }
    }

    EventRecycleAdapter.OnItemClickListener onItemClickListener;
    OnRequestViewMoreResults onRequestViewMoreResults;
    FollowerListAdapter.OnFollowerItemNeedViewFacebook onRequestOpenFacebook;
    FirebaseStorage storage = FirebaseStorage.getInstance();
    StorageReference storageRef = storage.getReference();
    public ArrayList<FB_User> mData;
    private Context mContext;

    public ContactRecycleAdapter(Context context, ArrayList<FB_User> data) {
        this.mData = data;
        this.mContext = context;
    }

    public void setOnItemClickListener(EventRecycleAdapter.OnItemClickListener callback) {
        onItemClickListener = callback;
    }

    public void setOnRequestOpenFacebook(FollowerListAdapter.OnFollowerItemNeedViewFacebook callback) {
        onRequestOpenFacebook = callback;
    }

    public void setOnRequestViewMoreResults(OnRequestViewMoreResults callback) {
        onRequestViewMoreResults = callback;
    }

    @Override
    public ContactRecycleAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_follower_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.setIsRecyclable(false);
        final FB_User data = mData.get(position);

        if (data == null) {
            holder.viewMore.setVisibility(View.VISIBLE);
            holder.container.setVisibility(View.GONE);

            holder.viewMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onRequestViewMoreResults.onRequestViewMoreResults();
                }
            });

            return;
        }

        holder.mainView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClickListener.onItemClickListener(position);
            }
        });
        holder.container.setVisibility(View.VISIBLE);
        holder.viewMore.setVisibility(View.GONE);

        holder.viewFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onRequestOpenFacebook.onRequestOpen("http://facebook.com/" + data.facebookID, data.displayName);
            }
        });

        holder.name.setText(data.displayName);

        if (data.facebookID == null || data.facebookID.isEmpty()) {
            holder.viewFacebook.setVisibility(View.GONE);
        }
        else {
            holder.viewFacebook.setVisibility(View.VISIBLE);
        }

        String uniName = UniversityService.getInstance().getUniversityName(data.studyingInformation.universityID);
        holder.info.setText(uniName + " • K " + data.studyingInformation.startYear);

        Picasso.with(mContext).cancelRequest(holder.avatar);
        if (!data.profileImageURL.isEmpty()) {
            int startIndex = data.profileImageURL.indexOf("profileImages");
            if (startIndex < 0) {
                Picasso.with(mContext)
                        .load(data.profileImageURL)
                        .into(new Target() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                holder.avatar.setImageBitmap(bitmap);
                            }

                            @Override
                            public void onBitmapFailed(Drawable errorDrawable) {
                                holder.avatar.setImageDrawable(ContextCompat.getDrawable(mContext, R.mipmap.ic_avatar));
                            }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {
                                //TODO
                            }
                        });
            }
            else {
                int endIndex = data.profileImageURL.indexOf(".jpg");
                String child = data.profileImageURL.substring(startIndex, endIndex) + ".jpg";
                storageRef.child(child).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        Picasso.with(mContext).cancelRequest(holder.avatar);
                        Picasso.with(mContext).load(uri).into(holder.avatar);
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        holder.avatar.setImageDrawable(ContextCompat.getDrawable(mContext, R.mipmap.ic_avatar));
                    }
                });
            }
        }
        else {
            holder.avatar.setImageDrawable(ContextCompat.getDrawable(mContext, R.mipmap.ic_avatar));
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void add(FB_User data, int position) {
        insert(data, position);
    }

    public void add(FB_User data) {
        int thePos = mData.size();
        insert(data, thePos);
    }

    public void insert(FB_User data, int position) {
        this.mData.add(position, data);
        notifyItemInserted(position);
    }

    public void remove(int position) {
        mData.remove(position);
        notifyItemRemoved(position);
    }

    public FB_User getItem(int pos) {
        return mData.get(pos);
    }

    public void clear() {
        int size = mData.size();
        mData.clear();
        notifyItemRangeRemoved(0, size);
    }

    public void clearWithoutReloadUI() {
        mData.clear();
    }

    public void addAll(ArrayList<FB_User> data) {
        int startIndex = mData.size();
        mData.addAll(startIndex, data);
        notifyItemRangeInserted(startIndex, data.size());
    }
}
