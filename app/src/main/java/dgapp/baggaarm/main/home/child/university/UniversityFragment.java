package dgapp.baggaarm.main.home.child.university;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import dgapp.baggaarm.R;
import dgapp.baggaarm.main.home.HomeStaticCacheService;
import dgapp.baggaarm.main.home.child.university.university_list.adapter.UniversityFragmentAdapter;
import dgapp.baggaarm.utils.NonSwipeViewPager;
import dgapp.baggaarm.utils.SegmentedControl;

public class UniversityFragment extends Fragment {

    NonSwipeViewPager mViewPager;
    SegmentedControl mSegmentedControl;

    UniversityFragmentAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home_child_university, container, false);

        bindingControls(view);
        setupControlEvents();
        init();

        return view;
    }

    void bindingControls(View view) {
        mSegmentedControl = (SegmentedControl) view.findViewById(R.id.sc_university_segmented);
        mViewPager = (NonSwipeViewPager) view.findViewById(R.id.vp_container);
    }

    void setupControlEvents() {
        mSegmentedControl.setOnSegmentedChangedListener(new SegmentedControl.OnSegmentedChangedListener() {
            @Override
            public void onSegmentedChanged(int index) {
                HomeStaticCacheService.getInstance().universityTypeIndex = index;
                mViewPager.setCurrentItem(index, true);
            }
        });
    }

    void init() {
        mAdapter = new UniversityFragmentAdapter(getChildFragmentManager());
        mViewPager.setAdapter(mAdapter);

        mSegmentedControl.setTextA(getString(R.string.university_text_title));
        mSegmentedControl.setTextB(getString(R.string.university_image_title));
    }
}
