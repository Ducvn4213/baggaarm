package dgapp.baggaarm.main.home.child.room.room_list.new_room;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import dgapp.baggaarm.R;
import dgapp.baggaarm.main.home.child.room.room_list.adapter.AreaListAdapter;
import dgapp.baggaarm.service.RoomService;
import dgapp.baggaarm.service.model.AddNewRoomModel;
import dgapp.baggaarm.service.model.firebase.FB_RoomArea;
import dgapp.baggaarm.utils.BaseActivity;

public class NewRoomInfoActivity extends BaseActivity {

    ImageButton mBack;
    ImageButton mSave;
    EditText mAddress;
    EditText mWidth;
    EditText mHeight;
    EditText mCost;
    EditText mPhone;
    TextView mAreas;
    Button mAreasButton;

    RoomService mService = RoomService.getInstance();
    List<FB_RoomArea> areaData = new ArrayList<>();
    AddNewRoomModel addNewRoomModel = new AddNewRoomModel();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_room_info);

        bindingControls();
        setupControlEvents();
    }

    void bindingControls() {
        mBack = (ImageButton) findViewById(R.id.ib_back);
        mSave = (ImageButton) findViewById(R.id.ib_save);

        mAddress = (EditText) findViewById(R.id.edt_address);
        mWidth = (EditText) findViewById(R.id.edt_width);
        mHeight = (EditText) findViewById(R.id.edt_height);
        mCost = (EditText) findViewById(R.id.edt_cost);
        mPhone = (EditText) findViewById(R.id.edt_phone);

        mAreas = (TextView) findViewById(R.id.tv_areas);
        mAreasButton = (Button) findViewById(R.id.btn_choose_area);
    }

    void setupControlEvents() {
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewRoomInfoActivity.this.finish();
            }
        });

        mSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doSave();
            }
        });

        mAreasButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestSelectAreas();
            }
        });
    }

    void requestSelectAreas() {
        if (areaData.size() == 0) {
            mService.getAreas(new RoomService.GetAreasCallback() {
                @Override
                public void onSuccess(final List<FB_RoomArea> data) {
                    NewRoomInfoActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            areaData = data;
                            doRequestSelectAreas();
                        }
                    });
                }

                @Override
                public void onFail() {
                    //TODO: show error message
                }
            });
        }
        else {
            doRequestSelectAreas();
        }
    }

    void doRequestSelectAreas() {
        final Dialog dialog = new Dialog(NewRoomInfoActivity.this);
        dialog.setContentView(R.layout.layout_areas_selection);

        ListView listView = (ListView) dialog.findViewById(R.id.lv_area_items);
        Button ok = (Button) dialog.findViewById(R.id.btn_ok);
        final AreaListAdapter adapter = new AreaListAdapter(NewRoomInfoActivity.this, areaData);


        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                adapter.toggleSelection(position);
            }
        });

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Integer> selections = adapter.getSelection();
                List<String> selectionsString = new ArrayList<String>();
                String presentString = "";
                for (int s : selections) {
                    selectionsString.add(areaData.get(s).key);
                    presentString += areaData.get(s).displayID + ", ";
                }

                if (!presentString.isEmpty()) {
                    presentString = presentString.substring(0, presentString.length() - 2);
                    mAreas.setText(presentString);
                }

                addNewRoomModel.areas = selectionsString;


                dialog.dismiss();
            }
        });

        dialog.show();
    }

    void doSave() {
        String address = mAddress.getText().toString();
        String width = mWidth.getText().toString();
        String height = mHeight.getText().toString();
        String cost = mCost.getText().toString();
        String phone = mPhone.getText().toString();

        addNewRoomModel.address = address;
        addNewRoomModel.phone = phone;

        try {
            int intWidth = Integer.parseInt(width);
            addNewRoomModel.width = intWidth;
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

        try {
            int intHeight = Integer.parseInt(height);
            addNewRoomModel.height = intHeight;
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

        try {
            int intCost = Integer.parseInt(cost);
            addNewRoomModel.cost = intCost;
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }


        if (!addNewRoomModel.enoughData()) {
            showErrorDialog(getString(R.string.add_new_room_missing));
        }
        else {
            mService.setAddNewRoomInfoModel(addNewRoomModel);
            this.finish();
        }
    }
}
