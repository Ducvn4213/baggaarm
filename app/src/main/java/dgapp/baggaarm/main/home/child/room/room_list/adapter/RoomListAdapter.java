package dgapp.baggaarm.main.home.child.room.room_list.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import dgapp.baggaarm.R;
import dgapp.baggaarm.service.JobService;
import dgapp.baggaarm.service.model.firebase.FB_Job;
import dgapp.baggaarm.service.model.firebase.FB_Room;
import dgapp.baggaarm.utils.Utils;

public class RoomListAdapter extends ArrayAdapter<FB_Room> {

    private Context mContext;
    private List<FB_Room> mData;
    private static LayoutInflater mInflater = null;
    private int displayMode = 0;

    public RoomListAdapter(Context context, List<FB_Room> data) {
        super(context, R.layout.layout_room_grid_item, data);

        mContext = context;
        mData = data;

        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return mData.size();
    }

    public long getItemId(int position) {
        return position;
    }

    public void setDisplayMode(int mode) {
        displayMode = mode;
        notifyDataSetInvalidated();
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = null;
        if(view == null) {
            if (displayMode == 0) {
                view = mInflater.inflate(R.layout.layout_room_grid_item, null);
            }
            else {
                view = mInflater.inflate(R.layout.layout_room_list_item, null);
            }
        }

        FB_Room data = mData.get(position);

        ImageView logo = (ImageView) view.findViewById(R.id.iv_image);
        TextView address = (TextView) view.findViewById(R.id.tv_address);
        TextView size = (TextView) view.findViewById(R.id.tv_size);
        TextView cost = (TextView) view.findViewById(R.id.tv_cost);
        TextView phone = (TextView) view.findViewById(R.id.tv_phone);

        address.setText(data.roomInformation.address);
        size.setText(data.roomInformation.length + " * " + data.roomInformation.width + " (D * R)");
        cost.setText(Utils.numberToVND(data.roomInformation.price + "") + " " + mContext.getString(R.string.vnd_unit));
        phone.setText(data.roomInformation.contactPhoneNumber);

        if (!data.imgURL.isEmpty()) {
            Picasso.with(mContext).load(data.imgURL).into(logo);
        }
        else {
            logo.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.placeholder_image));
        }
        return view;
    }
}
