package dgapp.baggaarm.main.home;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import dgapp.baggaarm.MainActivity;
import dgapp.baggaarm.R;
import dgapp.baggaarm.main.home.adapter.HomeFragmentAdapter;
import dgapp.baggaarm.main.home.child.HomeChildFragment;
import dgapp.baggaarm.utils.NonSwipeViewPager;

public class HomeFragment extends Fragment implements MainActivity.PassBackHandle {

    public enum HOME_FEATURE {
        JOB,
        ROOM,
        UNIVERSITY,
        EVENT
    }

    NonSwipeViewPager mViewPager;
    HOME_FEATURE mHomeFeature = HOME_FEATURE.JOB;

    HomeFragmentAdapter mAdapter;

    MainActivity.PassBackHandle mPassBackHandle;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        bindingControls(view);
        init();

        return view;
    }

    void bindingControls(View view) {
        mViewPager = (NonSwipeViewPager) view.findViewById(R.id.vp_container);
    }

    void init() {
        mAdapter = new HomeFragmentAdapter(getChildFragmentManager());
        mViewPager.setAdapter(mAdapter);
    }

    public void onSelectFeature(HOME_FEATURE feature) {
        mHomeFeature = feature;
        mViewPager.setCurrentItem(1, true);
    }

    public HOME_FEATURE getCurrentHomeFeature() {
        return mHomeFeature;
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible) {
            MainActivity.getInstance().setPassBackHandle(this);
        }
    }

    public void setPassBackHandle(MainActivity.PassBackHandle passBackHandle) {
        mPassBackHandle = passBackHandle;
    }

    @Override
    public void doBack(boolean onDeviceBackButton) {
        int currentItem = mViewPager.getCurrentItem();
        if (currentItem != 0 && mPassBackHandle!= null) {
            mPassBackHandle.doBack(false);
        }

        if (currentItem == 0 && onDeviceBackButton) {
            MainActivity.getInstance().forceDoBack();
        }
    }

    public void forceDoBack() {
        int currentItem = mViewPager.getCurrentItem();
        if (currentItem > 0) {
            currentItem = currentItem - 1;
            mViewPager.setCurrentItem(currentItem, true)    ;
        }
    }
}
