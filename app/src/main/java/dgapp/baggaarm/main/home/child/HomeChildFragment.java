package dgapp.baggaarm.main.home.child;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import dgapp.baggaarm.MainActivity;
import dgapp.baggaarm.R;
import dgapp.baggaarm.main.home.HomeFragment;
import dgapp.baggaarm.main.home.HomeStaticCacheService;
import dgapp.baggaarm.main.home.child.adapter.HomeChildFragmentAdapter;
import dgapp.baggaarm.main.home.child.event.EventsFragment;
import dgapp.baggaarm.main.home.child.job.JobsFragment;
import dgapp.baggaarm.main.home.child.job.job_list.intern.InternshipFragment;
import dgapp.baggaarm.main.home.child.job.job_list.part_time.PartTimeFragment;
import dgapp.baggaarm.main.home.child.room.RoomsFragment;
import dgapp.baggaarm.main.home.child.university.UniversityFragment;
import dgapp.baggaarm.utils.NonSwipeViewPager;

public class HomeChildFragment extends Fragment implements MainActivity.PassBackHandle {

    NonSwipeViewPager mViewPager;
    HomeChildFragmentAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home_child, container, false);

        bindingControls(view);
        init();

        return view;
    }

    void bindingControls(View view) {
        mViewPager = (NonSwipeViewPager) view.findViewById(R.id.vp_container);
    }

    void init() {
        mAdapter = new HomeChildFragmentAdapter(getChildFragmentManager());
        mViewPager.setAdapter(mAdapter);
    }

    HomeFragment getParentHomeFragment() {
        return (HomeFragment) getParentFragment();
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible) {
            getParentHomeFragment().setPassBackHandle(this);

            int index = HomeStaticCacheService.getInstance().homeFeatureIndex;
            mViewPager.setCurrentItem(index, false);

            if (index == 0) {
                if (HomeStaticCacheService.getInstance().jobTypeIndex == 0 && InternshipFragment.getInstance() != null) {
                    InternshipFragment.getInstance().onResume();
                    return;
                }


                if (HomeStaticCacheService.getInstance().jobTypeIndex == 1 && PartTimeFragment.getInstance() != null) {
                    PartTimeFragment.getInstance().onResume();
                    return;
                }
            }
            else if (index == 3) {
                if (EventsFragment.getInstance() != null) {
                    EventsFragment.getInstance().onResume();
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        int index = HomeStaticCacheService.getInstance().homeFeatureIndex;
        mViewPager.setCurrentItem(index, false);
    }

    @Override
    public void doBack(boolean onDeviceBackButton) {
        getParentHomeFragment().forceDoBack();
    }
}
