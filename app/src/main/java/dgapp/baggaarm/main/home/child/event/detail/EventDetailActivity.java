package dgapp.baggaarm.main.home.child.event.detail;


import android.Manifest;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.Image;
import android.media.MediaDataSource;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import dgapp.baggaarm.R;
import dgapp.baggaarm.service.EventService;
import dgapp.baggaarm.service.callback.CommonCallback;
import dgapp.baggaarm.service.model.firebase.FB_Event;
import dgapp.baggaarm.utils.BaseActivity;
import dgapp.baggaarm.utils.EventHostItem;
import dgapp.baggaarm.utils.WebViewActivity;

public class EventDetailActivity extends BaseActivity {

    ImageView mImage;
    TextView mName;
    TextView mAddress;
    TextView mTime;
    TextView mPrice;
    TextView mDescription;
    Button mBook;
    ImageButton mOpenFace;
    EventHostItem mHost;
    ImageButton mBack;
    ImageButton mAddEvent;

    ImageView mInterestingImage;
    ImageView mSavesImage;
    ImageView mWillGOImage;

    TextView mInterestingCount;
    TextView mSavesCount;
    TextView mWillGOCount;

    LinearLayout mInterestingContainer;
    LinearLayout mSavesContainer;
    LinearLayout mWillGoContainer;

    ProgressBar mProgress;
    LinearLayout mContent;


    EventService mService = EventService.getInstance();
    FB_Event mData;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);

        bindingControls();
        setupControlEvents();
    }

    @Override
    protected void onResume() {
        super.onResume();

        bindData();
    }

    void bindingControls() {
        mProgress = (ProgressBar) findViewById(R.id.pb_progress);
        mContent = (LinearLayout) findViewById(R.id.ll_content);

        mBack = (ImageButton) findViewById(R.id.ib_back);
        mAddEvent = (ImageButton) findViewById(R.id.ib_add_event);

        mImage = (ImageView) findViewById(R.id.iv_image);
        mName = (TextView) findViewById(R.id.tv_event_name);
        mAddress = (TextView) findViewById(R.id.tv_event_address);
        mTime = (TextView) findViewById(R.id.tv_event_time);
        mPrice = (TextView) findViewById(R.id.tv_event_price);
        mDescription = (TextView) findViewById(R.id.tv_event_description);
        mBook = (Button) findViewById(R.id.btn_book);
        mHost = (EventHostItem) findViewById(R.id.ehi_host);
        mOpenFace = (ImageButton) findViewById(R.id.ib_open_face);

        mInterestingImage = (ImageView) findViewById(R.id.iv_interesting_count);
        mSavesImage = (ImageView) findViewById(R.id.iv_saved_count);
        mWillGOImage = (ImageView) findViewById(R.id.iv_will_go_count);

        mInterestingCount = (TextView) findViewById(R.id.tv_interesting_count);
        mSavesCount = (TextView) findViewById(R.id.tv_saved_count);
        mWillGOCount = (TextView) findViewById(R.id.tv_go_count);

        mInterestingContainer = (LinearLayout) findViewById(R.id.ll_interesting_container);
        mSavesContainer = (LinearLayout) findViewById(R.id.ll_saves_container);
        mWillGoContainer = (LinearLayout) findViewById(R.id.ll_will_go_container);
    }

    void setupControlEvents() {
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventDetailActivity.this.finish();
            }
        });

        mAddEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveToCalendar();
            }
        });

        mHost.setOnEventHostItemViewMoreClickListenter(new EventHostItem.OnEventHostItemViewMoreClick() {
            @Override
            public void onViewMoreClick() {
                //TODO
            }
        });

        mBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EventDetailActivity.this, WebViewActivity.class);
                intent.putExtra("url", mData.extend.bookURL);
                startActivity(intent);
            }
        });

        mOpenFace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EventDetailActivity.this, WebViewActivity.class);
                intent.putExtra("url", mData.extend.infoURL);
                startActivity(intent);
            }
        });

        mInterestingContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mData.isInteresting) {
                    mService.unInterestEvent(mData.key, new CommonCallback() {
                        @Override
                        public void onCompleted() {
                            mData.isInteresting = false;
                            mData.interestingCount -= 1;
                            updateActivityInfo();
                        }

                        @Override
                        public void onFail() {
                            //TODO
                        }
                    });
                } else {
                    mService.interestEvent(mData.key, new CommonCallback() {
                        @Override
                        public void onCompleted() {
                            mData.isInteresting = true;
                            mData.interestingCount += 1;
                            updateActivityInfo();
                        }

                        @Override
                        public void onFail() {

                        }
                    });
                }
            }
        });

        mSavesContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mData.isSaved) {
                    mService.unSaveEvent(mData.key, new CommonCallback() {
                        @Override
                        public void onCompleted() {
                            mData.isSaved = false;
                            mData.saveCount -= 1;
                            updateActivityInfo();
                        }

                        @Override
                        public void onFail() {
                            //TODO
                        }
                    });
                } else {
                    mService.saveEvent(mData.extend.hostID, mData.key, new CommonCallback() {
                        @Override
                        public void onCompleted() {
                            mData.isSaved = true;
                            mData.saveCount += 1;
                            updateActivityInfo();
                        }

                        @Override
                        public void onFail() {

                        }
                    });
                }
            }
        });

        mWillGoContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mData.isWillGo) {
                    mService.unWillGoEvent(mData.key, new CommonCallback() {
                        @Override
                        public void onCompleted() {
                            mData.isWillGo = false;
                            mData.willGoCount -= 1;
                            updateActivityInfo();
                        }

                        @Override
                        public void onFail() {
                            //TODO
                        }
                    });
                } else {
                    mService.willGoEvent(mData.key, new CommonCallback() {
                        @Override
                        public void onCompleted() {
                            mData.isWillGo = true;
                            mData.willGoCount += 1;
                            updateActivityInfo();
                        }

                        @Override
                        public void onFail() {

                        }
                    });
                }
            }
        });

        mHost.setOnEventHostItemViewMoreClickListenter(new EventHostItem.OnEventHostItemViewMoreClick() {
            @Override
            public void onViewMoreClick() {
                Intent intent = new Intent(EventDetailActivity.this, ClubDetailActivity.class);
                intent.putExtra("key", mData.key);
                startActivity(intent);
            }
        });
    }

    void updateActivityInfo() {
        mInterestingCount.setText(mData.interestingCount + "");
        mSavesCount.setText(mData.saveCount + "");
        mWillGOCount.setText(mData.willGoCount + "");

        if (mData.isInteresting) {
            mInterestingImage.setColorFilter(ContextCompat.getColor(EventDetailActivity.this, R.color.colorPrimary));
        } else {
            mInterestingImage.setColorFilter(ContextCompat.getColor(EventDetailActivity.this, android.R.color.darker_gray));
        }

        if (mData.isSaved) {
            mSavesImage.setColorFilter(ContextCompat.getColor(EventDetailActivity.this, R.color.colorPrimary));
        } else {
            mSavesImage.setColorFilter(ContextCompat.getColor(EventDetailActivity.this, android.R.color.darker_gray));
        }

        if (mData.isWillGo) {
            mWillGOImage.setColorFilter(ContextCompat.getColor(EventDetailActivity.this, R.color.colorPrimary));
        } else {
            mWillGOImage.setColorFilter(ContextCompat.getColor(EventDetailActivity.this, android.R.color.darker_gray));
        }
    }

    void bindData() {
        String key = getIntent().getStringExtra("key");
        mService.getEventDetail(key, new EventService.GetEventsCallback() {
            @Override
            public void onSuccess(final List<FB_Event> data) {
                EventDetailActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mData = data.get(0);

                        Picasso.with(EventDetailActivity.this).load(mData.imgURL).into(mImage);

                        mName.setText(mData.title);
                        mAddress.setText(mData.address);
                        mTime.setText(mData.time.displayTime);
                        mPrice.setText(mData.ticket.price + "");

                        mDescription.setText(mData.extend.description);
                        mHost.setData(data.get(0).host);

                        updateActivityInfo();

                        mProgress.setVisibility(View.GONE);
                        mContent.setVisibility(View.VISIBLE);
                    }
                });
            }

            @Override
            public void onFail() {
                //TODO
            }
        });
    }

    public void saveToCalendar() {
        getCalendarPersmissionAndContinue();
    }

    @Override
    protected void doContinue() {
        super.doContinue();

        ContentResolver cr = EventDetailActivity.this.getContentResolver();
        ContentValues values = new ContentValues();

        values.put(CalendarContract.Events.DTSTART, correctDateString(mData.time.beginDate));
        values.put(CalendarContract.Events.DTEND, correctDateString(mData.time.endDate));
        values.put(CalendarContract.Events.TITLE, mData.title);
        values.put(CalendarContract.Events.DESCRIPTION, mData.extend.description);
        //values.put(CalendarContract.Events.CALENDAR_ID, );

        Toast.makeText(EventDetailActivity.this, getCalendarID(getApplicationContext()), Toast.LENGTH_LONG).show();

        TimeZone timeZone = TimeZone.getDefault();
        //Toast.makeText(EventDetailActivity.this, timeZone.getID(), Toast.LENGTH_LONG).show();

        values.put(CalendarContract.Events.EVENT_TIMEZONE, timeZone.getID());



        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        //cr.insert(CalendarContract.Events.CONTENT_URI, values);
        showDialog(getString(R.string.app_name), getString(R.string.add_event_completed));
    }

    @Override
    protected void cantContinue() {
        super.cantContinue();
    }

    private long correctDateString(String input) {
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        try {
            Date date = format.parse(input);
            return date.getTime() * 1000;
            //SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMdd");
            //String datetime = dateformat.format(date);
            //return datetime;
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public String getCalendarID(Context c) {

        String projection[] = {"_id", "calendar_displayName"};
        Uri calendars;
        calendars = Uri.parse("content://com.android.calendar/calendars");

        ContentResolver contentResolver = c.getContentResolver();
        Cursor managedCursor = contentResolver.query(calendars, projection, null, null, null);

        String calID = null;
        if (managedCursor.moveToFirst()){
            int idCol = managedCursor.getColumnIndex(projection[0]);
            do {
                calID = managedCursor.getString(idCol);

                if (calID != null && !calID.trim().isEmpty()) {
                    return calID;
                }
            } while(managedCursor.moveToNext());
            managedCursor.close();
        }
        return calID;

    }
}
