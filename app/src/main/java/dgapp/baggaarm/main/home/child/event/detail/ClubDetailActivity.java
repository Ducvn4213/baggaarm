package dgapp.baggaarm.main.home.child.event.detail;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import dgapp.baggaarm.R;
import dgapp.baggaarm.main.home.child.event.adapter.EventListAdapter;
import dgapp.baggaarm.main.home.child.event.saved.SavedEventsActivity;
import dgapp.baggaarm.service.EventService;
import dgapp.baggaarm.service.callback.CommonCallback;
import dgapp.baggaarm.service.model.firebase.FB_Event;
import dgapp.baggaarm.utils.BaseActivity;
import dgapp.baggaarm.utils.EventHostItem;
import dgapp.baggaarm.utils.Utils;
import dgapp.baggaarm.utils.WebViewActivity;

public class ClubDetailActivity extends BaseActivity {

    ClubDetailHeader mHeader;
    ImageButton mBack;

    ListView mEventList;
    EventListAdapter mAdapter;

    LinearLayout mContent;
    ProgressBar mProgress;

    EventService mService = EventService.getInstance();
    FB_Event mData;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_club_detail);

        bindingControls();
        setupControlEvents();
    }

    @Override
    protected void onResume() {
        super.onResume();

        bindData();
    }

    void bindingControls() {
        mProgress = (ProgressBar) findViewById(R.id.pb_progress);
        mContent = (LinearLayout) findViewById(R.id.ll_content);
        mBack = (ImageButton) findViewById(R.id.ib_back);

        mEventList = (ListView) findViewById(R.id.lv_event_list);
    }

    void setupControlEvents() {
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClubDetailActivity.this.finish();
            }
        });

        mEventList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(ClubDetailActivity.this, EventDetailActivity.class);
                intent.putExtra("key", mAdapter.getItem(position).key);
                startActivity(intent);
            }
        });
    }

    void bindData() {
        String key = getIntent().getStringExtra("key");
        mService.getEventDetail(key, new EventService.GetEventsCallback() {
            @Override
            public void onSuccess(final List<FB_Event> data) {
                ClubDetailActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mData = data.get(0);

                        mService.getEventsFromClub(mData.extend.hostID, new EventService.GetEventsCallback() {
                            @Override
                            public void onSuccess(List<FB_Event> data) {
                                if (mAdapter == null) {
                                    mProgress.setVisibility(View.GONE);
                                    mContent.setVisibility(View.VISIBLE);

                                    mHeader = new ClubDetailHeader(ClubDetailActivity.this);
                                    mHeader.setData(mData);
                                    mEventList.addHeaderView(mHeader);

                                    mAdapter = new EventListAdapter(ClubDetailActivity.this, data);
                                    mEventList.setAdapter(mAdapter);
                                }
                                else {
                                    mAdapter.clear();
                                    mAdapter.addAll(data);
                                    mAdapter.notifyDataSetChanged();
                                }
                            }

                            @Override
                            public void onFail() {
                                //TODO
                            }
                        });
                    }
                });
            }

            @Override
            public void onFail() {
                //TODO
            }
        });
    }
}
