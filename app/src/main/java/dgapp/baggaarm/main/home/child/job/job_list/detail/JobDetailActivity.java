package dgapp.baggaarm.main.home.child.job.job_list.detail;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import dgapp.baggaarm.R;
import dgapp.baggaarm.service.JobService;
import dgapp.baggaarm.service.callback.CommonCallback;
import dgapp.baggaarm.service.model.firebase.FB_Job;
import dgapp.baggaarm.utils.BaseActivity;

public class JobDetailActivity extends BaseActivity {

    ProgressBar mProgress;
    LinearLayout mContent;

    ImageButton mBack;
    ImageView mLogo;
    ImageButton mSaveJob;
    TextView mCompanyName;
    TextView mJobTitle;
    TextView mJobSalary;
    TextView mJobLocation;
    TextView mJobMajor;
    TextView mJobTime;
    TextView mJobRequirement;
    TextView mJobDescription;
    TextView mJobBenefit;
    TextView mJobNote;
    TextView mContactName;
    TextView mContactPhone;
    TextView mContactMail;
    TextView mContactLocation;
    LinearLayout mContactPhoneContainer;
    LinearLayout mContactMailContainer;


    JobService.JOB_TYPE type;
    JobService mService = JobService.getInstance();
    FB_Job mJob;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_child_job_detail);

        bindingControls();
        setupControlEvents();
        init();
    }

    void bindingControls() {
        mProgress = (ProgressBar) findViewById(R.id.pb_progress);
        mContent = (LinearLayout) findViewById(R.id.ll_content);
        mBack = (ImageButton) findViewById(R.id.ib_back);
        mLogo = (ImageView) findViewById(R.id.iv_logo);
        mSaveJob = (ImageButton) findViewById(R.id.ib_save_job);
        mCompanyName = (TextView) findViewById(R.id.tv_company_name);
        mJobTitle = (TextView) findViewById(R.id.tv_job_title);
        mJobSalary = (TextView) findViewById(R.id.tv_salary);
        mJobLocation = (TextView) findViewById(R.id.tv_company_location);
        mJobMajor = (TextView) findViewById(R.id.tv_job_major);
        mJobTime = (TextView) findViewById(R.id.tv_job_time);
        mJobRequirement = (TextView) findViewById(R.id.tv_job_requirement);
        mJobDescription = (TextView) findViewById(R.id.tv_job_description);
        mJobBenefit = (TextView) findViewById(R.id.tv_job_benefit);
        mJobNote = (TextView) findViewById(R.id.tv_job_note);
        mContactName = (TextView) findViewById(R.id.tv_contact_name);
        mContactPhone = (TextView) findViewById(R.id.tv_contact_phone);
        mContactMail = (TextView) findViewById(R.id.tv_contact_email);
        mContactLocation = (TextView) findViewById(R.id.tv_contact_location);
        mContactPhoneContainer = (LinearLayout) findViewById(R.id.ll_contact_phone);
        mContactMailContainer = (LinearLayout) findViewById(R.id.ll_contact_mail);
    }

    void setupControlEvents() {
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JobDetailActivity.this.finish();
            }
        });

        mContactPhoneContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + mJob.phoneNumberOfEmployer));
                startActivity(intent);
            }
        });

        mContactMailContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                Uri data = Uri.parse("mailto:" + mJob.emailOfEmployer + "?subject=" + "" + "&body=" + "");
                intent.setData(data);
                startActivity(intent);
            }
        });

        mSaveJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mJob.isSaved) {
                    mService.unSaveJob(JobDetailActivity.this.type, mJob.key, new CommonCallback() {
                        @Override
                        public void onCompleted() {
                            mSaveJob.setColorFilter(ContextCompat.getColor(JobDetailActivity.this, android.R.color.darker_gray));
                            mJob.isSaved = false;
                        }

                        @Override
                        public void onFail() {
                            //TODO
                        }
                    });
                }
                else {
                    mService.saveJob(JobDetailActivity.this.type, mJob.key, new CommonCallback() {
                        @Override
                        public void onCompleted() {
                            mSaveJob.setColorFilter(ContextCompat.getColor(JobDetailActivity.this, R.color.colorPrimary));
                            mJob.isSaved = true;
                        }

                        @Override
                        public void onFail() {
                            //TODO
                        }
                    });
                }
            }
        });
    }

    void init() {
        String key = getIntent().getStringExtra("key");
        String type = getIntent().getStringExtra("type");

        if (type.equalsIgnoreCase("intern")) {
            this.type = JobService.JOB_TYPE.INTERN;
        }
        else {
            this.type = JobService.JOB_TYPE.PART_TIME;
        }

        mService.getJobDetail(key, new JobService.GetJobCallback() {
            @Override
            public void onCompleted(FB_Job job) {
                updateUI(job);
            }

            @Override
            public void onFail() {
                //TODO
            }
        });
    }

    void updateUI(FB_Job job) {
        mJob = job;

        mProgress.setVisibility(View.GONE);
        mContent.setVisibility(View.VISIBLE);

        if (!job.companyLogoURL.isEmpty()) {
            Picasso.with(JobDetailActivity.this).load(job.companyLogoURL).into(mLogo);
        }

        if (job.isSaved) {
            mSaveJob.setColorFilter(ContextCompat.getColor(JobDetailActivity.this, R.color.colorPrimary));
        }
        else {
            mSaveJob.setColorFilter(ContextCompat.getColor(JobDetailActivity.this, android.R.color.darker_gray));
        }

        mCompanyName.setText(job.companyName);
        mJobTitle.setText(job.titleOfJob);
        mJobSalary.setText(job.salary);
        mJobLocation.setText(job.addressOfJob);
        mJobMajor.setText(mService.getNameCategoryByKey(job.jobCategoryID));
        mJobTime.setText(job.timeOfJob);
        mJobRequirement.setText(job.jobRequirements);
        mJobDescription.setText(job.jobDescription);
        mJobBenefit.setText(job.jobWelfare);
        mJobNote.setText(job.note);
        mContactName.setText(job.employerName);
        mContactPhone.setText(job.phoneNumberOfEmployer);
        mContactMail.setText(job.emailOfEmployer);
        mContactLocation.setText(job.addressOfEmployer);
    }
}
