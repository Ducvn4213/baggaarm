package dgapp.baggaarm.main.home.adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import dgapp.baggaarm.main.home.child.HomeChildFragment;
import dgapp.baggaarm.main.home.main.HomeMainFragment;

public class HomeFragmentAdapter extends FragmentStatePagerAdapter {

    public HomeFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new HomeMainFragment();
            case 1:
                return new HomeChildFragment();
            default: break;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
