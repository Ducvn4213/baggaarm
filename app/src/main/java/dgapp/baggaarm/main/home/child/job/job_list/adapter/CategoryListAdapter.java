package dgapp.baggaarm.main.home.child.job.job_list.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import dgapp.baggaarm.R;
import dgapp.baggaarm.service.JobService;
import dgapp.baggaarm.service.model.firebase.FB_Job;
import dgapp.baggaarm.service.model.firebase.FB_JobCategory;

public class CategoryListAdapter extends ArrayAdapter<FB_JobCategory> {

    private Context mContext;
    private List<FB_JobCategory> mData;
    private static LayoutInflater mInflater = null;

    private List<Integer> selection = new ArrayList<>();

    public CategoryListAdapter(Context context, List<FB_JobCategory> data) {
        super(context, R.layout.layout_category_item, data);

        mContext = context;
        mData = data;

        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void toggleSelection(int index) {
        int existedIndex = selection.indexOf(index);
        if (existedIndex < 0) {
            selection.add(index);
        }
        else {
            selection.remove(existedIndex);
        }

        notifyDataSetInvalidated();
    }

    public List<String> getFilterKeys() {
        List<String> returnData = new ArrayList<>();
        for (int index : selection) {
            returnData.add(mData.get(index).key);
        }

        return returnData;
    }

    public List<Integer> getSelection() {
        return selection;
    }

    public int getCount() {
        return mData.size();
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if(view == null) {
            view = mInflater.inflate(R.layout.layout_category_item, null);
        }

        FB_JobCategory data = mData.get(position);


        TextView name = (TextView) view.findViewById(R.id.tv_name);

        name.setText(data.fullName);

        int existedIndex = selection.indexOf(position);
        if (existedIndex < 0) {
            view.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorWhite));
        }
        else {
            view.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
        }

        return view;
    }
}
