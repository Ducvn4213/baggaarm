package dgapp.baggaarm.main.home.child.event.saved;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import dgapp.baggaarm.R;
import dgapp.baggaarm.main.home.child.event.adapter.EventListAdapter;
import dgapp.baggaarm.main.home.child.event.adapter.SavedEventListAdapter;
import dgapp.baggaarm.main.home.child.event.detail.EventDetailActivity;
import dgapp.baggaarm.main.home.child.event.filter.FilterActivity;
import dgapp.baggaarm.service.EventService;
import dgapp.baggaarm.service.model.firebase.FB_Event;
import dgapp.baggaarm.utils.BaseActivity;

public class SavedEventsActivity extends BaseActivity {

    private static final int FILTER_REQUEST_CODE = 999;

    LinearLayout mNoEventContainer;
    ProgressBar mProgressBar;
    SwipeRefreshLayout mRefreshLayout;
    ListView mEventList;
    ImageButton mBack;

    SavedEventListAdapter mAdapter;

    boolean preventLoadingData = false;

    EventService mService = EventService.getInstance();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_home_child_event_saved);

        bindingControls();
        setupControlEvents();
        init();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!preventLoadingData) {
            doGetEvents(true);
        }

        preventLoadingData = false;

    }

    void bindingControls() {
        mNoEventContainer = (LinearLayout) findViewById(R.id.ll_no_event);
        mProgressBar = (ProgressBar) findViewById(R.id.pb_progress);
        mRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.srl_refresh_layout);
        mEventList = (ListView) findViewById(R.id.lv_event_list);
        mBack = (ImageButton) findViewById(R.id.ib_back);
    }

    void setupControlEvents() {
        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                doGetEvents(false);
            }
        });

        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mEventList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                preventLoadingData = true;
                Intent intent = new Intent(SavedEventsActivity.this, EventDetailActivity.class);
                intent.putExtra("key", mAdapter.getItem(position).key);
                startActivity(intent);
            }
        });
    }

    void init() {
        mAdapter = new SavedEventListAdapter(SavedEventsActivity.this, new ArrayList<FB_Event>());
        mEventList.setAdapter(mAdapter);

        mAdapter.setOnUpdateValueFail(new EventListAdapter.OnUpdateValueFail() {
            @Override
            public void onUpdateValueFail() {
                String errorMessage = getString(R.string.event_update_value_fail);
                showErrorDialog(errorMessage);
            }
        });
    }

    void doGetEvents(boolean needShowProgressBar) {
        if (needShowProgressBar) {
            mNoEventContainer.setVisibility(View.GONE);
            mProgressBar.setVisibility(View.VISIBLE);
            mRefreshLayout.setVisibility(View.GONE);
        }

        mService.getSavedEvent(new EventService.GetEventsCallback() {
            @Override
            public void onSuccess(List<FB_Event> data) {
                mRefreshLayout.setRefreshing(false);

                mNoEventContainer.setVisibility(View.GONE);
                mProgressBar.setVisibility(View.GONE);
                mRefreshLayout.setVisibility(View.VISIBLE);

                if (mAdapter == null) {
                    mAdapter = new SavedEventListAdapter(SavedEventsActivity.this, data);
                    mEventList.setAdapter(mAdapter);
                }
                else {
                    mAdapter.clear();
                    mAdapter.addAll(data);
                    mAdapter.notifyDataSetChanged();
                }

                if (data.size() == 0) {
                    onNoEvents();
                }
            }

            @Override
            public void onFail() {
                //TODO
            }
        });
    }

    void onNoEvents() {
        mNoEventContainer.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.GONE);
        mRefreshLayout.setVisibility(View.GONE);
    }
}
