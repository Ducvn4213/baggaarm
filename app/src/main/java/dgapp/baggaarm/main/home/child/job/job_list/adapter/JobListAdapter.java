package dgapp.baggaarm.main.home.child.job.job_list.adapter;

import android.content.Context;
import android.media.Image;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import dgapp.baggaarm.R;
import dgapp.baggaarm.service.JobService;
import dgapp.baggaarm.service.model.firebase.FB_Job;

public class JobListAdapter extends ArrayAdapter<FB_Job> {

    private Context mContext;
    private List<FB_Job> mData;
    private static LayoutInflater mInflater = null;

    public JobListAdapter(Context context, List<FB_Job> data) {
        super(context, R.layout.layout_job_item, data);

        mContext = context;
        mData = data;

        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return mData.size();
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if(view == null) {
            view = mInflater.inflate(R.layout.layout_job_item, null);
        }

        FB_Job data = mData.get(position);

        ImageView logo = (ImageView) view.findViewById(R.id.iv_logo);
        TextView title = (TextView) view.findViewById(R.id.tv_title);
        TextView major = (TextView) view.findViewById(R.id.tv_major);
        TextView salary = (TextView) view.findViewById(R.id.tv_salary);
        TextView time = (TextView) view.findViewById(R.id.tv_time);
        TextView address = (TextView) view.findViewById(R.id.tv_address);

        title.setText(data.titleOfJob);
        salary.setText(data.salary);
        time.setText(data.timeOfJob);
        address.setText(data.addressOfJob);

        major.setText(JobService.getInstance().getNameCategoryByKey(data.jobCategoryID));
//        if (position == 0 || !mData.get(position - 1).jobCategoryID.equalsIgnoreCase(data.jobCategoryID)) {
//            major.setText(JobService.getInstance().getNameCategoryByKey(data.jobCategoryID));
//        }
//        else {
//            major.setText("");
//            major.setVisibility(View.GONE);
//        }

        if (!data.companyLogoURL.isEmpty()) {
            Picasso.with(mContext).load(data.companyLogoURL).into(logo);
        }
        else {
            logo.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.placeholder_image));
        }
        return view;
    }
}
