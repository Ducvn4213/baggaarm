package dgapp.baggaarm.main.home.child.room;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import dgapp.baggaarm.R;
import dgapp.baggaarm.main.home.HomeStaticCacheService;
import dgapp.baggaarm.main.home.child.room.room_list.adapter.RoomFragmentAdapter;
import dgapp.baggaarm.main.home.child.room.room_list.filter.FilterActivity;
import dgapp.baggaarm.main.home.child.room.room_list.match.MatchRoomFragment;
import dgapp.baggaarm.main.home.child.room.room_list.my_post.MyRoomPostActivity;
import dgapp.baggaarm.main.home.child.room.room_list.new_room.NewRoomActivity;
import dgapp.baggaarm.main.home.child.room.room_list.room.FullRoomFragment;
import dgapp.baggaarm.utils.NonSwipeViewPager;
import dgapp.baggaarm.utils.SegmentedControl;

public class RoomsFragment extends Fragment {

    NonSwipeViewPager mViewPager;

    RoomFragmentAdapter mAdapter;
    SegmentedControl mSegmentedControl;

    ImageButton mFilter;
    ImageButton mDisplayMode;
    ImageButton mNewRoom;
    ImageButton mMyRoomPost;

    private static RoomsFragment instance;
    boolean preventResumeAction = false;

    public static RoomsFragment getInstance() {
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home_child_room, container, false);

        bindingControls(view);
        setupControlEvents();
        init();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!preventResumeAction) {
            mViewPager.setCurrentItem(HomeStaticCacheService.getInstance().roomTypeIndex, false);
            mSegmentedControl.setSelected(HomeStaticCacheService.getInstance().roomTypeIndex);
            preventResumeAction = false;
        }

        instance = this;
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible) {
            correctUI();
        }
    }

    void correctUI() {
        if (mViewPager != null && mSegmentedControl != null) {
            mViewPager.setCurrentItem(mSegmentedControl.getSelected(), false);
        }
    }

    void bindingControls(View view) {
        mViewPager = (NonSwipeViewPager) view.findViewById(R.id.vp_container);
        mSegmentedControl = (SegmentedControl) view.findViewById(R.id.sc_room_segmented);
        mFilter = (ImageButton) view.findViewById(R.id.ib_filter);
        mDisplayMode = (ImageButton) view.findViewById(R.id.ib_display_mode);
        mNewRoom = (ImageButton) view.findViewById(R.id.ib_new_room);
        mMyRoomPost = (ImageButton) view.findViewById(R.id.ib_my_room_post);
    }

    void setupControlEvents() {
        mSegmentedControl.setOnSegmentedChangedListener(new SegmentedControl.OnSegmentedChangedListener() {
            @Override
            public void onSegmentedChanged(int index) {
                HomeStaticCacheService.getInstance().roomTypeIndex = index;
                mViewPager.setCurrentItem(index, true);
            }
        });

        mFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), FilterActivity.class);
                startActivity(intent);
            }
        });

        mDisplayMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentDisplayMode = HomeStaticCacheService.getInstance().roomDisplayMode;
                if (currentDisplayMode == 0) {
                    currentDisplayMode = 1;
                }
                else {
                    currentDisplayMode = 0;
                }

                HomeStaticCacheService.getInstance().roomDisplayMode = currentDisplayMode;
                updateDisplayMode();
            }
        });

        mNewRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), NewRoomActivity.class);
                startActivity(intent);
            }
        });

        mMyRoomPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mViewPager.getCurrentItem() == 0) {
                    FullRoomFragment.getInstance().preventLoadingData = true;
                }
                else {
                    MatchRoomFragment.getInstance().preventLoadingData = true;
                }

                Intent intent = new Intent(getActivity(), MyRoomPostActivity.class);
                startActivity(intent);
            }
        });
    }

    void updateDisplayMode() {
        int currentDisplayMode = HomeStaticCacheService.getInstance().roomDisplayMode;
        if (currentDisplayMode == 0) {
            mDisplayMode.setImageResource(R.mipmap.ic_grid);
        }
        else {
            mDisplayMode.setImageResource(R.mipmap.ic_list);
        }

        if (FullRoomFragment.getInstance() != null) {
            FullRoomFragment.getInstance().changeDisplayMode(currentDisplayMode);
        }
        if (MatchRoomFragment.getInstance() != null) {
            MatchRoomFragment.getInstance().changeDisplayMode(currentDisplayMode);
        }
    }

    void init() {
        mAdapter = new RoomFragmentAdapter(getChildFragmentManager());
        mViewPager.setAdapter(mAdapter);


        mSegmentedControl.setTextA(getString(R.string.rooms_full_room_title));
        mSegmentedControl.setTextB(getString(R.string.rooms_match_room_title));
    }
}
