package dgapp.baggaarm.main.home.child.job;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import dgapp.baggaarm.R;
import dgapp.baggaarm.main.home.HomeStaticCacheService;
import dgapp.baggaarm.main.home.child.job.job_list.adapter.JobFragmentAdapter;
import dgapp.baggaarm.main.home.child.job.job_list.filter.FilterActivity;
import dgapp.baggaarm.main.home.child.job.job_list.intern.InternshipFragment;
import dgapp.baggaarm.main.home.child.job.job_list.part_time.PartTimeFragment;
import dgapp.baggaarm.main.home.child.job.job_list.saved.SavedJobsActivity;
import dgapp.baggaarm.utils.NonSwipeViewPager;
import dgapp.baggaarm.utils.SegmentedControl;

import static android.app.Activity.RESULT_OK;

public class JobsFragment extends Fragment {

    private static final int FILTER_REQUEST_CODE = 999;

    NonSwipeViewPager mViewPager;
    SegmentedControl mSegmentedControl;
    ImageButton mFilter;
    ImageButton mSaved;

    JobFragmentAdapter mAdapter;
    boolean preventResumeAction = false;

    private static JobsFragment instance;
    public static JobsFragment getInstance() {
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home_child_job, container, false);

        bindingControls(view);
        setupControlEvents();
        init();

        return view;
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible) {
            correctUI();
        }
    }

    void correctUI() {
        if (mViewPager != null && mSegmentedControl != null) {
            mViewPager.setCurrentItem(mSegmentedControl.getSelected(), false);
        }
    }

    void bindingControls(View view) {
        mViewPager = (NonSwipeViewPager) view.findViewById(R.id.vp_container);
        mSegmentedControl = (SegmentedControl) view.findViewById(R.id.sc_job_segmented);
        mFilter = (ImageButton) view.findViewById(R.id.ib_filter);
        mSaved = (ImageButton) view.findViewById(R.id.ib_saved);
    }

    void setupControlEvents() {
        mSegmentedControl.setOnSegmentedChangedListener(new SegmentedControl.OnSegmentedChangedListener() {
            @Override
            public void onSegmentedChanged(int index) {
                HomeStaticCacheService.getInstance().jobTypeIndex = index;
                mViewPager.setCurrentItem(index, true);
            }
        });

        mFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), FilterActivity.class);
                if (mViewPager.getCurrentItem() == 0) {
                    intent.putExtra("type", "intern");
                }
                else {
                    intent.putExtra("type", "part");
                }
                startActivityForResult(intent, FILTER_REQUEST_CODE);
                preventResumeAction = true;
            }
        });

        mSaved.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mViewPager.getCurrentItem() == 0) {
                    InternshipFragment.getInstance().preventLoadingData = true;
                }
                else {
                    PartTimeFragment.getInstance().preventLoadingData = true;
                }

                Intent intent = new Intent(getActivity(), SavedJobsActivity.class);
                startActivity(intent);
                preventResumeAction = true;
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FILTER_REQUEST_CODE && resultCode == RESULT_OK) {
            //TODO
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!preventResumeAction) {
            mViewPager.setCurrentItem(HomeStaticCacheService.getInstance().jobTypeIndex, false);
            mSegmentedControl.setSelected(HomeStaticCacheService.getInstance().jobTypeIndex);
            preventResumeAction = false;
        }

        instance = this;
    }

    void init() {
        mAdapter = new JobFragmentAdapter(getChildFragmentManager());
        mViewPager.setAdapter(mAdapter);

        mSegmentedControl.setTextA(getString(R.string.home_child_job_internship));
        mSegmentedControl.setTextB(getString(R.string.home_child_job_part_time));
    }
}
