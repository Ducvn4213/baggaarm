package dgapp.baggaarm.main.home.child.job.job_list.adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import dgapp.baggaarm.main.home.child.job.job_list.saved.intern.InternshipFragment;
import dgapp.baggaarm.main.home.child.job.job_list.saved.part_time.PartTimeFragment;

public class SavedJobFragmentAdapter extends FragmentStatePagerAdapter {

    public SavedJobFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new InternshipFragment();
            case 1:
                return new PartTimeFragment();
            default: break;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
