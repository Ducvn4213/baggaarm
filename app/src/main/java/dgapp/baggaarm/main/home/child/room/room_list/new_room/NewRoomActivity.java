package dgapp.baggaarm.main.home.child.room.room_list.new_room;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import dgapp.baggaarm.R;
import dgapp.baggaarm.main.home.child.room.room_list.detail.RoomDetailActivity;
import dgapp.baggaarm.service.RoomService;
import dgapp.baggaarm.utils.BaseActivity;

public class NewRoomActivity extends BaseActivity {

    private static final int PERMISSIONS_CAMERA_REQUEST = 8761;
    private final int PICK_IMAGE_REQUEST_CODE = 9999;
    private final int TAKE_IMAGE_REQUEST_CODE = 9998;

    Button mBack;
    Button mPost;
    Button mRoomInfo;
    ImageView mImage;
    EditText mTitle;
    EditText mDescription;

    RoomService mService = RoomService.getInstance();
    Bitmap currentSelectedImage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_room);

        bindingControls();
        setupControlEvents();
    }

    void bindingControls() {
        mBack = (Button) findViewById(R.id.btn_back);
        mPost = (Button) findViewById(R.id.btn_post);
        mRoomInfo = (Button) findViewById(R.id.btn_room_info);
        mImage = (ImageView) findViewById(R.id.iv_image);
        mTitle = (EditText) findViewById(R.id.edt_title);
        mDescription = (EditText) findViewById(R.id.edt_description);
    }

    void setupControlEvents() {
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewRoomActivity.this.finish();
            }
        });

        mRoomInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewRoomActivity.this, NewRoomInfoActivity.class);
                startActivity(intent);
            }
        });

        mImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestChangeImage();
            }
        });

        mPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPostNewRoom();
            }
        });
    }

    void onPostNewRoom() {
        String title = mTitle.getText().toString();
        String description = mDescription.getText().toString();

        mService.setTitleToAddNewRoom(title);
        mService.setDescriptionToAddNewRoom(description);
        mService.setImageToAddNewRoom(currentSelectedImage);

        showLoading();
        if (mService.addNewRoomIsReady()) {
            mService.addNewRoom(getApplicationContext(), new RoomService.AddNewRoomCallback() {
                @Override
                public void onSuccess(final String newID) {
                    NewRoomActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideLoading();
                            Intent intent = new Intent(NewRoomActivity.this, RoomDetailActivity.class);
                            intent.putExtra("id", newID);
                            startActivity(intent);
                            NewRoomActivity.this.finish();
                        }
                    });
                }

                @Override
                public void onFail() {
                    NewRoomActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            hideLoading();
                            showErrorDialog(getString(R.string.add_new_room_fail_message));
                        }
                    });
                }
            });
        }
        else {
            showLoading();
            showErrorDialog(getString(R.string.add_new_room_missing));
        }
    }

    public void requestChangeImage() {
        final Dialog actionDialog = new Dialog(NewRoomActivity.this);
        actionDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        actionDialog.setCancelable(true);
        actionDialog.setContentView(R.layout.layout_change_image_action);

        TextView camera = (TextView) actionDialog.findViewById(R.id.tv_take_from_camera);
        TextView gallery = (TextView) actionDialog.findViewById(R.id.tv_get_from_gallery);

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionDialog.dismiss();
                checkCameraPermission();
            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionDialog.dismiss();
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                ActivityCompat.startActivityForResult(NewRoomActivity.this, Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST_CODE, null);
            }
        });

        actionDialog.show();
    }

    void checkCameraPermission() {
        List<String> permissions = new ArrayList<>();
        if (ContextCompat.checkSelfPermission(NewRoomActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            gotoTakeImageFromCamera();
        }
        else {
            permissions.add(Manifest.permission.CAMERA);
            String[] perArr = new String[permissions.size()];
            perArr = permissions.toArray(perArr);
            ActivityCompat.requestPermissions(NewRoomActivity.this,
                    perArr,
                    PERMISSIONS_CAMERA_REQUEST);
        }
    }

    void gotoTakeImageFromCamera() {
        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePicture.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePicture, TAKE_IMAGE_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSIONS_CAMERA_REQUEST) {
            if (grantResults.length > 0) {
                boolean isGrant = false;
                for (int gr : grantResults) {
                    if (gr == PackageManager.PERMISSION_GRANTED) {
                        isGrant = true;
                        break;
                    }
                }

                if (isGrant) {
                    gotoTakeImageFromCamera();
                    return;
                }
                else {
                    showErrorDialog(getString(R.string.dialog_message_missing_camera_permission));
                    return;
                }
            }
            else {
                showErrorDialog(getString(R.string.dialog_message_missing_camera_permission));
                return;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST_CODE && resultCode == RESULT_OK) {
            try {
                Uri filePath = data.getData();
                Bitmap newBitmapAvatar = MediaStore.Images.Media.getBitmap(NewRoomActivity.this.getContentResolver(), filePath);
                currentSelectedImage = newBitmapAvatar;
                mImage.setImageBitmap(newBitmapAvatar);
            }
            catch (Exception ex) {
                ex.printStackTrace();
                NewRoomActivity.this.showErrorDialog(getString(R.string.dialog_error_cant_set_image));
            }
        }

        if (requestCode == TAKE_IMAGE_REQUEST_CODE && resultCode == RESULT_OK) {
            try {
                Bitmap newBitmapAvatar = (Bitmap) data.getExtras().get("data");
                currentSelectedImage = newBitmapAvatar;
                mImage.setImageBitmap(newBitmapAvatar);
            }
            catch (Exception ex) {
                ex.printStackTrace();
                NewRoomActivity.this.showErrorDialog(getString(R.string.dialog_error_cant_set_image));
            }
        }
    }
}
