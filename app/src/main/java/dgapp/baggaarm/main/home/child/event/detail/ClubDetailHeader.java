package dgapp.baggaarm.main.home.child.event.detail;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import dgapp.baggaarm.R;
import dgapp.baggaarm.main.home.child.event.adapter.EventListAdapter;
import dgapp.baggaarm.service.EventService;
import dgapp.baggaarm.service.model.firebase.FB_Event;
import dgapp.baggaarm.utils.BaseActivity;
import dgapp.baggaarm.utils.Utils;
import dgapp.baggaarm.utils.WebViewActivity;

public class ClubDetailHeader extends LinearLayout {

    ImageView mBanner;
    ImageView mLogo;
    TextView mName;
    TextView mFullName;
    TextView mIntroduction;
    Button mFanPage;

    FB_Event mData;
    Context mContext;

    public ClubDetailHeader(Context context) {
        this(context, null);
    }

    public ClubDetailHeader(final Context context, AttributeSet attrs) {
        super(context, attrs);

        mContext = context;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.activity_club_detail_header, this, true);

        bindingControls();
        setupControlEvents();
    }

    void bindingControls() {
        mFanPage = (Button) findViewById(R.id.btn_fan_page);

        mName = (TextView) findViewById(R.id.tv_event_name);
        mBanner = (ImageView) findViewById(R.id.iv_banner);
        mLogo = (ImageView) findViewById(R.id.iv_logo);
        mFullName = (TextView) findViewById(R.id.tv_event_fullname);
        mIntroduction = (TextView) findViewById(R.id.tv_event_introduction);
    }

    void setupControlEvents() {
//        mFanPage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(ClubDetailHeader.this, WebViewActivity.class);
//                intent.putExtra("url", mData.host.fanpageURL);
//                startActivity(intent);
//            }
//        });
    }

    void setData(FB_Event event) {
        mData = event;
        if (mData.host.coverPhotoURL.isEmpty()) {
            mBanner.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.placeholder_image));
        }
        else {
            Picasso.with(mContext).load(mData.host.coverPhotoURL).into(mBanner);
        }

        if (mData.host.profilePhotoURL.isEmpty()) {
            mLogo.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.placeholder_image));
        }
        else {
            Picasso.with(mContext).load(mData.host.profilePhotoURL).into(mLogo);
        }

        mName.setText(mData.host.displayName);
        mFullName.setText(mData.host.fullName);
        mIntroduction.setText(mData.host.hostDescription);
    }
}
