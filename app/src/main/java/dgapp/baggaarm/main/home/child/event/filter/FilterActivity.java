package dgapp.baggaarm.main.home.child.event.filter;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import dgapp.baggaarm.R;
import dgapp.baggaarm.login.signup.fragments.university.adapter.UniversityListAdapter;
import dgapp.baggaarm.service.EventService;
import dgapp.baggaarm.service.UniversityService;
import dgapp.baggaarm.utils.BaseActivity;

public class FilterActivity extends BaseActivity {

    TextView mTitle;
    ListView mCategoryList;
    ImageButton mBack;

    UniversityListAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_child_event_filter);

        bindingControls();
        setupControlEvents();
        init();
    }

    void bindingControls() {
        mTitle = (TextView) findViewById(R.id.tv_title);
        mCategoryList = (ListView) findViewById(R.id.lv_category_list);
        mBack = (ImageButton) findViewById(R.id.ib_back);
    }

    void setupControlEvents() {
        mCategoryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mAdapter.setSelectionIndex(position);
                EventService.getInstance().setFilterKey(mAdapter.getItem(position).ID);
                finish();
            }
        });

        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FilterActivity.this.finish();
            }
        });
    }

    void init() {
        mAdapter = new UniversityListAdapter(getApplicationContext(), UniversityService.getInstance().getUniversitiesData());
        mAdapter.showNameOnly();

        mCategoryList.setAdapter(mAdapter);
    }
}
