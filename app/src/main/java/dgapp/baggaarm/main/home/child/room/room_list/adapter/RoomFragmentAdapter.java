package dgapp.baggaarm.main.home.child.room.room_list.adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import dgapp.baggaarm.main.home.child.job.job_list.intern.InternshipFragment;
import dgapp.baggaarm.main.home.child.job.job_list.part_time.PartTimeFragment;
import dgapp.baggaarm.main.home.child.room.room_list.match.MatchRoomFragment;
import dgapp.baggaarm.main.home.child.room.room_list.room.FullRoomFragment;

public class RoomFragmentAdapter extends FragmentStatePagerAdapter {

    public RoomFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new FullRoomFragment();
            case 1:
                return new MatchRoomFragment();
            default: break;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
