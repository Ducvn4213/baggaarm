package dgapp.baggaarm.main.home.child.room.room_list.my_post;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import dgapp.baggaarm.R;
import dgapp.baggaarm.main.home.child.room.room_list.adapter.RoomListAdapter;
import dgapp.baggaarm.main.home.child.room.room_list.detail.RoomDetailActivity;
import dgapp.baggaarm.main.home.child.room.room_list.new_room.NewRoomActivity;
import dgapp.baggaarm.service.RoomService;
import dgapp.baggaarm.service.model.firebase.FB_Room;
import dgapp.baggaarm.utils.BaseActivity;
import in.srain.cube.views.GridViewWithHeaderAndFooter;

public class MyRoomPostActivity extends BaseActivity {

    int displayMode = 0;

    ImageButton mBack;
    ImageButton mNew;
    ImageButton mDisplayMode;

    RoomService mService = RoomService.getInstance();

    GridViewWithHeaderAndFooter mGridView;
    SwipeRefreshLayout mSwipeRefreshLayout;
    ProgressBar mProgress;

    boolean preventLoadingData = false;

    RoomListAdapter mAdapter;
    LinearLayout mNoDataLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_room_post);

        bindingControls();
        setupControlEvents();
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!preventLoadingData) {
            mSwipeRefreshLayout.setVisibility(View.GONE);
            mProgress.setVisibility(View.VISIBLE);
            mNoDataLayout.setVisibility(View.GONE);

            mService.getMyRoom(getApplicationContext(), new RoomService.GetRoomsCallback() {
                @Override
                public void onSuccess(List<FB_Room> data) {
                    if (data.size() > 0) {
                        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                        mProgress.setVisibility(View.GONE);
                    }
                    else {
                        onNoData();
                        return;
                    }

                    mAdapter.clear();
                    mAdapter.addAll(data);
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onFail() {
                    //TODO
                }
            });
        }

        preventLoadingData = false;
    }

    void bindingControls() {
        mBack = (ImageButton) findViewById(R.id.ib_back);
        mNew = (ImageButton) findViewById(R.id.ib_new_room);
        mDisplayMode = (ImageButton) findViewById(R.id.ib_display_mode);
        mNoDataLayout = (LinearLayout) findViewById(R.id.ll_no_room);
        mGridView = (GridViewWithHeaderAndFooter) findViewById(R.id.gv_room_list);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.srl_refresh_layout);
        mProgress = (ProgressBar) findViewById(R.id.pb_progress);
    }

    void setupControlEvents() {
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyRoomPostActivity.this.finish();
            }
        });

        mNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyRoomPostActivity.this, NewRoomActivity.class);
                startActivity(intent);
            }
        });

        mDisplayMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (displayMode == 0) {
                    displayMode = 1;
                }
                else {
                    displayMode = 0;
                }

                changeDisplayMode(displayMode);
            }
        });

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                preventLoadingData = true;
                Intent intent = new Intent(MyRoomPostActivity.this, RoomDetailActivity.class);
                intent.putExtra("id", mAdapter.getItem(position).key + "");
                startActivity(intent);
            }
        });

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mService.getMyRoom(getApplicationContext(), new RoomService.GetRoomsCallback() {
                    @Override
                    public void onSuccess(List<FB_Room> data) {
                        if (data.size() > 0) {
                            mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                            mProgress.setVisibility(View.GONE);
                        }
                        else {
                            onNoData();
                            return;
                        }

                        mAdapter.clear();
                        mAdapter.addAll(data);
                        mAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onFail() {
                        //TODO
                    }
                });
            }
        });
    }

    void init() {
        LayoutInflater inflater = (LayoutInflater) MyRoomPostActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View footer = inflater.inflate(R.layout.layout_room_footer_view, null, false);

        mGridView.addFooterView(footer);

        int color = ContextCompat.getColor(MyRoomPostActivity.this, R.color.colorPrimary);
        mSwipeRefreshLayout.setColorSchemeColors(color);

        List<FB_Room> initJobs = new ArrayList<>();
        mAdapter = new RoomListAdapter(MyRoomPostActivity.this, initJobs);
        mGridView.setAdapter(mAdapter);
    }

    void onNoData() {
        mNoDataLayout.setVisibility(View.VISIBLE);
        mSwipeRefreshLayout.setVisibility(View.GONE);
        mProgress.setVisibility(View.GONE);
    }

    public void changeDisplayMode(int mode) {
        if (mode == 0) {
            mGridView.setNumColumns(2);
        }
        else {
            mGridView.setNumColumns(1);
        }

        mAdapter.setDisplayMode(mode);
    }
}
