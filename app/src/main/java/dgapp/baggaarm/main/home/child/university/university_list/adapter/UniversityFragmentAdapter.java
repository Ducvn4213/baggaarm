package dgapp.baggaarm.main.home.child.university.university_list.adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import dgapp.baggaarm.main.home.child.room.room_list.match.MatchRoomFragment;
import dgapp.baggaarm.main.home.child.room.room_list.room.FullRoomFragment;
import dgapp.baggaarm.main.home.child.university.university_list.photo.PhotoUniversityFragment;
import dgapp.baggaarm.main.home.child.university.university_list.text.TextUniversityFragment;

public class UniversityFragmentAdapter extends FragmentStatePagerAdapter {

    public UniversityFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new TextUniversityFragment();
            case 1:
                return new PhotoUniversityFragment();
            default: break;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
