package dgapp.baggaarm.main.home.child.job.job_list.saved.intern;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import dgapp.baggaarm.R;
import dgapp.baggaarm.main.home.child.job.JobsFragment;
import dgapp.baggaarm.main.home.child.job.job_list.adapter.JobListAdapter;
import dgapp.baggaarm.main.home.child.job.job_list.detail.JobDetailActivity;
import dgapp.baggaarm.service.JobService;
import dgapp.baggaarm.service.model.firebase.FB_Job;

public class InternshipFragment extends Fragment {

    ListView mListView;
    ProgressBar mProgress;
    LinearLayout mNoJob;
    SwipeRefreshLayout mSwipeRefreshLayout;

    JobListAdapter mAdapter;
    List<FB_Job> mData;

    boolean preventLoadingData = false;

    JobService mService = JobService.getInstance();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home_child_job_intern_saved, container, false);

        bindingControls(view);
        setupControlEvents();
        init();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!preventLoadingData) {
            mSwipeRefreshLayout.setVisibility(View.GONE);
            mProgress.setVisibility(View.VISIBLE);
            mNoJob.setVisibility(View.GONE);

            mService.getInternSavedJob(new JobService.GetJobsCallback() {
                @Override
                public void onCompleted(List<FB_Job> internJobs, List<FB_Job> partTimeJobs) {
                    if (internJobs.size() > 0) {
                        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                        mProgress.setVisibility(View.GONE);
                    }
                    else {
                        onNoData();
                        return;
                    }

                    mAdapter = new JobListAdapter(getContext(), internJobs);
                    mListView.setAdapter(mAdapter);
                }

                @Override
                public void onFail() {

                }
            });
        }

        preventLoadingData = false;
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible) {
            //TODO
        }
    }

    void onNoData() {
        mSwipeRefreshLayout.setVisibility(View.GONE);
        mProgress.setVisibility(View.GONE);
        mNoJob.setVisibility(View.VISIBLE);
    }

    JobsFragment getParentJobsFragment() {
        return (JobsFragment) getParentFragment();
    }

    void bindingControls(View view) {
        mListView = (ListView) view.findViewById(R.id.lv_job_list);
        mProgress = (ProgressBar) view.findViewById(R.id.pb_progress);
        mNoJob = (LinearLayout) view.findViewById(R.id.ll_no_job);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.srl_refresh_layout);
    }

    void setupControlEvents() {
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position >= mAdapter.getCount()) {
                    return;
                }

                preventLoadingData = true;
                Intent intent = new Intent(getActivity(), JobDetailActivity.class);
                intent.putExtra("key", mAdapter.getItem(position).key);
                intent.putExtra("type", "intern");
                startActivity(intent);
            }
        });

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mService.getInternSavedJob(new JobService.GetJobsCallback() {
                    @Override
                    public void onCompleted(List<FB_Job> internJobs, List<FB_Job> partTimeJobs) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        if (internJobs.size() > 0) {
                            mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                            mProgress.setVisibility(View.GONE);
                        }
                        else {
                            onNoData();
                            return;
                        }

                        mAdapter = new JobListAdapter(getContext(), internJobs);
                        mListView.setAdapter(mAdapter);
                    }

                    @Override
                    public void onFail() {

                    }
                });
            }
        });
    }

    void init() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View footer = inflater.inflate(R.layout.layout_job_footer_listview, null, false);

        mListView.addFooterView(footer);

        int color = ContextCompat.getColor(getContext(), R.color.colorPrimary);
        mSwipeRefreshLayout.setColorSchemeColors(color);

        List<FB_Job> initJobs = new ArrayList<>();
        mAdapter = new JobListAdapter(getContext(), initJobs);
        mListView.setAdapter(mAdapter);
    }
}
