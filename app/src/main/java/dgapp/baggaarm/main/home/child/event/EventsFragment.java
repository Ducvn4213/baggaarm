package dgapp.baggaarm.main.home.child.event;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;

import java.util.ArrayList;
import java.util.List;

import dgapp.baggaarm.R;
import dgapp.baggaarm.main.home.child.event.adapter.EventListAdapter;
import dgapp.baggaarm.main.home.child.event.adapter.EventRecycleAdapter;
import dgapp.baggaarm.main.home.child.event.detail.EventDetailActivity;
import dgapp.baggaarm.main.home.child.event.filter.FilterActivity;
import dgapp.baggaarm.main.home.child.event.saved.SavedEventsActivity;
import dgapp.baggaarm.service.EventService;
import dgapp.baggaarm.service.model.firebase.FB_Event;
import dgapp.baggaarm.utils.BaseFragment;
import dgapp.baggaarm.utils.OnLoadMoreListener;

public class EventsFragment extends BaseFragment {

    private static final int FILTER_REQUEST_CODE = 999;

    ProgressBar mProgressBar;
    SuperRecyclerView mEventList;
    ImageButton mFilter;
    ImageButton mSaved;

    List<String> allEventsKey;
    boolean isLoadingMore = false;

    EventRecycleAdapter mAdapter;
    boolean preventLoadingData = false;

    EventService mService = EventService.getInstance();
    private static EventsFragment instance;
    public static EventsFragment getInstance() {
        return instance;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home_child_event, container, false);

        bindingControls(view);
        setupControlEvents();
        init();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!preventLoadingData && !isLoadingMore) {
            isLoadingMore = true;
            doGetEvents(true);
        }

        preventLoadingData = false;

        instance = this;
    }

    void bindingControls(View view) {
        mProgressBar = (ProgressBar) view.findViewById(R.id.pb_progress);
        mEventList = (SuperRecyclerView) view.findViewById(R.id.srv_event_list);
        mFilter = (ImageButton) view.findViewById(R.id.ib_filter);
        mSaved = (ImageButton) view.findViewById(R.id.ib_saved);
    }

    void setupControlEvents() {
        mEventList.setRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mAdapter.clearWithoutReloadUI();
                doGetEvents(false);
            }
        });

        mEventList.setupMoreListener(new OnMoreListener() {
            @Override
            public void onMoreAsked(int numberOfItems, int numberBeforeMore, int currentItemPos) {
                loadMoreEvents();
            }}, 2);

        mFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), FilterActivity.class);
                startActivityForResult(intent, FILTER_REQUEST_CODE);
            }
        });

        mSaved.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                preventLoadingData = true;
                Intent intent = new Intent(getActivity(), SavedEventsActivity.class);
                startActivityForResult(intent, FILTER_REQUEST_CODE);
            }
        });
    }

    void init() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mEventList.setLayoutManager(mLayoutManager);

        mAdapter = new EventRecycleAdapter(new ArrayList<FB_Event>());
        mEventList.setAdapter(mAdapter);

        mAdapter.setOnUpdateValueFail(new EventRecycleAdapter.OnUpdateValueFail() {
            @Override
            public void onUpdateValueFail() {
                String errorMessage = getString(R.string.event_update_value_fail);
                showErrorDialog(errorMessage);
            }
        });

        mAdapter.setOnItemClickListener(new EventRecycleAdapter.OnItemClickListener() {
            @Override
            public void onItemClickListener(int position) {
                preventLoadingData = true;
                Intent intent = new Intent(getActivity(), EventDetailActivity.class);
                intent.putExtra("key", mAdapter.getItem(position).key);
                startActivity(intent);
            }
        });
    }

    void doGetEvents(boolean needShowProgressBar) {
        if (needShowProgressBar) {
            mEventList.setVisibility(View.GONE);
        }

        mService.getEventsKey(getContext(), new EventService.GetEventsKeyCallback() {
            @Override
            public void onSuccess(List<String> data) {
                allEventsKey = data;
                loadMoreEvents();
            }

            @Override
            public void onFail() {
                onNoEvents();
            }
        });
    }

    void loadMoreEvents() {
        List<String> keyToLoad = new ArrayList<>();
        int currentCount = mAdapter.getItemCount();

        for (int i = currentCount; i < allEventsKey.size(); i++) {
            keyToLoad.add(allEventsKey.get(i));
            if ((allEventsKey.size() - 1) == i) {
                mEventList.removeMoreListener();
            }
            if (keyToLoad.size() == 6) {
                break;
            }
        }



        if (keyToLoad.size() == 0) {
            return;
        }

        mService.getEventsFromKeys(keyToLoad, new EventService.GetEventsCallback() {
            @Override
            public void onSuccess(List<FB_Event> data) {
                isLoadingMore = false;

                mEventList.setVisibility(View.VISIBLE);

                doAddDataToAdapter(data);
            }

            @Override
            public void onFail() {
                //TODO
            }
        });
    }

    void doAddDataToAdapter(List<FB_Event> data) {
        for (FB_Event event : data) {
            if (notContain(event)) {
                mAdapter.add(event);
            }
        }

        mAdapter.notifyDataSetChanged();
    }

    boolean notContain(FB_Event event) {
        int count = mAdapter.getItemCount();
        for (int i = 0; i < count; i++) {
            if (mAdapter.getItem(i).key.equalsIgnoreCase(event.key)) {
                return false;
            }
        }

        return true;
    }

    void onNoEvents() {
        mEventList.setVisibility(View.GONE);
    }
}
