package dgapp.baggaarm.main.home.child.job.job_list.saved;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageButton;

import java.util.List;

import dgapp.baggaarm.R;
import dgapp.baggaarm.main.home.child.job.job_list.adapter.SavedJobFragmentAdapter;
import dgapp.baggaarm.service.JobService;
import dgapp.baggaarm.service.model.firebase.FB_Job;
import dgapp.baggaarm.utils.BaseActivity;
import dgapp.baggaarm.utils.NonSwipeViewPager;
import dgapp.baggaarm.utils.SegmentedControl;

public class SavedJobsActivity extends BaseActivity {

    ImageButton mBack;
    SegmentedControl mSegmentedControl;
    NonSwipeViewPager mViewPager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_saved_job);

        bindingControls();
        setupControlEvents();
        init();
    }

    void bindingControls() {
        mBack = (ImageButton) findViewById(R.id.ib_back);
        mSegmentedControl = (SegmentedControl) findViewById(R.id.sc_job_segmented);
        mViewPager = (NonSwipeViewPager) findViewById(R.id.vp_container);
    }

    void setupControlEvents() {
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SavedJobsActivity.this.finish();
            }
        });

        mSegmentedControl.setOnSegmentedChangedListener(new SegmentedControl.OnSegmentedChangedListener() {
            @Override
            public void onSegmentedChanged(int index) {
                mViewPager.setCurrentItem(index, true);
            }
        });
    }

    void init() {
        mViewPager.setAdapter(new SavedJobFragmentAdapter(getSupportFragmentManager()));

        mSegmentedControl.setTextA(getString(R.string.home_child_job_internship));
        mSegmentedControl.setTextB(getString(R.string.home_child_job_part_time));
    }
}
