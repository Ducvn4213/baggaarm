package dgapp.baggaarm.main.home.main;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import dgapp.baggaarm.MainActivity;
import dgapp.baggaarm.R;
import dgapp.baggaarm.main.home.HomeFragment;
import dgapp.baggaarm.main.home.HomeStaticCacheService;
import dgapp.baggaarm.service.EventService;
import dgapp.baggaarm.service.JobService;
import dgapp.baggaarm.service.RoomService;

public class HomeMainFragment extends Fragment implements MainActivity.PassBackHandle {

    RelativeLayout mJob;
    RelativeLayout mRoom;
    RelativeLayout mEvent;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home_home, container, false);

        bindingControls(view);
        setupControlEvents();

        return view;
    }

    HomeFragment getParentHomeFragment() {
        return (HomeFragment) getParentFragment();
    }

    void bindingControls(View view) {
        mJob = (RelativeLayout) view.findViewById(R.id.rl_job);
        mRoom = (RelativeLayout) view.findViewById(R.id.rl_room);
        mEvent = (RelativeLayout) view.findViewById(R.id.rl_event);
    }

    void setupControlEvents() {
        mJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeStaticCacheService.getInstance().homeFeatureIndex = 0;
                JobService.getInstance().setInternFilterKeys(new ArrayList<String>());
                JobService.getInstance().setPartTimeFilterKeys(new ArrayList<String>());
                getParentHomeFragment().onSelectFeature(HomeFragment.HOME_FEATURE.JOB);
            }
        });

        mRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeStaticCacheService.getInstance().homeFeatureIndex = 1;
                RoomService.getInstance().clearRoomFilter();
                getParentHomeFragment().onSelectFeature(HomeFragment.HOME_FEATURE.ROOM);
            }
        });

        mEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeStaticCacheService.getInstance().homeFeatureIndex = 3;
                EventService.getInstance().setFilterKey(null);
                getParentHomeFragment().onSelectFeature(HomeFragment.HOME_FEATURE.EVENT);
            }
        });
    }

    @Override
    public void doBack(boolean onDeviceBackButton) {
        //TODO
    }
}
