package dgapp.baggaarm.main.home.child.job.job_list.filter;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import dgapp.baggaarm.R;
import dgapp.baggaarm.main.home.child.job.job_list.adapter.CategoryListAdapter;
import dgapp.baggaarm.service.JobService;
import dgapp.baggaarm.utils.BaseActivity;

public class FilterActivity extends BaseActivity {

    TextView mTitle;
    ListView mCategoryList;
    ImageButton mBack;
    Button mSearch;

    CategoryListAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_child_job_filter);

        bindingControls();
        setupControlEvents();
        init();
    }

    void bindingControls() {
        mTitle = (TextView) findViewById(R.id.tv_title);
        mCategoryList = (ListView) findViewById(R.id.lv_category_list);
        mBack = (ImageButton) findViewById(R.id.ib_back);
        mSearch = (Button) findViewById(R.id.btn_search);
    }

    void setupControlEvents() {
        mCategoryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mAdapter.toggleSelection(position);
                if (mAdapter.getSelection().size() > 0) {
                    mSearch.setEnabled(true);
                }
                else {
                    mSearch.setEnabled(false);
                }
            }
        });

        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FilterActivity.this.finish();
            }
        });

        mSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_OK);
                String type = getIntent().getStringExtra("type");
                if (type.equalsIgnoreCase("intern")) {
                    JobService.getInstance().setInternFilterKeys(mAdapter.getFilterKeys());
                }
                else {
                    JobService.getInstance().setPartTimeFilterKeys(mAdapter.getFilterKeys());
                }
                finish();
            }
        });
    }

    void init() {
        String type = getIntent().getStringExtra("type");
        if (type.equalsIgnoreCase("intern")) {
            mTitle.setText(getString(R.string.jobs_filter_intern_title));
            mAdapter = JobService.getInstance().getInternCategoryListAdapter(FilterActivity.this);
        }
        else {
            mTitle.setText(getString(R.string.jobs_filter_part_time_title));
            mAdapter = JobService.getInstance().getPartTimeCategoryListAdapter(FilterActivity.this);
        }

        mCategoryList.setAdapter(mAdapter);
    }
}
