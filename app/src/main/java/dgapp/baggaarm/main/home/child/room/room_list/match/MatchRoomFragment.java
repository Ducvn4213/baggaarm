package dgapp.baggaarm.main.home.child.room.room_list.match;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import dgapp.baggaarm.R;
import dgapp.baggaarm.main.home.child.room.room_list.adapter.RoomListAdapter;
import dgapp.baggaarm.main.home.child.room.room_list.detail.RoomDetailActivity;
import dgapp.baggaarm.service.RoomService;
import dgapp.baggaarm.service.model.firebase.FB_Room;
import dgapp.baggaarm.utils.BaseFragment;
import in.srain.cube.views.GridViewWithHeaderAndFooter;

public class MatchRoomFragment extends BaseFragment {

    RoomService mService = RoomService.getInstance();

    GridViewWithHeaderAndFooter mGridView;
    SwipeRefreshLayout mSwipeRefreshLayout;
    ProgressBar mProgress;

    RoomListAdapter mAdapter;
    LinearLayout mNoDataLayout;

    public boolean preventLoadingData = false;

    private static MatchRoomFragment instance;
    public static MatchRoomFragment getInstance() {
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home_child_room_match, container, false);

        bindingControls(view);
        setupControlEvents();
        init();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!preventLoadingData) {
            mSwipeRefreshLayout.setVisibility(View.GONE);
            mProgress.setVisibility(View.VISIBLE);
            mNoDataLayout.setVisibility(View.GONE);

            mService.getMatchRoom(new RoomService.GetRoomsCallback() {
                @Override
                public void onSuccess(List<FB_Room> data) {
                    if (data.size() > 0) {
                        mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                        mProgress.setVisibility(View.GONE);
                    }
                    else {
                        onNoData();
                        return;
                    }

                    mAdapter.clear();
                    mAdapter.addAll(data);
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onFail() {
                    //TODO
                }
            });
        }

        preventLoadingData = false;

        instance = this;
    }

    void bindingControls(View view) {
        mNoDataLayout = (LinearLayout) view.findViewById(R.id.ll_no_room);
        mGridView = (GridViewWithHeaderAndFooter) view.findViewById(R.id.gv_room_list);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.srl_refresh_layout);
        mProgress = (ProgressBar) view.findViewById(R.id.pb_progress);
    }

    void setupControlEvents() {
        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                preventLoadingData = true;
                Intent intent = new Intent(getActivity(), RoomDetailActivity.class);
                intent.putExtra("id", mAdapter.getItem(position).key + "");
                startActivity(intent);
            }
        });

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mService.getMatchRoom(new RoomService.GetRoomsCallback() {
                    @Override
                    public void onSuccess(List<FB_Room> data) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        if (data.size() > 0) {
                            mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                            mProgress.setVisibility(View.GONE);
                        }
                        else {
                            onNoData();
                            return;
                        }

                        mAdapter.clear();
                        mAdapter.addAll(data);
                        mAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onFail() {
                        mSwipeRefreshLayout.setRefreshing(false);
                        //TODO
                    }
                });
            }
        });
    }

    void init() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View footer = inflater.inflate(R.layout.layout_room_footer_view, null, false);

        mGridView.addFooterView(footer);

        int color = ContextCompat.getColor(getContext(), R.color.colorPrimary);
        mSwipeRefreshLayout.setColorSchemeColors(color);

        List<FB_Room> initJobs = new ArrayList<>();
        mAdapter = new RoomListAdapter(getContext(), initJobs);
        mGridView.setAdapter(mAdapter);
    }

    void onNoData() {
        mNoDataLayout.setVisibility(View.VISIBLE);
        mSwipeRefreshLayout.setVisibility(View.GONE);
        mProgress.setVisibility(View.GONE);
    }

    public void changeDisplayMode(int mode) {
        if (mode == 0) {
            mGridView.setNumColumns(2);
        }
        else {
            mGridView.setNumColumns(1);
        }

        mAdapter.setDisplayMode(mode);
    }
}
