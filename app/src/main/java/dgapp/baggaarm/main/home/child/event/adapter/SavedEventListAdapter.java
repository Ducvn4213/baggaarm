package dgapp.baggaarm.main.home.child.event.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.List;

import dgapp.baggaarm.R;
import dgapp.baggaarm.service.model.firebase.FB_Event;
import dgapp.baggaarm.utils.EventItem;

public class SavedEventListAdapter extends ArrayAdapter<FB_Event> {

    private Context mContext;
    private List<FB_Event> mData;
    private static LayoutInflater mInflater = null;

    EventListAdapter.OnUpdateValueFail onUpdateValueFail;

    public void setOnUpdateValueFail(EventListAdapter.OnUpdateValueFail callback) {
        this.onUpdateValueFail = callback;
    }

    public SavedEventListAdapter(Context context, List<FB_Event> data) {
        super(context, R.layout.layout_event_item_in_list, data);

        mContext = context;
        mData = data;

        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return mData.size();
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if(view == null) {
            view = mInflater.inflate(R.layout.layout_event_item_in_list, null);
        }

        FB_Event data = mData.get(position);

        EventItem item = (EventItem) view.findViewById(R.id.ei_item);

        item.setData(position, data);
        item.setDelegate(itemDelegate);

        return view;
    }

    private ItemDelegate itemDelegate = new ItemDelegate();

    private class ItemDelegate implements EventItem.EventItemDelegate {

        @Override
        public void onInterestingPrepareToChange(int index, boolean value) {
            mData.get(index).isInteresting = value;
            if (value) {
                mData.get(index).interestingCount += 1;
            }
            else {
                mData.get(index).interestingCount -= 1;
            }
            notifyDataSetChanged();
        }

        @Override
        public void onSavesPrepareToChange(int index, boolean value) {
            mData.get(index).isSaved = value;
            if (value) {
                mData.get(index).saveCount += 1;
            }
            else {
                mData.get(index).saveCount -= 1;
            }
            notifyDataSetChanged();
        }

        @Override
        public void onWillGoPrepareToChange(int index, boolean value) {
            mData.get(index).isWillGo = value;
            if (value) {
                mData.get(index).willGoCount += 1;
            }
            else {
                mData.get(index).willGoCount -= 1;
            }
            notifyDataSetChanged();
        }

        @Override
        public void onInterestingChanged(int index, boolean value) {
            //Do nothing if value actually changed
        }

        @Override
        public void onSavesChanged(int index, boolean value) {
            mData.remove(index);
            notifyDataSetInvalidated();
        }

        @Override
        public void onWillGoChanged(int index, boolean value) {
            //Do nothing if value actually changed
        }

        @Override
        public void onInterestingChangeFail(int index, boolean oldValue) {
            mData.get(index).isInteresting = !oldValue;
            if (!oldValue) {
                mData.get(index).interestingCount += 1;
            }
            else {
                mData.get(index).interestingCount -= 1;
            }
            notifyDataSetChanged();
            onUpdateValueFail.onUpdateValueFail();
        }

        @Override
        public void onSavesChangedFail(int index, boolean oldValue) {
            mData.get(index).isSaved = !oldValue;
            if (!oldValue) {
                mData.get(index).saveCount += 1;
            }
            else {
                mData.get(index).saveCount -= 1;
            }
            notifyDataSetChanged();
            onUpdateValueFail.onUpdateValueFail();
        }

        @Override
        public void onWillGoChangedFail(int index, boolean oldValue) {
            mData.get(index).isWillGo = !oldValue;
            if (!oldValue) {
                mData.get(index).willGoCount += 1;
            }
            else {
                mData.get(index).willGoCount -= 1;
            }
            notifyDataSetChanged();
            onUpdateValueFail.onUpdateValueFail();
        }
    }
}
