package dgapp.baggaarm.main.home.child.adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import dgapp.baggaarm.main.home.child.HomeChildFragment;
import dgapp.baggaarm.main.home.child.event.EventsFragment;
import dgapp.baggaarm.main.home.child.job.JobsFragment;
import dgapp.baggaarm.main.home.child.room.RoomsFragment;
import dgapp.baggaarm.main.home.child.university.UniversityFragment;
import dgapp.baggaarm.main.home.main.HomeMainFragment;

public class HomeChildFragmentAdapter extends FragmentStatePagerAdapter {

    public HomeChildFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new JobsFragment();
            case 1:
                return new RoomsFragment();
            case 2:
                return new UniversityFragment();
            case 3:
                return new EventsFragment();
            default: break;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }
}
