package dgapp.baggaarm.main.home.child.room.room_list.detail;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Locale;

import dgapp.baggaarm.R;
import dgapp.baggaarm.service.RoomService;
import dgapp.baggaarm.service.model.firebase.FB_Room;
import dgapp.baggaarm.utils.BaseActivity;
import dgapp.baggaarm.utils.Utils;

public class RoomDetailActivity extends BaseActivity {

    ImageButton mBack;
    ImageView mImage;
    TextView mTitle;
    TextView mAddress;
    TextView mSize;
    TextView mCost;
    TextView mPhone;
    TextView mDetail;
    Button mViewMap;
    Button mCallNow;

    RoomService mService = RoomService.getInstance();
    FB_Room mRoom;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_detail);

        bindingControls();
        setupControlEvents();
        init();
    }

    void bindingControls() {
        mBack = (ImageButton) findViewById(R.id.ib_back);
        mImage = (ImageView) findViewById(R.id.iv_image);
        mTitle = (TextView) findViewById(R.id.tv_title);
        mAddress = (TextView) findViewById(R.id.tv_address);
        mSize = (TextView) findViewById(R.id.tv_size);
        mCost = (TextView) findViewById(R.id.tv_cost);
        mPhone = (TextView) findViewById(R.id.tv_phone);
        mDetail = (TextView) findViewById(R.id.tv_detail);
        mViewMap = (Button) findViewById(R.id.btn_view_map);
        mCallNow = (Button) findViewById(R.id.btn_call_now);
    }

    void setupControlEvents() {
        mViewMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String strUri = "http://maps.google.com/maps?q=" + mRoom.roomInformation.address;
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(strUri));
                intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");

                startActivity(intent);
            }
        });

        mCallNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + mRoom.roomInformation.contactPhoneNumber));
                startActivity(intent);
            }
        });

        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RoomDetailActivity.this.finish();
            }
        });
    }

    void init() {
        String id = getIntent().getStringExtra("id");
        mService.getRoomDetail(id, new RoomService.GetRoomsCallback() {
            @Override
            public void onSuccess(final List<FB_Room> data) {
                RoomDetailActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        bindData(data.get(0));
                    }
                });
            }

            @Override
            public void onFail() {

            }
        });
    }

    void bindData(FB_Room data) {
        mRoom = data;

        if (!mRoom.imgURL.isEmpty()) {
            Picasso.with(RoomDetailActivity.this).load(mRoom.imgURL).into(mImage);
        }

        mTitle.setText(mRoom.roomExtend.title);
        mAddress.setText(mRoom.roomInformation.address);
        mSize.setText(data.roomInformation.length + " * " + data.roomInformation.width + " (D * R)");
        mCost.setText(Utils.numberToVND(data.roomInformation.price + "") + " " + RoomDetailActivity.this.getString(R.string.vnd_unit));
        mPhone.setText(data.roomInformation.contactPhoneNumber);

        mDetail.setText(mRoom.roomExtend.content);
    }
}
