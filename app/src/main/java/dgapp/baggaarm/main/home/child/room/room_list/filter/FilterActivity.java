package dgapp.baggaarm.main.home.child.room.room_list.filter;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatSpinner;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Filter;

import dgapp.baggaarm.R;
import dgapp.baggaarm.service.RoomService;
import dgapp.baggaarm.service.model.firebase.FB_RoomArea;
import dgapp.baggaarm.utils.BaseActivity;

public class FilterActivity extends BaseActivity {

    AppCompatSpinner mAreaSpinner;
    AppCompatSpinner mPriceSpinner;
    AppCompatSpinner mOrderSpinner;
    Button mFilter;
    ImageButton mBack;

    RoomService mService = RoomService.getInstance();

    List<String> priceList;
    List<String> orderList;
    List<FB_RoomArea> areaList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_filter);

        bindingControls();
        setupControlEvents();
        init();
    }

    void bindingControls() {
        mPriceSpinner = (AppCompatSpinner) findViewById(R.id.spn_price);
        mOrderSpinner = (AppCompatSpinner) findViewById(R.id.spn_order);
        mAreaSpinner = (AppCompatSpinner) findViewById(R.id.spn_area);
        mFilter = (Button) findViewById(R.id.btn_filter);
        mBack = (ImageButton) findViewById(R.id.ib_back);
    }

    void setupControlEvents() {
        mFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mService.setAreaFilterIndex(mAreaSpinner.getSelectedItemPosition());
                mService.setPriceFilterIndex(mPriceSpinner.getSelectedItemPosition());
                mService.setOrderFilterIndex(mOrderSpinner.getSelectedItemPosition());
                FilterActivity.this.finish();
            }
        });

        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FilterActivity.this.finish();
            }
        });
    }

    void init() {
        mService.getAreas(new RoomService.GetAreasCallback() {
            @Override
            public void onSuccess(final List<FB_RoomArea> data) {
                FilterActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        areaList = data;
                        List<String> areaData = new ArrayList<String>();
                        areaData.add(getString(R.string.room_filter_all));
                        for (FB_RoomArea ra : areaList) {
                            areaData.add(ra.displayID + " - " + ra.fullName);
                        }

                        ArrayAdapter<String> a_adapter = new ArrayAdapter<>(getApplicationContext(), R.layout.layout_spinner_item_center,
                                R.id.tv_item_text, areaData);
                        a_adapter.setDropDownViewResource(R.layout.layout_spinner_dropdown_item_center);
                        mAreaSpinner.setAdapter(a_adapter);
                    }
                });
            }

            @Override
            public void onFail() {
                //TODO
            }
        });

        priceList = new ArrayList<>();
        String[] items = getResources().getStringArray(R.array.array_room_filter_price);
        if (priceList.size() == 0) {
            Collections.addAll(priceList, items);
        }

        ArrayAdapter<String> pr_adapter = new ArrayAdapter<>(getApplicationContext(), R.layout.layout_spinner_item_center,
                R.id.tv_item_text, priceList);
        pr_adapter.setDropDownViewResource(R.layout.layout_spinner_dropdown_item_center);
        mPriceSpinner.setAdapter(pr_adapter);

        orderList = new ArrayList<>();
        items = getResources().getStringArray(R.array.array_room_filter_order);
        if (orderList.size() == 0) {
            Collections.addAll(orderList, items);
        }

        ArrayAdapter<String> od_adapter = new ArrayAdapter<>(getApplicationContext(), R.layout.layout_spinner_item_center,
                R.id.tv_item_text, orderList);
        od_adapter.setDropDownViewResource(R.layout.layout_spinner_dropdown_item_center);
        mOrderSpinner.setAdapter(od_adapter);
    }
}
