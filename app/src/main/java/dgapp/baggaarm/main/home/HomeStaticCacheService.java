package dgapp.baggaarm.main.home;

public class HomeStaticCacheService {

    private static HomeStaticCacheService instance;
    private HomeStaticCacheService() {}

    public static HomeStaticCacheService getInstance() {
        if (instance == null) {
            instance = new HomeStaticCacheService();
        }

        return instance;
    }

    public int homeFeatureIndex = 0;

    public int jobTypeIndex = 0;
    public int roomTypeIndex = 0;
    public int universityTypeIndex = 0;

    public int roomDisplayMode = 0;
}
