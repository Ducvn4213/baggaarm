package dgapp.baggaarm.main.account.follow.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import dgapp.baggaarm.R;
import dgapp.baggaarm.service.JobService;
import dgapp.baggaarm.service.UniversityService;
import dgapp.baggaarm.service.model.firebase.FB_Job;
import dgapp.baggaarm.service.model.firebase.FB_User;

public class FollowerListAdapter extends ArrayAdapter<FB_User> {

    public static interface OnFollowerItemNeedViewFacebook {
        void onRequestOpen(String link, String name);
    }

    private Context mContext;
    private List<FB_User> mData;
    private static LayoutInflater mInflater = null;

    private OnFollowerItemNeedViewFacebook delegate;

    public FollowerListAdapter(Context context, List<FB_User> data) {
        super(context, R.layout.layout_follower_item, data);

        mContext = context;
        mData = data;

        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setOnFollowerItemNeedViewFacebook(OnFollowerItemNeedViewFacebook delegate) {
        this.delegate = delegate;
    }

    public int getCount() {
        return mData.size();
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if(view == null) {
            view = mInflater.inflate(R.layout.layout_follower_item, null);
        }

        final FB_User data = mData.get(position);

        ImageView avatar = (ImageView) view.findViewById(R.id.iv_avatar);
        TextView name = (TextView) view.findViewById(R.id.tv_name);
        TextView info = (TextView) view.findViewById(R.id.tv_info);
        Button viewFacebook = (Button) view.findViewById(R.id.btn_view_facebook);

        viewFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FollowerListAdapter.this.delegate.onRequestOpen("http://facebook.com/" + data.facebookID, data.displayName);
            }
        });

        name.setText(data.displayName);

        if (data.facebookID == null || data.facebookID.isEmpty()) {
            viewFacebook.setVisibility(View.GONE);
        }
        else {
            viewFacebook.setVisibility(View.VISIBLE);
        }

        String uniName = UniversityService.getInstance().getUniversityName(data.studyingInformation.universityID);
        info.setText(uniName + " • K " + data.studyingInformation.startYear);

        if (!data.profileImageURL.isEmpty()) {
            Picasso.with(mContext).load(data.profileImageURL).into(avatar);
        }
        else {
            avatar.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.placeholder_image));
        }
        return view;
    }
}
