package dgapp.baggaarm.main.account;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import dgapp.baggaarm.R;
import dgapp.baggaarm.service.AuthenticationService;
import dgapp.baggaarm.service.callback.CommonCallback;
import dgapp.baggaarm.service.model.firebase.FB_User;
import dgapp.baggaarm.utils.BaseActivity;

import static java.security.AccessController.getContext;

public class SettingsBirthDayActivity extends BaseActivity implements DatePickerDialog.OnDateSetListener {

    ImageButton mBack;
    Button mChange;
    TextView mBirth;

    Calendar myCalendar = Calendar.getInstance();
    AuthenticationService mService;
    String birthDay = null;
    FB_User mCurrentUser = mService.getCurrentUser();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_setting_birth_day);

        mService = AuthenticationService.getInstance(getApplicationContext());

        bindingControls();
        setupControlEvents();
        initData();
    }

    void bindingControls() {
        mBack = (ImageButton) findViewById(R.id.btn_back);
        mChange = (Button) findViewById(R.id.btn_change_birth);
        mBirth = (TextView) findViewById(R.id.tv_birth_day);
    }

    void setupControlEvents() {
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SettingsBirthDayActivity.this.finish();
            }
        });

        mBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(SettingsBirthDayActivity.this, SettingsBirthDayActivity.this, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        mChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLoading();
                mService.changBirthDay(birthDay, new CommonCallback() {
                    @Override
                    public void onCompleted() {
                        SettingsBirthDayActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideLoading();
                                setResult(RESULT_OK);
                                SettingsBirthDayActivity.this.finish();
                            }
                        });
                    }

                    @Override
                    public void onFail() {
                        SettingsBirthDayActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideLoading();
                                showErrorDialog(getString(R.string.change_birth_fail));
                            }
                        });
                    }
                });
            }
        });
    }

    void initData() {
        mBirth.setText(mCurrentUser.birthday);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        myCalendar.set(Calendar.YEAR, year);
        myCalendar.set(Calendar.MONTH, month);
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        String myFormat = getString(R.string.date_format);
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);

        birthDay = sdf.format(myCalendar.getTime());
        String date = birthDay;
        date = date.replace("-20", " 20");
        date = date.replace("-19", " 19");
        date = date.replace("-", " tháng ");

        mBirth.setText(date);
        checkUI();
    }

    void checkUI() {
        if (birthDay == null) {
            mChange.setEnabled(false);
            return;
        }

        mChange.setEnabled(true);
    }
}
