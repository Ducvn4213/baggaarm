package dgapp.baggaarm.main.account.follow;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewParent;
import android.widget.ImageButton;

import dgapp.baggaarm.R;
import dgapp.baggaarm.main.account.follow.adapter.SettingFollowAdapter;
import dgapp.baggaarm.utils.BaseActivity;
import dgapp.baggaarm.utils.NonSwipeViewPager;
import dgapp.baggaarm.utils.SegmentedControl;

public class SettingFollowActivity extends BaseActivity {

    ImageButton mBack;
    SegmentedControl mSegmentedControl;
    NonSwipeViewPager mViewPager;

    SettingFollowAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_follow);

        bindingControls();
        setupControlEvents();
        initData();
    }

    void bindingControls() {
        mBack = (ImageButton) findViewById(R.id.ib_back);
        mSegmentedControl = (SegmentedControl) findViewById(R.id.sc_kind_segmented);
        mViewPager = (NonSwipeViewPager) findViewById(R.id.vp_container);
    }

    void setupControlEvents() {
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SettingFollowActivity.this.finish();
            }
        });

        mSegmentedControl.setOnSegmentedChangedListener(new SegmentedControl.OnSegmentedChangedListener() {
            @Override
            public void onSegmentedChanged(int index) {
                mViewPager.setCurrentItem(index, true);
            }
        });
    }

    void initData() {
        mAdapter = new SettingFollowAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mAdapter);

        mSegmentedControl.setTextA(getString(R.string.follow_following_title));
        mSegmentedControl.setTextB(getString(R.string.follow_followers_title));
    }
}
