package dgapp.baggaarm.main.account.follow;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import dgapp.baggaarm.R;
import dgapp.baggaarm.utils.BaseActivity;
import dgapp.baggaarm.utils.WebViewActivity;

public class FollowerInfoActivity extends BaseActivity {

    String id, name, avatar, relationship, face_id, university, major, year;

    TextView mName;
    TextView mUniversityName;
    TextView mMajor;
    TextView mSeason;
    ImageView mHeartBackground;
    ImageView mAvatar;
    ImageButton mRelationshipStatus;

    ImageButton mBack;
    Button mViewFacebook;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_follower_info);

        bindingControls();
        setupControlEvents();
        initData();
    }

    void bindingControls() {
        mName = (TextView) findViewById(R.id.tv_name);
        mUniversityName = (TextView) findViewById(R.id.tv_university_name);
        mMajor = (TextView) findViewById(R.id.tv_major);
        mSeason = (TextView) findViewById(R.id.tv_season);
        mHeartBackground = (ImageView)  findViewById(R.id.iv_avatar_background);
        mAvatar = (ImageView)  findViewById(R.id.iv_avatar);
        mRelationshipStatus = (ImageButton) findViewById(R.id.ib_relationship_status);

        mBack = (ImageButton) findViewById(R.id.ib_back);
        mViewFacebook = (Button) findViewById(R.id.btn_view_facebook);
    }

    void setupControlEvents() {
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FollowerInfoActivity.this.finish();
            }
        });

        mViewFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FollowerInfoActivity.this, WebViewActivity.class);
                intent.putExtra("url", "http://facebook.com/" + face_id);
                intent.putExtra("title", name);
                startActivity(intent);
            }
        });
    }

    void initData() {
        Intent intent = getIntent();
        id = intent.getStringExtra("id");
        name = intent.getStringExtra("name");
        avatar = intent.getStringExtra("avatar");
        relationship = intent.getStringExtra("relationship");
        face_id = intent.getStringExtra("face_id");
        university = intent.getStringExtra("university");
        major = intent.getStringExtra("major");
        year = intent.getStringExtra("year");

        mName.setText(name);

        mUniversityName.setText(university);
        mMajor.setText(major);
        mSeason.setText("Khóa " + year);

        Animation scale = new ScaleAnimation(0, 1, 0, 1, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        scale.setDuration(1000);
        scale.setRepeatCount(Animation.INFINITE);
        scale.setRepeatMode(Animation.INFINITE);

        Animation alpha = new AlphaAnimation(0.0f,0.3f);
        scale.setDuration(1000);
        scale.setRepeatCount(Animation.INFINITE);
        scale.setRepeatMode(Animation.INFINITE);

        AnimationSet animSet = new AnimationSet(true);
        animSet.setFillEnabled(true);
        animSet.addAnimation(scale);
        animSet.addAnimation(alpha);

        mHeartBackground.setAnimation(animSet);

        if (!avatar.isEmpty()) {
            Picasso.with(FollowerInfoActivity.this).load(avatar).into(mAvatar);
        }

        if (relationship.equalsIgnoreCase("0")) {
            mRelationshipStatus.setImageResource(R.mipmap.ic_in_relationship);
        }
        else if (relationship.equalsIgnoreCase("1")) {
            mRelationshipStatus.setImageResource(R.mipmap.ic_alone);
            mHeartBackground.setVisibility(View.INVISIBLE);
            mHeartBackground.setAnimation(null);
        }
        else {
            mRelationshipStatus.setImageResource(R.mipmap.ic_fa);
            mHeartBackground.setVisibility(View.INVISIBLE);
            mHeartBackground.setAnimation(null);
        }
    }
}
