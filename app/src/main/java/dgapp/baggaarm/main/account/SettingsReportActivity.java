package dgapp.baggaarm.main.account;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import dgapp.baggaarm.R;
import dgapp.baggaarm.utils.BaseActivity;

public class SettingsReportActivity extends BaseActivity {

    ImageButton mBack;
    TextView mSupport;
    TextView mReport;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_report);

        bindingControls();
        setupControlEvents();
    }

    void bindingControls() {
        mBack = (ImageButton) findViewById(R.id.btn_back);
        mSupport = (TextView) findViewById(R.id.tv_support);
        mReport = (TextView) findViewById(R.id.tv_report);
    }

    void setupControlEvents() {
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SettingsReportActivity.this.finish();
            }
        });

        mSupport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                Uri data = Uri.parse("mailto:" + getString(R.string.support_email) + "?subject=" + getString(R.string.support_email_subject) + "" + "&body=" + "");
                intent.setData(data);
                startActivity(intent);
            }
        });

        mReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                Uri data = Uri.parse("mailto:" + getString(R.string.report_email) + "?subject=" + getString(R.string.support_email_subject)+ "" + "&body=" + "");
                intent.setData(data);
                startActivity(intent);
            }
        });
    }
}
