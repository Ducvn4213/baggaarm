package dgapp.baggaarm.main.account;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import dgapp.baggaarm.R;
import dgapp.baggaarm.login.signin.forgot_password.ForgotPasswordActivity;
import dgapp.baggaarm.service.AuthenticationService;
import dgapp.baggaarm.service.callback.CommonCallback;
import dgapp.baggaarm.utils.BaseActivity;

public class SettingsPasswordActivity extends BaseActivity {

    ImageButton mBack;
    EditText mOldPass;
    EditText mNewPass;
    EditText mNewPassAgain;
    Button mForgotPass;
    Button mChangePass;

    AuthenticationService mService;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_change_password);

        mService = AuthenticationService.getInstance(getApplicationContext());

        bindingControls();
        setupControlEvents();
    }

    void bindingControls() {
        mBack = (ImageButton) findViewById(R.id.btn_back);
        mOldPass = (EditText) findViewById(R.id.edt_old_pass);
        mNewPass = (EditText) findViewById(R.id.edt_new_pass);
        mNewPassAgain = (EditText) findViewById(R.id.edt_new_pass_again);
        mForgotPass = (Button) findViewById(R.id.btn_forgot_password);
        mChangePass = (Button) findViewById(R.id.btn_change_pass);
    }

    void setupControlEvents() {
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SettingsPasswordActivity.this.finish();
            }
        });

        mForgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingsPasswordActivity.this, ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });

        mChangePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onChangePass();
            }
        });

        mOldPass.addTextChangedListener(textWatcher);
        mNewPass.addTextChangedListener(textWatcher);
        mNewPassAgain.addTextChangedListener(textWatcher);
    }

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            //TODO: do nothing
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //TODO: do nothing
        }

        @Override
        public void afterTextChanged(Editable s) {
            String oldPass = mOldPass.getText().toString();
            String newPass = mNewPass.getText().toString();
            String newPassAgain = mNewPassAgain.getText().toString();

            if (oldPass.isEmpty() || newPass.isEmpty() || newPassAgain.isEmpty()) {
                mChangePass.setEnabled(false);
            }
            else {
                mChangePass.setEnabled(true);
            }
        }
    };

    void onChangePass() {
        String oldPass = mOldPass.getText().toString();
        String newPass = mNewPass.getText().toString();
        String newPassAgain = mNewPassAgain.getText().toString();

        if (oldPass == null || oldPass.isEmpty()) {
            showErrorDialog(getString(R.string.change_password_missing_old_pass));
            return;
        }

        if (newPass == null || newPass.isEmpty()) {
            showErrorDialog(getString(R.string.change_password_missing_new_pass));
            return;
        }

        if (!newPass.equalsIgnoreCase(newPassAgain)) {
            showErrorDialog(getString(R.string.change_password_pass_not_match));
            return;
        }

        showLoading();
        mService.changePassword(oldPass, newPass, new CommonCallback() {
            @Override
            public void onCompleted() {
                SettingsPasswordActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        setResult(RESULT_OK);
                        SettingsPasswordActivity.this.finish();
                        return;
                    }
                });
            }

            @Override
            public void onFail() {
                SettingsPasswordActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showErrorDialog(getString(R.string.change_password_fail));
                        return;
                    }
                });
            }
        });
    }
}
