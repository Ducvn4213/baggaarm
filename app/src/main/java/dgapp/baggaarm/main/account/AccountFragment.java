package dgapp.baggaarm.main.account;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import dgapp.baggaarm.R;
import dgapp.baggaarm.main.home.child.event.saved.SavedEventsActivity;
import dgapp.baggaarm.main.home.child.job.job_list.saved.SavedJobsActivity;
import dgapp.baggaarm.main.home.child.room.room_list.my_post.MyRoomPostActivity;
import dgapp.baggaarm.service.AuthenticationService;
import dgapp.baggaarm.service.UniversityService;
import dgapp.baggaarm.service.model.firebase.FB_User;
import dgapp.baggaarm.utils.BaseFragment;
import dgapp.baggaarm.utils.WebViewActivity;

import static android.app.Activity.RESULT_OK;

public class AccountFragment extends BaseFragment {

    private final int EDIT_PROFILE_REQUEST_CODE = 7894;

    TextView mName;
    TextView mUniversityName;
    TextView mMajor;
    TextView mSeason;
    ImageView mHeartBackground;
    ImageView mAvatar;
    ImageButton mSettings;
    ImageButton mRelationshipStatus;

    ImageView mSavedJobs;
    ImageView mSavedRooms;
    ImageView mSavedUniversityPosts;
    ImageView mSavedEvents;

    Button mViewFacebook;
    Button mEdit;

    AuthenticationService mService;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account, container, false);

        mService = AuthenticationService.getInstance(getContext());

        bindingControls(view);
        setupControlEvents();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        init();
    }

    void bindingControls(View view) {
        mName = (TextView) view.findViewById(R.id.tv_name);
        mUniversityName = (TextView) view.findViewById(R.id.tv_university_name);
        mMajor = (TextView) view.findViewById(R.id.tv_major);
        mSeason = (TextView) view.findViewById(R.id.tv_season);
        mHeartBackground = (ImageView)  view.findViewById(R.id.iv_avatar_background);
        mAvatar = (ImageView)  view.findViewById(R.id.iv_avatar);
        mSettings = (ImageButton) view.findViewById(R.id.ib_settings);
        mRelationshipStatus = (ImageButton) view.findViewById(R.id.ib_relationship_status);

        mSavedJobs = (ImageView) view.findViewById(R.id.iv_saved_job);
        mSavedRooms = (ImageView) view.findViewById(R.id.iv_saved_room);
        mSavedUniversityPosts = (ImageView) view.findViewById(R.id.iv_saved_university_post);
        mSavedEvents = (ImageView) view.findViewById(R.id.iv_saved_event);

        mViewFacebook = (Button) view.findViewById(R.id.btn_view_facebook);
        mEdit = (Button) view.findViewById(R.id.btn_edit_user);
    }

    void setupControlEvents() {
        mEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), EditAccountActivity.class);
                startActivityForResult(intent, EDIT_PROFILE_REQUEST_CODE);
            }
        });

        mViewFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), WebViewActivity.class);
                intent.putExtra("url", "http://facebook.com/" + mService.getCurrentUser().facebookID);
                intent.putExtra("title", mService.getCurrentUser().displayName);
                startActivity(intent);
            }
        });

        mSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SettingsActivity.class);
                startActivity(intent);
            }
        });

        mSavedJobs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SavedJobsActivity.class);
                startActivity(intent);
            }
        });

        mSavedRooms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MyRoomPostActivity.class);
                startActivity(intent);
            }
        });

        mSavedUniversityPosts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO
            }
        });

        mSavedEvents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SavedEventsActivity.class);
                startActivity(intent);
            }
        });
    }

    void init() {
        FB_User user = mService.getCurrentUser();

        mName.setText(user.displayName);

        String uniName = UniversityService.getInstance().getUniversityName(user.studyingInformation.universityID);
        String major = UniversityService.getInstance().getMajorName(user.studyingInformation.specialityID);

        mUniversityName.setText(uniName);
        mMajor.setText(major);
        mSeason.setText("Khóa " + user.studyingInformation.startYear);

        Animation scale = new ScaleAnimation(0, 1, 0, 1, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        scale.setDuration(1000);
        scale.setRepeatCount(Animation.INFINITE);
        scale.setRepeatMode(Animation.INFINITE);

        Animation alpha = new AlphaAnimation(0.0f,0.3f);
        scale.setDuration(1000);
        scale.setRepeatCount(Animation.INFINITE);
        scale.setRepeatMode(Animation.INFINITE);

        AnimationSet animSet = new AnimationSet(true);
        animSet.setFillEnabled(true);
        animSet.addAnimation(scale);
        animSet.addAnimation(alpha);

        mHeartBackground.setAnimation(animSet);

        if (!user.profileImageURL.isEmpty()) {
            Picasso.with(getContext()).load(user.profileImageURL).into(mAvatar);
        }

        switch (user.relationshipStatus) {
            case 0:
                mRelationshipStatus.setImageResource(R.mipmap.ic_in_relationship);
                break;
            case 1:
                mRelationshipStatus.setImageResource(R.mipmap.ic_alone);
                mHeartBackground.setVisibility(View.INVISIBLE);
                mHeartBackground.setAnimation(null);
                break;
            case 2:
                mRelationshipStatus.setImageResource(R.mipmap.ic_fa);
                mHeartBackground.setVisibility(View.INVISIBLE);
                mHeartBackground.setAnimation(null);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == EDIT_PROFILE_REQUEST_CODE && resultCode == RESULT_OK) {
            String title = getString(R.string.app_name_re_mask);
            String message = getString(R.string.edit_account_success);
            showDialog(title, message);
        }
    }
}
