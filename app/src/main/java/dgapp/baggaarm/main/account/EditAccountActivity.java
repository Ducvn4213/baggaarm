package dgapp.baggaarm.main.account;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import dgapp.baggaarm.R;
import dgapp.baggaarm.service.AuthenticationService;
import dgapp.baggaarm.service.UniversityService;
import dgapp.baggaarm.service.callback.CommonCallback;
import dgapp.baggaarm.service.model.firebase.FB_User;
import dgapp.baggaarm.utils.BaseActivity;

public class EditAccountActivity extends BaseActivity {

    private static final int PERMISSIONS_CAMERA_REQUEST = 8761;
    private final int PICK_IMAGE_REQUEST_CODE = 9999;
    private final int TAKE_IMAGE_REQUEST_CODE = 9998;

    enum gender {
        MALE,
        FEMALE,
        GAY
    }

    enum relationship {
        IN_RELATIONSHIP,
        ALONE,
        FA
    }

    ImageButton mBack;
    EditText mName;
    EditText mDescription;
    TextView mUniversityName;
    TextView mMajor;
    TextView mSeason;
    ImageView mAvatar;
    ImageButton mRelationshipStatus;
    RelativeLayout mChangeAvatarContainer;
    TextView mChangeAvatarTextView;

    ImageView mMaleImage;
    ImageView mFemaleImage;
    ImageView mGayImage;

    ImageView mInRelationshipImage;
    ImageView mAloneImage;
    ImageView mFAImage;
    TextView mInRelationshipTextView;
    TextView mAloneTextView;
    TextView mFATextView;

    Button mSave;

    AuthenticationService mService;
    gender currentGender;
    relationship currentRelationship;
    Bitmap currentSelectedImage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_account);

        mService = AuthenticationService.getInstance(getApplicationContext());

        bindingControls();
        setupControlEvents();
        initData();
    }

    void bindingControls() {
        mBack = (ImageButton) findViewById(R.id.ib_back);
        mSave = (Button) findViewById(R.id.btn_save);

        mName = (EditText) findViewById(R.id.edt_name);
        mDescription = (EditText) findViewById(R.id.edt_description);
        mUniversityName = (TextView) findViewById(R.id.tv_university_name);
        mMajor = (TextView) findViewById(R.id.tv_major);
        mSeason = (TextView) findViewById(R.id.tv_season);
        mAvatar = (ImageView) findViewById(R.id.iv_avatar);
        mRelationshipStatus = (ImageButton) findViewById(R.id.ib_relationship_status);
        mChangeAvatarContainer = (RelativeLayout) findViewById(R.id.rl_change_avatar);
        mChangeAvatarTextView = (TextView) findViewById(R.id.tv_change_avatar);

        mFemaleImage = (ImageView) findViewById(R.id.iv_female);
        mMaleImage = (ImageView) findViewById(R.id.iv_male);
        mGayImage = (ImageView) findViewById(R.id.iv_gay);

        mInRelationshipImage = (ImageView) findViewById(R.id.iv_in_relationship);
        mAloneImage = (ImageView) findViewById(R.id.iv_alone);
        mFAImage = (ImageView) findViewById(R.id.iv_fa);
        mInRelationshipTextView = (TextView) findViewById(R.id.tv_in_relationship);
        mAloneTextView = (TextView) findViewById(R.id.tv_alone);
        mFATextView = (TextView) findViewById(R.id.tv_fa);
    }

    void setupControlEvents() {
        mSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doSaveData();
            }
        });

        mChangeAvatarContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestUpdateAvatar();
            }
        });

        mChangeAvatarTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestUpdateAvatar();
            }
        });

        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditAccountActivity.this.finish();
            }
        });

        mFemaleImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setGender(gender.FEMALE);
            }
        });

        mMaleImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setGender(gender.MALE);
            }
        });

        mGayImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setGender(gender.GAY);
            }
        });

        mInRelationshipImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setRelationship(relationship.IN_RELATIONSHIP);
            }
        });

        mInRelationshipTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setRelationship(relationship.IN_RELATIONSHIP);
            }
        });

        mAloneImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setRelationship(relationship.ALONE);
            }
        });

        mAloneTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setRelationship(relationship.ALONE);
            }
        });

        mFAImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setRelationship(relationship.FA);
            }
        });

        mFATextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setRelationship(relationship.FA);
            }
        });
    }

    void initData() {
        FB_User user = mService.getCurrentUser();

        mName.setText(user.displayName);
        mDescription.setText(user.about);

        String uniName = UniversityService.getInstance().getUniversityName(user.studyingInformation.universityID);
        String major = UniversityService.getInstance().getMajorName(user.studyingInformation.specialityID);

        mUniversityName.setText(uniName);
        mMajor.setText(major);
        mSeason.setText("Khóa " + user.studyingInformation.startYear);

        currentGender = user.gender == 2 ? gender.GAY : user.gender == 0 ? gender.MALE : gender.FEMALE;
        updateGenderUI();

         currentRelationship = user.relationshipStatus == 0 ? relationship.IN_RELATIONSHIP : user.relationshipStatus == 1 ? relationship.ALONE : relationship.FA;
        updateRelationshipUI();

        if (!user.profileImageURL.isEmpty()) {
            Picasso.with(EditAccountActivity.this).load(user.profileImageURL).into(mAvatar);
        }
    }

    void setGender(gender gender) {
        currentGender = gender;
        updateGenderUI();
    }

    void setRelationship(relationship relationship) {
        currentRelationship = relationship;
        updateRelationshipUI();
    }

    void updateGenderUI() {
        mFemaleImage.setColorFilter(Color.GRAY);
        mMaleImage.setColorFilter(Color.GRAY);
        mGayImage.setColorFilter(Color.GRAY);

        switch (currentGender) {
            case MALE:
                mMaleImage.setColorFilter(ContextCompat.getColor(EditAccountActivity.this, R.color.colorPrimary));
                break;
            case FEMALE:
                mFemaleImage.setColorFilter(ContextCompat.getColor(EditAccountActivity.this, R.color.colorPrimary));
                break;
            case GAY:
                mGayImage.setColorFilter(ContextCompat.getColor(EditAccountActivity.this, R.color.colorPrimary));
                break;
        }
    }

    void updateRelationshipUI() {
        mInRelationshipImage.setColorFilter(Color.GRAY);
        mAloneImage.setColorFilter(Color.GRAY);
        mFAImage.setColorFilter(Color.GRAY);
        mInRelationshipTextView.setTextColor(Color.GRAY);
        mAloneTextView.setTextColor(Color.GRAY);
        mFATextView.setTextColor(Color.GRAY);

        switch (currentRelationship) {
            case IN_RELATIONSHIP:
                mRelationshipStatus.setImageResource(R.mipmap.ic_in_relationship);
                mInRelationshipImage.setColorFilter(Color.RED);
                mInRelationshipTextView.setTextColor(Color.RED);
                break;
            case ALONE:
                mRelationshipStatus.setImageResource(R.mipmap.ic_alone);
                mAloneImage.setColorFilter(Color.RED);
                mAloneTextView.setTextColor(Color.RED);
                break;
            case FA:
                mRelationshipStatus.setImageResource(R.mipmap.ic_fa);
                mFAImage.setColorFilter(Color.RED);
                mFATextView.setTextColor(Color.RED);
                break;
        }
    }

    void requestUpdateAvatar() {
        final Dialog actionDialog = new Dialog(EditAccountActivity.this);
        actionDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        actionDialog.setCancelable(true);
        actionDialog.setContentView(R.layout.layout_change_image_action);

        TextView camera = (TextView) actionDialog.findViewById(R.id.tv_take_from_camera);
        TextView gallery = (TextView) actionDialog.findViewById(R.id.tv_get_from_gallery);

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionDialog.dismiss();
                checkCameraPermission();
            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionDialog.dismiss();
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                ActivityCompat.startActivityForResult(EditAccountActivity.this, Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST_CODE, null);
            }
        });

        actionDialog.show();
    }

    void checkCameraPermission() {
        List<String> permissions = new ArrayList<>();
        if (ContextCompat.checkSelfPermission(EditAccountActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            gotoTakeImageFromCamera();
        }
        else {
            permissions.add(Manifest.permission.CAMERA);
            String[] perArr = new String[permissions.size()];
            perArr = permissions.toArray(perArr);
            ActivityCompat.requestPermissions(EditAccountActivity.this,
                    perArr,
                    PERMISSIONS_CAMERA_REQUEST);
        }
    }

    void gotoTakeImageFromCamera() {
        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePicture.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePicture, TAKE_IMAGE_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSIONS_CAMERA_REQUEST) {
            if (grantResults.length > 0) {
                boolean isGrant = false;
                for (int gr : grantResults) {
                    if (gr == PackageManager.PERMISSION_GRANTED) {
                        isGrant = true;
                        break;
                    }
                }

                if (isGrant) {
                    gotoTakeImageFromCamera();
                    return;
                }
                else {
                    showErrorDialog(getString(R.string.dialog_message_missing_camera_permission));
                    return;
                }
            }
            else {
                showErrorDialog(getString(R.string.dialog_message_missing_camera_permission));
                return;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST_CODE && resultCode == RESULT_OK) {
            try {
                Uri filePath = data.getData();
                Bitmap newBitmapAvatar = MediaStore.Images.Media.getBitmap(EditAccountActivity.this.getContentResolver(), filePath);
                currentSelectedImage = newBitmapAvatar;
                mAvatar.setImageBitmap(newBitmapAvatar);
            }
            catch (Exception ex) {
                ex.printStackTrace();
                EditAccountActivity.this.showErrorDialog(getString(R.string.dialog_error_cant_set_image));
            }
        }

        if (requestCode == TAKE_IMAGE_REQUEST_CODE && resultCode == RESULT_OK) {
            try {
                Bitmap newBitmapAvatar = (Bitmap) data.getExtras().get("data");
                currentSelectedImage = newBitmapAvatar;
                mAvatar.setImageBitmap(newBitmapAvatar);
            }
            catch (Exception ex) {
                ex.printStackTrace();
                EditAccountActivity.this.showErrorDialog(getString(R.string.dialog_error_cant_set_image));
            }
        }
    }

    void doSaveData() {
        String name = mName.getText().toString();
        String about = mDescription.getText().toString();
        int gender = currentGender == EditAccountActivity.gender.GAY ? 2 : currentGender == EditAccountActivity.gender.MALE ? 0 : 1;
        int relationship = currentRelationship == EditAccountActivity.relationship.IN_RELATIONSHIP ? 0 : currentRelationship == EditAccountActivity.relationship.ALONE ? 1 : 2;


        if (name.isEmpty()) {
            showErrorDialog(getString(R.string.edit_account_name_empty));
            return;
        }

        showLoading();
        mService.updateProfile(currentSelectedImage, name, about, gender, relationship, new CommonCallback() {
            @Override
            public void onCompleted() {
                EditAccountActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        setResult(RESULT_OK);
                        EditAccountActivity.this.finish();
                    }
                });
            }

            @Override
            public void onFail() {
                EditAccountActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showErrorDialog(getString(R.string.edit_account_fail));
                    }
                });
            }
        });
    }
}
