package dgapp.baggaarm.main.account;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import dgapp.baggaarm.R;
import dgapp.baggaarm.login.LoginActivity;
import dgapp.baggaarm.main.account.follow.SettingFollowActivity;
import dgapp.baggaarm.service.AuthenticationService;
import dgapp.baggaarm.service.Configs;
import dgapp.baggaarm.service.model.firebase.FB_User;
import dgapp.baggaarm.utils.BaseActivity;
import dgapp.baggaarm.utils.WebViewActivity;

public class SettingsActivity extends BaseActivity {

    private final int SETTINGS_PASSWORD_REQUEST_CODE = 9569;
    private final int SETTINGS_BIRTH_DAY_REQUEST_CODE = 9568;

    TextView mEmail;
    TextView mLogout;
    LinearLayout mTerm;
    LinearLayout mPrivacyPolicy;
    LinearLayout mReport;
    LinearLayout mTeamInfo;
    LinearLayout mPassword;
    LinearLayout mContactInfo;
    LinearLayout mBirthDay;
    LinearLayout mFollow;
    ImageButton mBack;

    AuthenticationService mAuthService;
    FB_User mCurrentUser;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        mAuthService = AuthenticationService.getInstance(getApplicationContext());
        mCurrentUser = mAuthService.getCurrentUser();

        bindingControls();
        setupControlEvents();
        initData();
    }

    void bindingControls() {
        mEmail = (TextView) findViewById(R.id.tv_email);
        mBack = (ImageButton) findViewById(R.id.btn_back);
        mLogout = (TextView) findViewById(R.id.tv_logout);
        mTerm = (LinearLayout) findViewById(R.id.ll_term);
        mPrivacyPolicy = (LinearLayout) findViewById(R.id.ll_privacy_policy);
        mReport = (LinearLayout) findViewById(R.id.ll_report);
        mTeamInfo = (LinearLayout) findViewById(R.id.ll_team_info);
        mPassword = (LinearLayout) findViewById(R.id.ll_password);
        mContactInfo = (LinearLayout) findViewById(R.id.ll_contact_info);
        mBirthDay = (LinearLayout) findViewById(R.id.ll_birth_day);
        mFollow = (LinearLayout) findViewById(R.id.ll_follow);
    }

    void setupControlEvents() {
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SettingsActivity.this.finish();
            }
        });

        mFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingsActivity.this, SettingFollowActivity.class);
                startActivity(intent);
            }
        });

        mBirthDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingsActivity.this, SettingsBirthDayActivity.class);
                startActivityForResult(intent, SETTINGS_BIRTH_DAY_REQUEST_CODE);
            }
        });

        mContactInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog = new Dialog(SettingsActivity.this);
                dialog.setContentView(R.layout.layout_change_setting_personal_info_dialog);

                if (mCurrentUser.facebookID != null && !mCurrentUser.facebookID.isEmpty()) {
                    TextView faceID = (TextView) dialog.findViewById(R.id.tv_contact_facebook_id);
                    faceID.setText("https://facebook.com/" + mCurrentUser.facebookID);
                }
                dialog.show();
            }
        });

        mPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingsActivity.this, SettingsPasswordActivity.class);
                startActivityForResult(intent, SETTINGS_PASSWORD_REQUEST_CODE);
            }
        });

        mTeamInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingsActivity.this, WebViewActivity.class);
                intent.putExtra("url", Configs.TEAM_INFORMATION_LINK);
                intent.putExtra("title", getString(R.string.settings_section_app_info_developer_info));
                startActivity(intent);
            }
        });

        mTerm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingsActivity.this, WebViewActivity.class);
                intent.putExtra("url", Configs.TERM_LINK);
                intent.putExtra("title", getString(R.string.settings_section_app_info_term));
                startActivity(intent);
            }
        });

        mPrivacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingsActivity.this, WebViewActivity.class);
                intent.putExtra("url", Configs.CONDITION_LINK);
                intent.putExtra("title", getString(R.string.settings_section_app_info_privacy_policy));
                startActivity(intent);
            }
        });

        mLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog dialog = new AlertDialog.Builder(SettingsActivity.this)
                        .setTitle(R.string.settings_section_user_security_log_off)
                        .setMessage(R.string.logout_message)
                        .setNegativeButton(R.string.button_cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setPositiveButton(R.string.button_yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                doLogout();
                                dialogInterface.dismiss();
                            }
                        }).create();
                if (!dialog.isShowing()) {
                    dialog.show();
                }
            }
        });

        mReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingsActivity.this, SettingsReportActivity.class);
                startActivity(intent);
            }
        });
    }

    void initData() {
        mEmail.setText(mCurrentUser.email);
    }

    void doLogout() {
        mAuthService.logout();
        Intent intent = new Intent(SettingsActivity.this, LoginActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SETTINGS_PASSWORD_REQUEST_CODE && resultCode == RESULT_OK) {
            String title = getString(R.string.app_name_re_mask);
            String message = getString(R.string.change_password_success);
            showDialog(title, message);
        }

        if (requestCode == SETTINGS_BIRTH_DAY_REQUEST_CODE && resultCode == RESULT_OK) {
            String title = getString(R.string.app_name_re_mask);
            String message = getString(R.string.change_birth_success);
            showDialog(title, message);
        }

    }
}
