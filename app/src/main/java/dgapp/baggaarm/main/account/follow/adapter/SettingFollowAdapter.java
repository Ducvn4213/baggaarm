package dgapp.baggaarm.main.account.follow.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import dgapp.baggaarm.main.account.AccountFragment;
import dgapp.baggaarm.main.account.follow.fragment.FollowersFragment;
import dgapp.baggaarm.main.account.follow.fragment.FollowingFragment;
import dgapp.baggaarm.main.contacts.ContactsFragment;
import dgapp.baggaarm.main.home.HomeFragment;
import dgapp.baggaarm.main.notifications.NotificationsFragment;

public class SettingFollowAdapter extends FragmentStatePagerAdapter {

    public SettingFollowAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new FollowingFragment();
            case 1:
                return new FollowersFragment();
            default: break;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}

