package dgapp.baggaarm.main.account.follow.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import dgapp.baggaarm.R;
import dgapp.baggaarm.main.account.follow.adapter.FollowerListAdapter;
import dgapp.baggaarm.service.FollowService;
import dgapp.baggaarm.service.model.firebase.FB_User;
import dgapp.baggaarm.utils.BaseFragment;
import dgapp.baggaarm.utils.WebViewActivity;

public class FollowingFragment extends BaseFragment {

    LinearLayout mNoFollowers;
    ProgressBar mProgress;
    SwipeRefreshLayout mFollowersContainer;
    ListView mFollowers;

    FollowerListAdapter mAdapter;
    FollowService mService = FollowService.getInstance();

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_follow_following, container, false);

        bindingControls(view);
        setupControlEvents();
        init();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        doGetFollowing();
    }

    void bindingControls(View view) {
        mNoFollowers = (LinearLayout) view.findViewById(R.id.ll_no_followers);
        mProgress = (ProgressBar) view.findViewById(R.id.pb_progress);
        mFollowersContainer = (SwipeRefreshLayout) view.findViewById(R.id.srl_refresh_layout);
        mFollowers = (ListView) view.findViewById(R.id.lv_follower_list);
    }

    void setupControlEvents() {
        mFollowersContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                doGetFollowing();
            }
        });
    }

    void init() {
        mNoFollowers.setVisibility(View.GONE);
        mProgress.setVisibility(View.VISIBLE);
        mFollowersContainer.setVisibility(View.GONE);

        mAdapter = new FollowerListAdapter(getContext(), new ArrayList<FB_User>());
        mFollowers.setAdapter(mAdapter);

        mAdapter.setOnFollowerItemNeedViewFacebook(new FollowerListAdapter.OnFollowerItemNeedViewFacebook() {
            @Override
            public void onRequestOpen(String link, String name) {
                Intent intent = new Intent(getActivity(), WebViewActivity.class);
                intent.putExtra("url", link);
                intent.putExtra("title", name);
                startActivity(intent);
            }
        });
    }

    void onNoFollowers() {
        mNoFollowers.setVisibility(View.VISIBLE);
        mProgress.setVisibility(View.GONE);
        mFollowersContainer.setVisibility(View.GONE);
    }

    void doGetFollowing() {
        mService.getFollowing(getContext(), new FollowService.GetFollowersCallback() {
            @Override
            public void onGetFollowersSuccess(List<FB_User> data) {
                mFollowersContainer.setRefreshing(false);

                mNoFollowers.setVisibility(View.GONE);
                mProgress.setVisibility(View.GONE);
                mFollowersContainer.setVisibility(View.VISIBLE);

                mAdapter.clear();
                mAdapter.addAll(data);
                mAdapter.notifyDataSetChanged();

                if (data.size() == 0) {
                    onNoFollowers();
                }
            }

            @Override
            public void onGetFollowersFail() {
                //TODO
            }
        });
    }
}
