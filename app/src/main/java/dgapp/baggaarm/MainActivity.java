package dgapp.baggaarm;

import android.icu.text.MessagePattern;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;

import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import dgapp.baggaarm.main.adapter.MainFragmentAdapter;
import dgapp.baggaarm.utils.NonSwipeViewPager;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    public static interface PassBackHandle {
        void doBack(boolean onDeviceBackButton);
    }

    BottomNavigationViewEx mNavigation;
    NonSwipeViewPager mViewPager;

    MainFragmentAdapter mAdapter;
    PassBackHandle mPassBackHandle;

    private static MainActivity instance;
    public static MainActivity getInstance() {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bindingControls();
        setupControlsEvents();
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        instance = this;
    }

    void bindingControls() {
        mNavigation = (BottomNavigationViewEx) findViewById(R.id.navigation);
        mViewPager = (NonSwipeViewPager) findViewById(R.id.vp_container);
    }

    void setupControlsEvents() {
        mNavigation.setOnNavigationItemSelectedListener(this);
    }

    void init() {
        mNavigation.setTextVisibility(false);
        mNavigation.enableAnimation(false);
        mNavigation.enableShiftingMode(false);

        mAdapter = new MainFragmentAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mAdapter);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int olditem = mViewPager.getCurrentItem();
        switch (item.getItemId()) {
            case R.id.bar_home:
                mViewPager.setCurrentItem(0, true);
                if (olditem == 0) {
                    passBackToFragment(false);
                }
                return true;
            case R.id.bar_contacts:
                mViewPager.setCurrentItem(1, true);
                if (olditem == 1) {
                    passBackToFragment(false);
                }
                return true;
            case R.id.bar_notifications:
                mViewPager.setCurrentItem(2, true);
                if (olditem == 2) {
                    passBackToFragment(false);
                }
                return true;
            case R.id.bar_account:
                mViewPager.setCurrentItem(3, true);
                if (olditem == 3) {
                    passBackToFragment(false);
                }
                return true;
        }

        return false;
    }

    public void setPassBackHandle(PassBackHandle passBackHandle) {
        mPassBackHandle = passBackHandle;
    }

    void passBackToFragment(boolean onDeviceBackButton) {
        mPassBackHandle.doBack(onDeviceBackButton);
    }



    @Override
    public void onBackPressed() {
        passBackToFragment(true);
    }

    boolean doubleBackToExitPressedOnce = false;
    public void forceDoBack() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        doubleBackToExitPressedOnce = true;
        Toast.makeText(this, getString(R.string.double_back_to_exit_message), Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
}
