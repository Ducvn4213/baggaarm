package dgapp.baggaarm.service;

public class Configs {
    public static final int SPLASH_DISPLAY_LENGTH = 1000;
    public static final String TERM_LINK = "http://baggaarm.com/term-vi.html";
    public static final String CONDITION_LINK = "http://baggaarm.com/privacy-vi.html";
    public static final String TEAM_INFORMATION_LINK = "http://baggaarm.com/baggaarm-team.html";
}
