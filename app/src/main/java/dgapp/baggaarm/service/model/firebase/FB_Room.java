package dgapp.baggaarm.service.model.firebase;

public class FB_Room {
    public String key = "";
    public String imgURL = "";
    public String areaKey = "";
    public FB_RoomInformation roomInformation;

    public FB_RoomExtend roomExtend;

    public FB_Room() {}
}