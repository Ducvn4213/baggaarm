package dgapp.baggaarm.service.model.firebase;

public class FB_Notification {
    public String key = "";
    public String imgURL = "";
    public String address = "";
    public String title = "";
    public Long interestingCount;
    public Long saveCount;
    public Long willGoCount;
    public FB_EventTime time;
    public FB_EventTicket ticket;

    public boolean isInteresting = false;
    public boolean isSaved = false;
    public boolean isWillGo = false;
    public FB_EventExtend extend;
    public FB_EventHost host;

    public FB_Notification() {}
}