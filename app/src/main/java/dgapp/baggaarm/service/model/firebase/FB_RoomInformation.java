package dgapp.baggaarm.service.model.firebase;

public class FB_RoomInformation {
    public String address = "";
    public String contactPhoneNumber = "";
    public Long length;
    public Long price;
    public Long width;

    public FB_RoomInformation() {}
}