package dgapp.baggaarm.service.model;

import android.graphics.Bitmap;

import java.util.List;

public class AddNewRoomModel {
    public String title = null;
    public String description = null;
    public String address = null;
    public int height = -1;
    public int width = -1;
    public int cost = -1;
    public String phone = null;
    public List<String> areas = null;
    public Bitmap image = null;

    public AddNewRoomModel() {}

    public boolean enoughData() {
        if (height == -1 || width == -1 || cost == -1 || phone == null || address == null || areas == null || address.isEmpty() || phone.isEmpty() || areas.size() == 0) {
            return false;
        }

        return true;
    }
}
