package dgapp.baggaarm.service.model.firebase;

public class FB_EventHost {
    public String coverPhotoURL = "";
    public String displayName = "";
    public String fanpageURL = "";
    public String fullName = "";
    public String hostDescription = "";
    public String profilePhotoURL = "";
    public String universityID;

    public FB_EventHost() {}
}