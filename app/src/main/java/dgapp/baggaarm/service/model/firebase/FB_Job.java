package dgapp.baggaarm.service.model.firebase;

public class FB_Job {
    public String key = "";
    public String addressOfEmployer = "";
    public String addressOfJob = "";
    public String companyLogoURL = "";
    public String companyName = "";
    public String date = "";
    public String emailOfEmployer = "";
    public String employerName = "";
    public String expiredDate = "";
    public String jobCategoryID = "";
    public String jobDescription = "";
    public String jobRequirements = "";
    public String jobWelfare = "";
    public String language = "";
    public String note = "";
    public String phoneNumberOfEmployer = "";
    public String salary = "";
    public String timeOfJob = "";
    public String titleOfJob = "";
    public Boolean isSaved = false;

    public FB_Job() {}
}