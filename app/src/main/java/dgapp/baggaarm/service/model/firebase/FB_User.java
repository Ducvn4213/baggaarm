package dgapp.baggaarm.service.model.firebase;

import com.google.gson.annotations.SerializedName;

public class FB_User {
    @SerializedName("id")
    public String id = "";
    @SerializedName("about")
    public String about = "";
    @SerializedName("birthday")
    public String birthday = "";
    @SerializedName("display_name")
    public String displayName = "";
    @SerializedName("email")
    public String email = "";
    @SerializedName("facebook_id")
    public String facebookID = "";
    @SerializedName("first_name")
    public String firstName = "";
    @SerializedName("gender")
    public int gender;
    @SerializedName("profile_image_url")
    public String profileImageURL = "";
    @SerializedName("relationship_status")
    public int relationshipStatus;
    @SerializedName("study_information")
    public FB_StudyInformation studyingInformation;
    @SerializedName("surname")
    public String surname = "";
    @SerializedName("password")
    public String password = "";

    public FB_User() {}
}