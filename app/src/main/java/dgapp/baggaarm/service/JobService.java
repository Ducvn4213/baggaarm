package dgapp.baggaarm.service;

import android.content.Context;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import dgapp.baggaarm.main.home.child.job.job_list.adapter.CategoryListAdapter;
import dgapp.baggaarm.service.callback.CommonCallback;
import dgapp.baggaarm.service.model.firebase.FB_Job;
import dgapp.baggaarm.service.model.firebase.FB_JobCategory;

public class JobService {

    public enum JOB_TYPE {
        INTERN,
        PART_TIME
    }

    public static interface GetJobsCallback {
        void onCompleted (List <FB_Job> internJobs, List<FB_Job> partTimeJobs);
        void onFail();
    }

    private interface GetJobsKeyCallback {
        void onCompeted(List<String> keys);
        void onFail();
    }

    public static interface GetJobCallback {
        void onCompleted(FB_Job job);
        void onFail();
    }



    private static JobService instance;
    private JobService() {}

    public static JobService getInstance() {
        if (instance == null) {
            instance = new JobService();
        }

        return instance;
    }

    List<FB_JobCategory> mCategory = new ArrayList<>();
    List<FB_JobCategory> mCategoryIntern = new ArrayList<>();
    List<FB_JobCategory> mCategoryPartTime = new ArrayList<>();

    List<String> mInternFilterKeys = new ArrayList<>();
    List<String> mPartTimeFilterKeys = new ArrayList<>();

    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference REF_JOBS = database.getReference().child("jobs");
    DatabaseReference REF_JOBS_KEY = database.getReference().child("jobCategory-jobKeys");
    DatabaseReference REF_JOB_CATEGORY = database.getReference().child("jobCategories");
    DatabaseReference REF_MINI_JOBS = database.getReference().child("miniJobs");
    DatabaseReference REF_MY_SAVED = database.getReference().child("mySaved");


    public void setInternFilterKeys(List<String> filerKeys) {
        mInternFilterKeys = filerKeys;
    }

    public void setPartTimeFilterKeys(List<String> filerKeys) {
        mPartTimeFilterKeys = filerKeys;
    }

    public List<String> getInternFilterKeys() {
        return mInternFilterKeys;
    }

    public List<String> getPartTimeFilterKeys() {
        return mPartTimeFilterKeys;
    }

    public CategoryListAdapter getInternCategoryListAdapter(Context context) {
        return new CategoryListAdapter(context, mCategoryIntern);
    }

    public CategoryListAdapter getPartTimeCategoryListAdapter(Context context) {
        return new CategoryListAdapter(context, mCategoryPartTime);
    }

    public void saveJob(JOB_TYPE type, final String id, final CommonCallback callback) {
        final String user_id = mAuth.getCurrentUser().getUid();
        DatabaseReference mySaved = type == JOB_TYPE.INTERN ? REF_MY_SAVED.child(user_id).child("internshipJobs") : REF_MY_SAVED.child(user_id).child("parttimeJobs");

        mySaved.child(id).setValue(true, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                REF_JOBS.child(id).child("saves").child(user_id).setValue(true, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        callback.onCompleted();
                    }
                });
            }
        });
    }

    public void unSaveJob(JOB_TYPE type, final String id, final CommonCallback callback) {
        final String user_id = mAuth.getCurrentUser().getUid();
        DatabaseReference mySaved = type == JOB_TYPE.INTERN ? REF_MY_SAVED.child(user_id).child("internshipJobs") : REF_MY_SAVED.child(user_id).child("parttimeJobs");

        mySaved.child(id).removeValue(new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                REF_JOBS.child(id).child("saves").child(user_id).removeValue(new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        callback.onCompleted();
                    }
                });
            }
        });
    }

    public void getInternSavedJob(final GetJobsCallback callback) {
        final String id = mAuth.getCurrentUser().getUid();
        REF_MY_SAVED.child(id).child("internshipJobs").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<String> jobIDs = new ArrayList<String>();
                for (DataSnapshot d : dataSnapshot.getChildren()) {
                    jobIDs.add(d.getKey());
                }

                handleKeyList(JOB_TYPE.INTERN, jobIDs, callback);

                REF_MY_SAVED.child(id).child("internshipJobs").removeEventListener(this);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onFail();

                REF_MY_SAVED.child(id).child("internshipJobs").removeEventListener(this);
            }
        });
    }

    public void getPartTimeSavedJob(final GetJobsCallback callback) {
        final String id = mAuth.getCurrentUser().getUid();
        REF_MY_SAVED.child(id).child("parttimeJobs").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<String> jobIDs = new ArrayList<String>();
                for (DataSnapshot d : dataSnapshot.getChildren()) {
                    jobIDs.add(d.getKey());
                }

                handleKeyList(JOB_TYPE.PART_TIME, jobIDs, callback);

                REF_MY_SAVED.child(id).child("parttimeJobs").removeEventListener(this);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onFail();

                REF_MY_SAVED.child(id).child("parttimeJobs").removeEventListener(this);
            }
        });
    }

    public void getJobCategories(final CommonCallback callback) {
        REF_JOB_CATEGORY.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<DataSnapshot> data = new ArrayList<DataSnapshot>();
                for (DataSnapshot d : dataSnapshot.getChildren()) {
                    data.add(d);
                }

                getJobCategories(data, callback);

                REF_JOB_CATEGORY.removeEventListener(this);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onFail();

                REF_JOB_CATEGORY.removeEventListener(this);
            }
        });
    }

    private void getJobCategories(List<DataSnapshot> data, final CommonCallback callback) {
        if (data.size() == 0) {
            callback.onCompleted();
            return;
        }

        DataSnapshot _data = data.get(0);
        for (DataSnapshot d : _data.getChildren()) {
            final FB_JobCategory category = d.getValue(FB_JobCategory.class);

            category.key = d.getKey();
            if (_data.getKey().equalsIgnoreCase("internshipJobs")) {
                mCategoryIntern.add(category);
            }
            else {
                mCategoryPartTime.add(category);
            }
            mCategory.add(category);
        }

        data.remove(0);
        getJobCategories(data, callback);
    }

    public String getNameCategoryByKey(String key) {
        for (FB_JobCategory cate : mCategory) {
            if (cate.key.equalsIgnoreCase(key)) {
                return cate.fullName;
            }
        }

        return key;
    }

    public void getJobDetail(final String key, final GetJobCallback callback) {
        final String user_id = mAuth.getCurrentUser().getUid();
        REF_JOBS.child(key).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                FB_Job job = dataSnapshot.getValue(FB_Job.class);

                job.key = key;

                DataSnapshot saves = dataSnapshot.child("saves");
                for (DataSnapshot d : saves.getChildren()) {
                    if (d.getKey().equalsIgnoreCase(user_id)) {
                        job.isSaved = true;
                        callback.onCompleted(job);
                        return;
                    }
                }

                job.isSaved = false;
                callback.onCompleted(job);

                REF_JOBS.child(key).removeEventListener(this);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onFail();

                REF_JOBS.child(key).removeEventListener(this);
            }
        });
    }

    public void getJobs(final JOB_TYPE type, final GetJobsCallback callback) {
        getJobsKey(type, new GetJobsKeyCallback() {
            @Override
            public void onCompeted(List<String> keys) {
                handleKeyList(type, keys, callback);
            }

            @Override
            public void onFail() {
                callback.onFail();
            }
        });
    }

    private void handleKeyList(JOB_TYPE type, List<String> keys, final GetJobsCallback callback) {
        List<FB_Job> callbackData = new ArrayList<>();
        downloadJobs(type, callbackData, keys, callback);
    }

    private void downloadJobs(final JOB_TYPE type, final List<FB_Job> callbackData, final List<String> keys, final GetJobsCallback callback) {
        if (keys.size() == 0) {
            if (type == JOB_TYPE.INTERN) {
                callback.onCompleted(callbackData, null);
            }
            else {
                callback.onCompleted(null, callbackData);
            }

            return;
        }

        final String key = keys.get(keys.size() - 1);
        getJobByKey(key, new GetJobCallback() {
            @Override
            public void onCompleted(FB_Job job) {
                if (job != null) {
                    job.key = key;
                    callbackData.add(job);
                }

                keys.remove(keys.size() - 1);
                downloadJobs(type, callbackData, keys, callback);
            }

            @Override
            public void onFail() {
                callback.onFail();
            }
        });
    }

    private void getJobByKey(final String key, final GetJobCallback callback) {
        final String user_id = mAuth.getCurrentUser().getUid();
        REF_MINI_JOBS.child(key).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                FB_Job job = dataSnapshot.getValue(FB_Job.class);

                if (job == null) {
                    callback.onCompleted(job);
                    return;
                }

                DataSnapshot saves = dataSnapshot.child("saves");
                for (DataSnapshot d : saves.getChildren()) {
                    if (d.getKey().equalsIgnoreCase(user_id)) {
                        job.isSaved = true;
                        callback.onCompleted(job);
                        return;
                    }
                }

                job.isSaved = false;
                callback.onCompleted(job);

                REF_MINI_JOBS.child(key).removeEventListener(this);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onFail();

                REF_MINI_JOBS.child(key).removeEventListener(this);
            }
        });
    }

    private void getJobsKey(final JOB_TYPE type, final GetJobsKeyCallback callback) {
        final String key = type == JOB_TYPE.INTERN ? "internshipJobs" : "parttimeJobs";
        REF_JOBS_KEY.child(key).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<String> keys = new ArrayList<String>();
                for (DataSnapshot major : dataSnapshot.getChildren()) {
                    for (DataSnapshot job : major.getChildren()) {
                        String key = job.getKey();
                        keys.add(key);
                    }
                }

                callback.onCompeted(keys);

                REF_JOBS_KEY.child(key).removeEventListener(this);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onFail();

                REF_JOBS_KEY.child(key).removeEventListener(this);
            }
        });
    }
}
