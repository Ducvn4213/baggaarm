package dgapp.baggaarm.service.callback;

public interface CommonCallback {
    public void onCompleted();
    public void onFail();
}