package dgapp.baggaarm.service;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import dgapp.baggaarm.service.callback.CommonCallback;
import dgapp.baggaarm.service.model.firebase.FB_Event;
import dgapp.baggaarm.service.model.firebase.FB_EventExtend;
import dgapp.baggaarm.service.model.firebase.FB_EventHost;
import dgapp.baggaarm.service.model.firebase.FB_User;

public class EventService {

    public interface GetEventsCallback {
        void onSuccess(List<FB_Event> data);
        void onFail();
    }

    public interface GetEventsKeyCallback {
        void onSuccess(List<String> data);
        void onFail();
    }

    private String filterKey;

    private static EventService instance;
    private EventService() {}

    public static EventService getInstance() {
        if (instance == null) {
            instance = new EventService();
        }

        return instance;
    }

    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference REF_UNIVERSITY_EVENTS = database.getReference().child("university-events");
    DatabaseReference REF_EVENTS = database.getReference().child("events");
    DatabaseReference REF_MY_SAVED = database.getReference().child("mySaved");
    DatabaseReference REF_EVENT_EXTENS = database.getReference().child("event-extens");
    DatabaseReference REF_EVENT_HOSTS = database.getReference().child("eventHosts");
    DatabaseReference REF_HOST_EVENTS = database.getReference().child("host-events");

    public void setFilterKey(String key) {
        filterKey = key;
    }

    public void saveEvent(final String hostID, final String id, final CommonCallback callback) {
        final String user_id = mAuth.getCurrentUser().getUid();
        final DatabaseReference mySaved = REF_MY_SAVED.child(user_id).child("events");

        REF_HOST_EVENTS.child(hostID).child(id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                REF_HOST_EVENTS.child(hostID).child(id).removeEventListener(this);
                Long bts = dataSnapshot.child("beginTimeStamp").getValue(Long.class);
                final long ets = dataSnapshot.child("endTimeStamp").getValue(Long.class);

                mySaved.child(id).child("beginTimeStamp").setValue(bts).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            mySaved.child(id).child("endTimeStamp").setValue(ets).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        REF_EVENTS.child(id).child("activities").child("saves").child(user_id).setValue(true).addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()) {
                                                    callback.onCompleted();
                                                }
                                                else {
                                                    callback.onFail();
                                                }
                                            }
                                        });
                                    }
                                    else {
                                        callback.onFail();
                                    }
                                }
                            });
                        }
                        else {
                            callback.onFail();
                        }
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onFail();
            }
        });
    }

    public void unSaveEvent(final String id, final CommonCallback callback) {
        final String user_id = mAuth.getCurrentUser().getUid();
        DatabaseReference mySaved = REF_MY_SAVED.child(user_id).child("events");

        mySaved.child(id).removeValue(new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                REF_EVENTS.child(id).child("activities").child("saves").child(user_id).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            callback.onCompleted();
                        }
                        else {
                            callback.onFail();
                        }
                    }
                });
            }
        });
    }

    public void interestEvent(final String id, final CommonCallback callback) {
        final String user_id = mAuth.getCurrentUser().getUid();
        REF_EVENTS.child(id).child("activities").child("interestings").child(user_id).setValue(true).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    callback.onCompleted();
                }
                else {
                    callback.onFail();
                }
            }
        });
    }

    public void unInterestEvent(final String id, final CommonCallback callback) {
        final String user_id = mAuth.getCurrentUser().getUid();

        REF_EVENTS.child(id).child("activities").child("interestings").child(user_id).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    callback.onCompleted();
                }
                else {
                    callback.onFail();
                }
            }
        });
    }

    public void willGoEvent(final String id, final CommonCallback callback) {
        final String user_id = mAuth.getCurrentUser().getUid();
        REF_EVENTS.child(id).child("activities").child("willGo").child(user_id).setValue(true).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    callback.onCompleted();
                }
                else {
                    callback.onFail();
                }
            }
        });
    }

    public void unWillGoEvent(final String id, final CommonCallback callback) {
        final String user_id = mAuth.getCurrentUser().getUid();
        REF_EVENTS.child(id).child("activities").child("willGo").child(user_id).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    callback.onCompleted();
                }
                else {
                    callback.onFail();
                }
            }
        });
    }

    public void getSavedEvent(final GetEventsCallback callback) {
        final String user_id = mAuth.getCurrentUser().getUid();
        REF_MY_SAVED.child(user_id).child("events").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                REF_MY_SAVED.child(user_id).child("events").removeEventListener(this);

                List<String> keys = new ArrayList<>();
                for (DataSnapshot d : dataSnapshot.getChildren()) {
                    keys.add(d.getKey());
                }

                List<FB_Event> returnValue = new ArrayList<FB_Event>();
                getEvents(returnValue, keys, callback);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onFail();
            }
        });
    }

    public void getEventsKey(Context context, final GetEventsKeyCallback callback) {
        FB_User user = AuthenticationService.getInstance(context).getCurrentUser();
        final String universityID = filterKey == null ? user.studyingInformation.universityID : filterKey;


        REF_UNIVERSITY_EVENTS.child(universityID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                REF_UNIVERSITY_EVENTS.child(universityID).removeEventListener(this);

                List<String> keys = new ArrayList<>();
                for (DataSnapshot d : dataSnapshot.getChildren()) {
                    keys.add(d.getKey());
                }

                List<FB_Event> returnValue = new ArrayList<FB_Event>();
                Collections.reverse(keys);
                callback.onSuccess(keys);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onFail();
            }
        });
    }

    public void getEventsFromKeys(final List<String> keys, final GetEventsCallback callback) {
        getEventsFromKeys(new ArrayList<FB_Event>(), keys, callback);
    }

    private void getEventsFromKeys(final List<FB_Event> returnValue, final List<String> keys, final GetEventsCallback callback) {
        if (keys.size() == 0) {
            callback.onSuccess(returnValue);
            return;
        }

        final String userID = mAuth.getCurrentUser().getUid();

        final String key = keys.get(0);
        REF_EVENTS.child(key).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final FB_Event event = dataSnapshot.getValue(FB_Event.class);

                if (event != null) {
                    event.interestingCount = dataSnapshot.child("activities").child("interestings").getChildrenCount();
                    for (DataSnapshot d : dataSnapshot.child("activities").child("interestings").getChildren()) {
                        if (d.getKey().equalsIgnoreCase(userID)) {
                            event.isInteresting = true;
                            break;
                        }
                    }

                    event.saveCount = dataSnapshot.child("activities").child("saves").getChildrenCount();
                    for (DataSnapshot d : dataSnapshot.child("activities").child("saves").getChildren()) {
                        if (d.getKey().equalsIgnoreCase(userID)) {
                            event.isSaved = true;
                            break;
                        }
                    }

                    event.willGoCount = dataSnapshot.child("activities").child("willGo").getChildrenCount();
                    for (DataSnapshot d : dataSnapshot.child("activities").child("willGo").getChildren()) {
                        if (d.getKey().equalsIgnoreCase(userID)) {
                            event.isWillGo= true;
                            break;
                        }
                    }

                    event.key = key;

                    REF_EVENT_EXTENS.child(key).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            final FB_EventExtend extend = dataSnapshot.getValue(FB_EventExtend.class);

                            extend.bookURL = dataSnapshot.child("bookTicketInfo").child("url").getValue(String.class);
                            extend.hostID = dataSnapshot.child("relations").child("hostID").getValue(String.class);
                            event.extend = extend;

                            returnValue.add(event);
                            keys.remove(0);
                            getEventsFromKeys(returnValue, keys, callback);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            callback.onFail();
                        }
                    });
                }
                else {
                    keys.remove(0);
                    getEventsFromKeys(returnValue, keys, callback);
                }

                REF_EVENTS.child(key).removeEventListener(this);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onFail();
            }
        });
    }

    private void getEvents(final List<FB_Event> returnValue, final List<String> keys, final GetEventsCallback callback) {
        if (keys.size() == 0) {
            callback.onSuccess(returnValue);
            return;
        }

        if (returnValue.size() != 0 && returnValue.size() % 5 == 0) {
            callback.onSuccess(returnValue);
        }

        final String userID = mAuth.getCurrentUser().getUid();

        final String key = keys.get(0);
        REF_EVENTS.child(key).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final FB_Event event = dataSnapshot.getValue(FB_Event.class);

                if (event != null) {
                    event.interestingCount = dataSnapshot.child("activities").child("interestings").getChildrenCount();
                    for (DataSnapshot d : dataSnapshot.child("activities").child("interestings").getChildren()) {
                        if (d.getKey().equalsIgnoreCase(userID)) {
                            event.isInteresting = true;
                            break;
                        }
                    }

                    event.saveCount = dataSnapshot.child("activities").child("saves").getChildrenCount();
                    for (DataSnapshot d : dataSnapshot.child("activities").child("saves").getChildren()) {
                        if (d.getKey().equalsIgnoreCase(userID)) {
                            event.isSaved = true;
                            break;
                        }
                    }

                    event.willGoCount = dataSnapshot.child("activities").child("willGo").getChildrenCount();
                    for (DataSnapshot d : dataSnapshot.child("activities").child("willGo").getChildren()) {
                        if (d.getKey().equalsIgnoreCase(userID)) {
                            event.isWillGo= true;
                            break;
                        }
                    }

                    event.key = key;

                    REF_EVENT_EXTENS.child(key).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            final FB_EventExtend extend = dataSnapshot.getValue(FB_EventExtend.class);

                            extend.bookURL = dataSnapshot.child("bookTicketInfo").child("url").getValue(String.class);
                            extend.hostID = dataSnapshot.child("relations").child("hostID").getValue(String.class);
                            event.extend = extend;

                            returnValue.add(event);
                            keys.remove(0);
                            getEvents(returnValue, keys, callback);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            callback.onFail();
                        }
                    });
                }
                else {
                    keys.remove(0);
                    getEvents(returnValue, keys, callback);
                }

                REF_EVENTS.child(key).removeEventListener(this);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onFail();
            }
        });
    }

    private void getEvents2(final List<FB_Event> returnValue, final List<String> keys, final GetEventsCallback callback) {
        if (keys.size() == 0) {
            callback.onSuccess(returnValue);
            return;
        }

        final String userID = mAuth.getCurrentUser().getUid();

        final String key = keys.get(0);
        REF_EVENTS.child(key).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final FB_Event event = dataSnapshot.getValue(FB_Event.class);

                if (event != null) {
                    event.interestingCount = dataSnapshot.child("activities").child("interestings").getChildrenCount();
                    for (DataSnapshot d : dataSnapshot.child("activities").child("interestings").getChildren()) {
                        if (d.getKey().equalsIgnoreCase(userID)) {
                            event.isInteresting = true;
                            break;
                        }
                    }

                    event.saveCount = dataSnapshot.child("activities").child("saves").getChildrenCount();
                    for (DataSnapshot d : dataSnapshot.child("activities").child("saves").getChildren()) {
                        if (d.getKey().equalsIgnoreCase(userID)) {
                            event.isSaved = true;
                            break;
                        }
                    }

                    event.willGoCount = dataSnapshot.child("activities").child("willGo").getChildrenCount();
                    for (DataSnapshot d : dataSnapshot.child("activities").child("willGo").getChildren()) {
                        if (d.getKey().equalsIgnoreCase(userID)) {
                            event.isWillGo= true;
                            break;
                        }
                    }

                    event.key = key;

                    REF_EVENT_EXTENS.child(key).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            final FB_EventExtend extend = dataSnapshot.getValue(FB_EventExtend.class);

                            extend.bookURL = dataSnapshot.child("bookTicketInfo").child("url").getValue(String.class);
                            extend.hostID = dataSnapshot.child("relations").child("hostID").getValue(String.class);
                            event.extend = extend;

                            returnValue.add(event);
                            keys.remove(0);
                            getEvents2(returnValue, keys, callback);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            callback.onFail();
                        }
                    });
                }
                else {
                    keys.remove(0);
                    getEvents2(returnValue, keys, callback);
                }

                REF_EVENTS.child(key).removeEventListener(this);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onFail();
            }
        });
    }

    public void getEventDetail(final String key, final GetEventsCallback callback) {
        final String userID = mAuth.getCurrentUser().getUid();
        REF_EVENTS.child(key).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                REF_EVENTS.child(key).removeEventListener(this);

                final FB_Event event = dataSnapshot.getValue(FB_Event.class);

                if (event != null) {
                    event.interestingCount = dataSnapshot.child("activities").child("interestings").getChildrenCount();
                    for (DataSnapshot d : dataSnapshot.child("activities").child("interestings").getChildren()) {
                        if (d.getKey().equalsIgnoreCase(userID)) {
                            event.isInteresting = true;
                            break;
                        }
                    }

                    event.saveCount = dataSnapshot.child("activities").child("saves").getChildrenCount();
                    for (DataSnapshot d : dataSnapshot.child("activities").child("saves").getChildren()) {
                        if (d.getKey().equalsIgnoreCase(userID)) {
                            event.isSaved = true;
                            break;
                        }
                    }

                    event.willGoCount = dataSnapshot.child("activities").child("willGo").getChildrenCount();
                    for (DataSnapshot d : dataSnapshot.child("activities").child("willGo").getChildren()) {
                        if (d.getKey().equalsIgnoreCase(userID)) {
                            event.isWillGo= true;
                            break;
                        }
                    }

                    event.key = key;

                    REF_EVENT_EXTENS.child(key).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            final FB_EventExtend extend = dataSnapshot.getValue(FB_EventExtend.class);

                            extend.bookURL = dataSnapshot.child("bookTicketInfo").child("url").getValue(String.class);
                            extend.hostID = dataSnapshot.child("relations").child("hostID").getValue(String.class);
                            event.extend = extend;

                            REF_EVENT_EXTENS.child(key).addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    REF_EVENT_EXTENS.child(key).removeEventListener(this);

                                    final FB_EventExtend extend = dataSnapshot.getValue(FB_EventExtend.class);

                                    extend.bookURL = dataSnapshot.child("bookTicketInfo").child("url").getValue(String.class);
                                    extend.hostID = dataSnapshot.child("relations").child("hostID").getValue(String.class);

                                    REF_EVENT_HOSTS.child(extend.hostID).addValueEventListener(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            REF_EVENT_HOSTS.child(extend.hostID).removeEventListener(this);

                                            FB_EventHost host = dataSnapshot.getValue(FB_EventHost.class);

                                            event.extend = extend;
                                            event.host = host;

                                            List<FB_Event> events = new ArrayList<FB_Event>();
                                            events.add(event);
                                            callback.onSuccess(events);
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                            callback.onFail();
                                        }
                                    });
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    callback.onFail();
                                }
                            });
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            callback.onFail();
                        }
                    });
                }
                else {
                    callback.onFail();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onFail();
            }
        });
    }

    public void getEventsFromClub(final String key, final GetEventsCallback callback) {
        REF_HOST_EVENTS.child(key).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<String> keys = new ArrayList<String>();
                for (DataSnapshot d : dataSnapshot.getChildren()) {
                    keys.add(d.getKey());
                }

                List<FB_Event> returnValue = new ArrayList<FB_Event>();
                getEvents2(returnValue, keys, callback);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onFail();
            }
        });
    }
}
