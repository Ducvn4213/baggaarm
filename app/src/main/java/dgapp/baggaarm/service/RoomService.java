package dgapp.baggaarm.service;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import dgapp.baggaarm.service.model.AddNewRoomModel;
import dgapp.baggaarm.service.model.firebase.FB_Room;
import dgapp.baggaarm.service.model.firebase.FB_RoomArea;
import dgapp.baggaarm.service.model.firebase.FB_RoomExtend;
import dgapp.baggaarm.service.model.firebase.FB_User;

public class RoomService {

    public static interface GetRoomsCallback {
        void onSuccess(List<FB_Room> data);
        void onFail();
    }

    public static interface GetAreasCallback {
        void onSuccess(List<FB_RoomArea> data);
        void onFail();
    }

    public static interface AddNewRoomCallback {
        void onSuccess(String newID);
        void onFail();
    }

    private static RoomService instance;
    private RoomService() {}

    public static RoomService getInstance() {
        if (instance == null) {
            instance = new RoomService();
        }

        return instance;
    }

    AddNewRoomModel addNewRoomInfoModel = new AddNewRoomModel();
    List<FB_RoomArea> areaList = new ArrayList<>();
    List<String> priceList = new ArrayList<>();
    List<String> orderList = new ArrayList<>();

    int areaFilterIndex = -1;
    int priceFilterIndex = -1;
    int orderFilterIndex = -1;

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    FirebaseStorage storage = FirebaseStorage.getInstance();
    DatabaseReference REF_ROOMS = database.getReference().child("roomPosts");
    DatabaseReference REF_AREAS = database.getReference().child("area-roomPosts");
    DatabaseReference REF_REGIONS_AREAS = database.getReference().child("regions-areas");
    DatabaseReference REF_ROOM_EXTEND = database.getReference().child("roomPost-extens");
    DatabaseReference REF_MY_POST = database.getReference().child("myPosts");

    public boolean addNewRoomIsReady() {
        if (!addNewRoomInfoModel.enoughData()) {
            return false;
        }

        if (addNewRoomInfoModel.image == null || addNewRoomInfoModel.title == null || addNewRoomInfoModel.title.isEmpty() || addNewRoomInfoModel.description == null || addNewRoomInfoModel.description.isEmpty()) {
            return false;
        }

        return true;
    }

    public void setAddNewRoomInfoModel(AddNewRoomModel data) {
        addNewRoomInfoModel = data;
    }

    public void setImageToAddNewRoom(Bitmap data) {
        addNewRoomInfoModel.image = data;
    }

    public void setTitleToAddNewRoom(String data) {
        addNewRoomInfoModel.title = data;
    }

    public void setDescriptionToAddNewRoom(String data) {
        addNewRoomInfoModel.description = data;
    }


    public void clearRoomFilter() {
        areaFilterIndex = -1;
        priceFilterIndex = -1;
        orderFilterIndex = -1;
    }

    public void setAreaFilterIndex(int index) {
        areaFilterIndex = index;
    }

    public void setPriceFilterIndex(int index) {
        priceFilterIndex = index;
    }

    public void setOrderFilterIndex(int index) {
        orderFilterIndex = index;
    }

    public void getFullRoom(final GetRoomsCallback callback) {
        if (areaFilterIndex != -1 || priceFilterIndex != -1 || orderFilterIndex != -1) {
            final String areaKey = areaFilterIndex < 1 ? null : areaList.get(areaFilterIndex - 1).key;
            final DatabaseReference areas = areaKey == null ? REF_AREAS : REF_AREAS.child(areaKey);

            areas.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    areas.removeEventListener(this);

                    List<String> roomKeys = new ArrayList<String>();
                    if (areaKey == null) {
                        for (DataSnapshot d : dataSnapshot.getChildren()) {
                            for (DataSnapshot dd : d.getChildren()) {
                                if(dd.getKey().equalsIgnoreCase("emptyRoomPosts")) {
                                    for (DataSnapshot ddd : dd.getChildren()) {
                                        if (!roomKeys.contains(ddd.getKey())) {
                                            roomKeys.add(ddd.getKey());
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else {
                        for (DataSnapshot dd : dataSnapshot.getChildren()) {
                            if(dd.getKey().equalsIgnoreCase("emptyRoomPosts")) {
                                for (DataSnapshot ddd : dd.getChildren()) {
                                    if (!roomKeys.contains(ddd.getKey())) {
                                        roomKeys.add(ddd.getKey());
                                    }
                                }
                            }
                        }
                    }

                    Collections.reverse(roomKeys);
                    getRoomFromKeys(new ArrayList<FB_Room>(), roomKeys, callback);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    callback.onFail();
                }
            });

            return;
        }

        REF_AREAS.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                REF_AREAS.removeEventListener(this);

                List<String> keys = new ArrayList<String>();
                for (DataSnapshot d : dataSnapshot.getChildren()) {
                    for (DataSnapshot dd : d.getChildren()) {
                        if(dd.getKey().equalsIgnoreCase("emptyRoomPosts")) {
                            for (DataSnapshot ddd : dd.getChildren()) {
                                if (!keys.contains(ddd.getKey())) {
                                    keys.add(ddd.getKey());
                                }
                            }
                        }
                    }
                }

                Collections.reverse(keys);
                getRoomFromKeys(new ArrayList<FB_Room>(), keys, callback);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void getMatchRoom(final GetRoomsCallback callback) {
        if (areaFilterIndex != -1 || priceFilterIndex != -1 || orderFilterIndex != -1) {
            final String areaKey = areaFilterIndex < 1 ? null : areaList.get(areaFilterIndex - 1).key;
            final DatabaseReference areas = areaKey == null ? REF_AREAS : REF_AREAS.child(areaKey);

            areas.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    areas.removeEventListener(this);

                    List<String> roomKeys = new ArrayList<String>();
                    if (areaKey == null) {
                        for (DataSnapshot d : dataSnapshot.getChildren()) {
                            for (DataSnapshot dd : d.getChildren()) {
                                if(dd.getKey().equalsIgnoreCase("findRoomatePosts")) {
                                    for (DataSnapshot ddd : dd.getChildren()) {
                                        if (!roomKeys.contains(ddd.getKey())) {
                                            roomKeys.add(ddd.getKey());
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else {
                        for (DataSnapshot dd : dataSnapshot.getChildren()) {
                            if(dd.getKey().equalsIgnoreCase("findRoomatePosts")) {
                                for (DataSnapshot ddd : dd.getChildren()) {
                                    if (!roomKeys.contains(ddd.getKey())) {
                                        roomKeys.add(ddd.getKey());
                                    }
                                }
                            }
                        }
                    }

                    Collections.reverse(roomKeys);
                    getRoomFromKeys(new ArrayList<FB_Room>(), roomKeys, callback);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    callback.onFail();
                }
            });

            return;
        }

        REF_AREAS.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                REF_AREAS.removeEventListener(this);

                List<String> roomKeys = new ArrayList<String>();
                for (DataSnapshot d : dataSnapshot.getChildren()) {
                    for (DataSnapshot dd : d.getChildren()) {
                        if(dd.getKey().equalsIgnoreCase("findRoomatePosts")) {
                            for (DataSnapshot ddd : dd.getChildren()) {
                                if (!roomKeys.contains(ddd.getKey())) {
                                    roomKeys.add(ddd.getKey());
                                }
                            }
                        }
                    }
                }

                Collections.reverse(roomKeys);
                getRoomFromKeys(new ArrayList<FB_Room>(), roomKeys, callback);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onFail();
            }
        });
    }

    public void getMyRoom(Context context, final GetRoomsCallback callback) {
        FB_User user = AuthenticationService.getInstance(context).getCurrentUser();
        REF_MY_POST.child(user.id).child("findRoomatePosts").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<String> keys = new ArrayList<String>();
                for (DataSnapshot d : dataSnapshot.getChildren()) {
                    keys.add(d.getKey());
                }

                Collections.reverse(keys);
                getRoomFromKeys(new ArrayList<FB_Room>(), keys, callback);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onFail();
            }
        });
    }

    private void getRoomFromKeys(final List<FB_Room> returnValue, final List<String> keys, final GetRoomsCallback callback) {
        if (keys.size() == 0) {
            callback.onSuccess(handleFilter(returnValue));
            return;
        }

        if (returnValue.size() > 0 && returnValue.size() % 5 == 0) {
            callback.onSuccess(handleFilter(returnValue));
        }

        final String key = keys.get(0);

        REF_ROOMS.child(key).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                REF_ROOMS.child(key).removeEventListener(this);

                FB_Room room = dataSnapshot.getValue(FB_Room.class);
                if (room != null) {
                    room.key = key;
                    returnValue.add(room);
                }

                keys.remove(0);
                getRoomFromKeys(returnValue, keys, callback);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    List<FB_Room> handleFilter(List<FB_Room> input) {
        List<FB_Room> afterFilterPrice = new ArrayList<>();
        List<FB_Room> afterFilterOrder = new ArrayList<>();
        if (priceFilterIndex < 1) {
            afterFilterPrice = input;
        }
        else {
            int min = getMinPrice();
            int max = getMaxPrice();

            for (FB_Room r : input) {
                if (r.roomInformation.price >= min && r.roomInformation.price <= max) {
                    afterFilterPrice.add(r);
                }
            }
        }

        if (orderFilterIndex < 1) {
            afterFilterOrder = afterFilterPrice;
        }
        else {
            if (orderFilterIndex == 1) {
                Collections.sort(afterFilterPrice, new Comparator<FB_Room>() {
                    @Override
                    public int compare(FB_Room o1, FB_Room o2) {
                        if (o1.roomInformation.price > o2.roomInformation.price) {
                            return 1;
                        }

                        return -1;
                    }
                });
            }
            else {
                Collections.sort(afterFilterPrice, new Comparator<FB_Room>() {
                    @Override
                    public int compare(FB_Room o1, FB_Room o2) {
                        if (o1.roomInformation.price > o2.roomInformation.price) {
                            return -1;
                        }

                        return 1;
                    }
                });
            }

            afterFilterOrder = afterFilterPrice;
        }

        return afterFilterOrder;
    }

    public void getAreas(final GetAreasCallback callback) {
        REF_REGIONS_AREAS.child("vi").child("hcmc").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<FB_RoomArea> returnValue = new ArrayList<>();
                for (DataSnapshot d : dataSnapshot.getChildren()) {
                    FB_RoomArea a = d.getValue(FB_RoomArea.class);
                    a.key = d.getKey();
                    returnValue.add(a);
                }

                areaList = returnValue;
                callback.onSuccess(returnValue);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onFail();
            }
        });
    }

    public void getRoomDetail(final String id, final GetRoomsCallback callback) {
        List<String> keys = new ArrayList<>();
        List<FB_Room> rooms = new ArrayList<>();

        keys.add(id);
        getRoomFromKeys(rooms, keys, new GetRoomsCallback() {
            @Override
            public void onSuccess(final List<FB_Room> data) {
                if (data.size() == 0) {
                    callback.onFail();
                    return;
                }

                final FB_Room room = data.get(0);
                REF_ROOM_EXTEND.child(id).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        REF_ROOM_EXTEND.child(id).removeEventListener(this);
                        FB_RoomExtend roomExtend = dataSnapshot.getValue(FB_RoomExtend.class);
                        room.roomExtend = roomExtend;

                        List<FB_Room> returnValue = new ArrayList<>();
                        returnValue.add(room);
                        callback.onSuccess(returnValue);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        callback.onFail();
                    }
                });
            }

            @Override
            public void onFail() {
                callback.onFail();
            }
        });
    }

    int getMinPrice() {
        if (priceFilterIndex == 1) {
            return 0;
        }

        if (priceFilterIndex == 2) {
            return 1000000;
        }

        if (priceFilterIndex == 3) {
            return 1500000;
        }

        if (priceFilterIndex == 4) {
            return 2000000;
        }

        return 3000000;
    }

    int getMaxPrice() {
        if (priceFilterIndex == 1) {
            return 1000000;
        }

        if (priceFilterIndex == 2) {
            return 1500000;
        }

        if (priceFilterIndex == 3) {
            return 2000000;
        }

        if (priceFilterIndex == 4) {
            return 3000000;
        }

        return 999999999;
    }

    public void addNewRoom(Context context, final AddNewRoomCallback callback) {
        FB_User user = AuthenticationService.getInstance(context).getCurrentUser();
        AddNewRoomModel data = addNewRoomInfoModel;
        final DatabaseReference newRoom = REF_ROOMS.push();

        newRoom.child("roomInformation").child("address").setValue(data.address);
        newRoom.child("roomInformation").child("contactPhoneNumber").setValue(data.phone);
        newRoom.child("roomInformation").child("length").setValue(data.height);
        newRoom.child("roomInformation").child("price").setValue(data.cost);
        newRoom.child("roomInformation").child("width").setValue(data.width);
        newRoom.child("title").setValue(data.title);
        newRoom.child("universityID").setValue(user.studyingInformation.universityID);


        Date curDate = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        newRoom.child("date").setValue(format.format(curDate));

        final DatabaseReference newRoomExtra = REF_ROOM_EXTEND.child(newRoom.getKey());
        newRoomExtra.child("content").setValue(data.description);
        newRoomExtra.child("title").setValue(data.title);
        newRoomExtra.child("type").setValue(2);
        newRoomExtra.child("relations").child("ownerUID").setValue(user.id);
        for (int i = 0; i < data.areas.size(); i++) {
            newRoomExtra.child("relations").child("nearestAreas").child(i + "").setValue(data.areas.get(i));
            REF_AREAS.child(data.areas.get(i)).child("findRoomatePosts").child(newRoom.getKey()).child("price").setValue(data.cost);
        }

        REF_MY_POST.child(user.id).child("findRoomatePosts").child(newRoom.getKey()).setValue(true);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        data.image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageData = baos.toByteArray();

        StorageReference storageRef = storage.getReference();
        StorageReference mountainsRef = storageRef.child("findRoomateImages/"+ newRoom.getKey() +".jpg");

        UploadTask uploadTask = mountainsRef.putBytes(imageData);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                callback.onFail();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                @SuppressWarnings("VisibleForTests") Uri downloadUrl = taskSnapshot.getDownloadUrl();
                newRoom.child("imgURL").setValue(downloadUrl.toString());
                callback.onSuccess(newRoom.getKey());
            }
        });
    }
}
