package dgapp.baggaarm.service;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import dgapp.baggaarm.service.model.firebase.FB_User;

public class FollowService {

    public static interface GetFollowersCallback {
        void onGetFollowersSuccess(List<FB_User> data);
        void onGetFollowersFail();
    }

    private static FollowService instance;
    private FollowService() {}

    public static FollowService getInstance() {
        if (instance == null) {
            instance = new FollowService();
        }

        return instance;
    }

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference REF_USERS = database.getReference().child("users");
    DatabaseReference REF_FOLLOWERS = database.getReference().child("relationshipFollowers");
    DatabaseReference REF_FOLLOWING = database.getReference().child("relationshipFollowing");

    public void getFollowers(Context context, final GetFollowersCallback callback) {
        AuthenticationService mAuthService = AuthenticationService.getInstance(context);
        final String id = mAuthService.getCurrentUser().id;
        REF_FOLLOWERS.child(id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                REF_FOLLOWERS.child(id).removeEventListener(this);
                List<String> ids = new ArrayList<String>();
                for (DataSnapshot d : dataSnapshot.getChildren()) {
                    ids.add(d.getKey());
                }

                getFollower(new ArrayList<FB_User>(), ids, callback);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                REF_FOLLOWERS.child(id).removeEventListener(this);
                callback.onGetFollowersFail();
            }
        });
    }

    public void getFollowing(Context context, final GetFollowersCallback callback) {
        AuthenticationService mAuthService = AuthenticationService.getInstance(context);
        final String id = mAuthService.getCurrentUser().id;
        REF_FOLLOWING.child(id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                REF_FOLLOWING.child(id).removeEventListener(this);
                List<String> ids = new ArrayList<String>();
                for (DataSnapshot d : dataSnapshot.getChildren()) {
                    ids.add(d.getKey());
                }

                getFollower(new ArrayList<FB_User>(), ids, callback);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                REF_FOLLOWING.child(id).removeEventListener(this);
                callback.onGetFollowersFail();
            }
        });
    }

    public void getFollower(final List<FB_User> returnValue, final List<String> ids, final GetFollowersCallback callback) {
        if (ids.size() == 0) {
            callback.onGetFollowersSuccess(returnValue);
            return;
        }

        if (returnValue.size() != 0 && returnValue.size() % 5 == 0) {
            callback.onGetFollowersSuccess(returnValue);
        }

        final String id = ids.get(0);
        REF_USERS.child(id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                REF_USERS.child(id).removeEventListener(this);
                final FB_User userData = dataSnapshot.getValue(FB_User.class);
                if (userData != null) {
                    userData.id = id;

                    returnValue.add(userData);
                }
                ids.remove(0);
                getFollower(returnValue, ids, callback);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                REF_USERS.child(id).removeEventListener(this);
                callback.onGetFollowersFail();
            }
        });
    }
}
