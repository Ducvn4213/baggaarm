package dgapp.baggaarm.service;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.List;

import dgapp.baggaarm.service.callback.CommonCallback;
import dgapp.baggaarm.service.model.firebase.FB_StudyInformation;
import dgapp.baggaarm.service.model.firebase.FB_User;
import dgapp.baggaarm.utils.Utils;

public class AuthenticationService {

    public static final String ERROR_INVALID_EMAIL = "The email address is badly formatted.";
    public static final String ERROR_WEAK_PASSWORD = "The given password is invalid.";
    public static final String ERROR_WRONG_PASSWORD = "The password is invalid or the user does not have a password.";
    public static final String ERROR_EMAIL_ALREADY_IN_USE = "The email address is already in use by another account.";
    public static final String ERROR_USER_NOT_FOUND = "There is no user record corresponding to this identifier. The user may have been deleted.";
    public static final String ERROR_NETWORK = "A network error (such as timeout, interrupted connection or unreachable host) has occurred.";
    public static final String UNVERIFIED = "UNVERIFIED";
    public static final String BLOCKED = "BLOCKED";

    public interface AuthenticationCallback {
        void onCompleted();
        void onFail(String error);
    }

    public static class SignUpData {
        public String birthday;
        public String displayName;
        public String email;
        public String password;
        public String firstName;
        public int gender;
        public FB_StudyInformation studyInformation;
        public String surname;

        public SignUpData() {}

        public FB_User getUserObject() {
            FB_User user = new FB_User();
            user.firstName = this.firstName;
            user.birthday = this.birthday;
            user.gender = this.gender;
            user.relationshipStatus = 1;
            user.email = this.email;
            user.displayName = this.displayName;
            user.surname = this.surname;
            user.studyingInformation = this.studyInformation;
            user.about = "";
            return user;
        }
    }

    private static AuthenticationService instance;
    private AuthenticationService() {}
    private static Context lastestContext;

    public static AuthenticationService getInstance(Context c) {
        lastestContext = c;
        if (instance == null) {
            instance = new AuthenticationService();
        }

        return instance;
    }

    FB_User mCurrentUser;

    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    FirebaseStorage storage = FirebaseStorage.getInstance();
    DatabaseReference REF_USERS = database.getReference().child("users");
    DatabaseReference REF_BLOCKED_USERS = database.getReference().child("blockedUsers");
    DatabaseReference REF_UNVERIFIED_USERS = database.getReference().child("unVerifiedUsers");

    public FB_User getCurrentUser() {
        return Utils.getSavedUser(lastestContext);
    }

    public void updateProfile(Bitmap avatar, final String displayName, final String about, final int gender, final int relationship, final CommonCallback callback) {
        if (avatar == null) {
            doUpdateGeneralInfo(displayName, about, gender, relationship, null, callback);
            return;
        }

        final String userID = mAuth.getCurrentUser().getUid();

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        avatar.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageData = baos.toByteArray();

        StorageReference storageRef = storage.getReference();
        StorageReference mountainsRef = storageRef.child("profileImages/"+ userID +".jpg");

        UploadTask uploadTask = mountainsRef.putBytes(imageData);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                callback.onFail();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                @SuppressWarnings("VisibleForTests") Uri downloadUrl = taskSnapshot.getDownloadUrl();
                doUpdateGeneralInfo(displayName, about, gender, relationship, downloadUrl.toString(), callback);
            }
        });
    }

    void doUpdateGeneralInfo(final String displayName, final String about, final int gender, final int relationship, final String avatarLink, final CommonCallback callback) {
        final String userID = mAuth.getCurrentUser().getUid();
        REF_USERS.child(userID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final FB_User userData = dataSnapshot.getValue(FB_User.class);
                userData.displayName = displayName;
                userData.about = about;
                userData.gender = gender;
                userData.relationshipStatus = relationship;
                userData.profileImageURL = avatarLink == null ? userData.profileImageURL : avatarLink;

                REF_USERS.child(userID).setValue(userData).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            saveUser(displayName, about, gender, relationship, avatarLink, new AuthenticationCallback() {
                                @Override
                                public void onCompleted() {
                                    callback.onCompleted();
                                }

                                @Override
                                public void onFail(String error) {
                                    callback.onFail();
                                }
                            });
                        }
                        else {
                            callback.onFail();
                        }
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onFail();
            }
        });
    }

    public void changBirthDay(String newBirth, final CommonCallback callback) {
        String id = mCurrentUser.id;
        REF_USERS.child(id).child("birthday").setValue(newBirth).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    saveUser(mCurrentUser.email, mCurrentUser.password, new AuthenticationCallback() {
                        @Override
                        public void onCompleted() {
                            callback.onCompleted();
                        }

                        @Override
                        public void onFail(String error) {
                            callback.onFail();
                        }
                    });
                }
                else {
                    callback.onFail();
                }
            }
        });
    }

    public void changePassword(String oldPass, final String newPass, final CommonCallback callback) {
        if (!oldPass.equalsIgnoreCase(mCurrentUser.password)) {
            callback.onFail();
            return;
        }

        mAuth.getCurrentUser().updatePassword(newPass).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    saveUser(mCurrentUser.email, newPass, new AuthenticationCallback() {
                        @Override
                        public void onCompleted() {
                            callback.onCompleted();
                        }

                        @Override
                        public void onFail(String error) {
                            callback.onFail();
                        }
                    });
                }
                else {
                    callback.onFail();
                }
            }
        });
    }

    public void logout() {
        mAuth.signOut();
    }

    public void login(final String email, final String password, final AuthenticationCallback callback) {
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    FirebaseUser user = task.getResult().getUser();
                    if (user.isEmailVerified()) {
                        checkBlockedUser(user.getUid(), new AuthenticationCallback() {
                            @Override
                            public void onCompleted() {
                                saveUser(email, password, callback);
                            }

                            @Override
                            public void onFail(String error) {
                                callback.onFail(error);
                            }
                        });
                    } else {
                        callback.onFail(UNVERIFIED);
                    }
                }
                else {
                    callback.onFail(task.getException().getMessage());
                }
            }
        });
    }

    void saveUser(final String email, final String pass, final AuthenticationCallback callback){
        final String userID = mAuth.getCurrentUser().getUid();
        REF_USERS.child(userID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final FB_User userData = dataSnapshot.getValue(FB_User.class);
                if (userData == null) {
                    callback.onFail(ERROR_USER_NOT_FOUND);
                    return;
                }

                userData.id = userID;
                userData.password = pass;

                mCurrentUser = userData;
                Utils.saveUser(lastestContext, mCurrentUser);
                callback.onCompleted();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onFail("");
            }
        });
    }

    void saveUser(final String name, final String about, final int gender, final int relationship, final String avatarLink, final AuthenticationCallback callback){
        final String userID = mAuth.getCurrentUser().getUid();
        REF_USERS.child(userID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final FB_User userData = dataSnapshot.getValue(FB_User.class);

                if (userData == null) {
                    callback.onFail("");
                    return;
                }

                userData.id = userID;
                mCurrentUser = userData;
                Utils.saveUser(lastestContext, mCurrentUser);
                callback.onCompleted();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onFail("");
            }
        });
    }

    void checkBlockedUser(String id, final AuthenticationCallback callback) {
        REF_BLOCKED_USERS.child(id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                callback.onCompleted();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onFail(BLOCKED);
            }
        });
    }

    public void register(final SignUpData signUpData, final AuthenticationCallback callback) {
        mAuth.createUserWithEmailAndPassword(signUpData.email, signUpData.password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    insertSignUpValue(signUpData, task, callback);
                }
                else {
                    callback.onFail(task.getException().getMessage());
                }
            }
        });
    }

    void insertSignUpValue(final SignUpData signUpData, final Task<AuthResult> _task, final AuthenticationCallback callback) {
        FB_User user = signUpData.getUserObject();
        String uid = _task.getResult().getUser().getUid();

        REF_UNVERIFIED_USERS.child(uid).setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    _task.getResult().getUser().sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                callback.onCompleted();
                            }
                            else {
                                callback.onFail(task.getException().getMessage());
                            }
                        }
                    });

                }
                else {
                    callback.onFail(task.getException().getMessage());
                }
            }
        });
    }

    void mergeUnVerifiedUserToUser(final String id, final AuthenticationCallback callback) {
        REF_UNVERIFIED_USERS.child(id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                FB_User user = dataSnapshot.getValue(FB_User.class);
                REF_USERS.child(id).setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        REF_UNVERIFIED_USERS.child(id).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                callback.onCompleted();
                            }
                        });
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //TODO
            }
        });
    }

    public void resetPassword(String email, final AuthenticationCallback callback) {
        mAuth.sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    callback.onCompleted();
                }
                else {
                    callback.onFail(task.getException().getMessage());
                }
            }
        });

    }

    public void resendVerifyEmail(final String email, final String password, final AuthenticationCallback callback) {
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    task.getResult().getUser().sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                callback.onCompleted();
                            }
                            else {
                                callback.onFail("");
                            }
                        }
                    });
                }
            }
        });
    }

    public void checkVerified(final String email, final String password, final AuthenticationCallback callback) {
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    if (task.getResult().getUser().isEmailVerified()) {
                        mergeUnVerifiedUserToUser(task.getResult().getUser().getUid(), new AuthenticationCallback() {
                            @Override
                            public void onCompleted() {
                                saveUser(email, password, callback);
                            }

                            @Override
                            public void onFail(String error) {
                                callback.onFail(error);
                            }
                        });
                    } else {
                        callback.onFail(UNVERIFIED);
                    }
                }
            }
        });
    }
}
