package dgapp.baggaarm.service;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import dgapp.baggaarm.service.callback.CommonCallback;
import dgapp.baggaarm.service.model.firebase.FB_Speciality;
import dgapp.baggaarm.service.model.firebase.FB_University;

public class UniversityService {

    private static UniversityService instance;
    private UniversityService() {}

    public static UniversityService getInstance() {
        if (instance == null) {
            instance = new UniversityService();
        }

        return instance;
    }

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference REF_UNIVERSITIES = database.getReference().child("universities");

    List<FB_Speciality> mMajorsData = new ArrayList<>();
    List<FB_University> mUniversitiesData = new ArrayList<>();

    public List<FB_University> getUniversitiesData() {
        return mUniversitiesData;
    }

    public List<FB_Speciality> getMajorsData() {
        return mMajorsData;
    }

    public String getUniversityName(String key) {
        for (FB_University fbu : mUniversitiesData) {
            if (fbu.ID.equalsIgnoreCase(key)) {
                return fbu.fullName;
            }
        }

        return null;
    }

    public String getMajorName(String key) {
        for (FB_Speciality fbs : mMajorsData) {
            if (fbs.ID.equalsIgnoreCase(key)) {
                return fbs.fullName;
            }
        }

        return null;
    }

    public List<FB_Speciality> getMajorsByUID(String uid) {
        if (mMajorsData == null || mMajorsData.size() == 0) {
            return null;
        }

        List<FB_Speciality> returnValue = new ArrayList<>();
        for (FB_Speciality fbs : mMajorsData) {
            if (fbs.UniversityID.equalsIgnoreCase(uid)) {
                returnValue.add(fbs);
            }
        }

        return returnValue;
    }

    public void getListAllUniversities(final CommonCallback callback) {
        REF_UNIVERSITIES.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<DataSnapshot> data = new ArrayList<DataSnapshot>();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    data.add(postSnapshot);
                }

                handleUniversityList(data, new CommonCallback() {
                    @Override
                    public void onCompleted() {
                        callback.onCompleted();
                    }

                    @Override
                    public void onFail() {
                        callback.onFail();
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onFail();
            }
        });
    }

    private void handleUniversityList(final List<DataSnapshot> data, final CommonCallback callback) {
        if (data.size() == 0) {
            callback.onCompleted();
            return;
        }
        DataSnapshot postSnapshot = data.get(0);
        REF_UNIVERSITIES.child(postSnapshot.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final FB_University university = dataSnapshot.getValue(FB_University.class);
                mUniversitiesData.add(university);

                data.remove(0);
                handleUniversityList(data, callback);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void getListAllSpecialities(final CommonCallback callback) {
        REF_UNIVERSITIES.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<DataSnapshot> data = new ArrayList<DataSnapshot>();
                for (final DataSnapshot rootPostSnapshot : dataSnapshot.getChildren()) {
                    data.add(rootPostSnapshot);
                }

                handleSpecialitiesList(data, new CommonCallback() {
                    @Override
                    public void onCompleted() {
                        callback.onCompleted();
                    }

                    @Override
                    public void onFail() {
                        callback.onFail();
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void handleSpecialitiesList(final List<DataSnapshot> data, final CommonCallback callback) {
        if (data.size() == 0) {
            callback.onCompleted();
            return;
        }
        final DataSnapshot postSnapshot = data.get(0);

        REF_UNIVERSITIES.child(postSnapshot.getKey()).child("specialities").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<DataSnapshot> _data = new ArrayList<DataSnapshot>();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    _data.add(postSnapshot);
                }

                handleSpecialitiesListWithUniversityID(postSnapshot.getKey(), _data, new CommonCallback() {
                    @Override
                    public void onCompleted() {
                        data.remove(0);
                        handleSpecialitiesList(data, callback);
                    }

                    @Override
                    public void onFail() {
                        callback.onFail();
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    void handleSpecialitiesListWithUniversityID(final String universityID, final List<DataSnapshot> data, final CommonCallback callback) {
        if (data.size() == 0) {
            callback.onCompleted();
            return;
        }
        DataSnapshot postSnapshot = data.get(0);

        REF_UNIVERSITIES.child(universityID).child("specialities").child(postSnapshot.getKey()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final FB_Speciality speciality = dataSnapshot.getValue(FB_Speciality.class);
                speciality.UniversityID = universityID;

                mMajorsData.add(speciality);
                data.remove(0);
                handleSpecialitiesListWithUniversityID(universityID, data, callback);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}