package dgapp.baggaarm.service.network;

public class Param {
    public String key;
    public String value;

    public Param() {}

    public Param(String key, String value) {
        this.key = key;
        this.value = value;
    }
}
