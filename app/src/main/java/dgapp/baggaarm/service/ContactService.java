package dgapp.baggaarm.service;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import dgapp.baggaarm.service.callback.CommonCallback;
import dgapp.baggaarm.service.model.firebase.FB_Speciality;
import dgapp.baggaarm.service.model.firebase.FB_StudyInformation;
import dgapp.baggaarm.service.model.firebase.FB_University;
import dgapp.baggaarm.service.model.firebase.FB_User;
import dgapp.baggaarm.service.network.Network;
import dgapp.baggaarm.service.network.Param;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

public class ContactService {

    private class ContactResponseGSONObject {
        @SerializedName("result")
        Boolean result;
        @SerializedName("data")
        List<ContactGSONOjbect> data;

        public ContactResponseGSONObject() {}
    }

    private class ShortContactResponseGSONObject {
        @SerializedName("result")
        Boolean result;
        @SerializedName("data")
        List<ShortContactGSONObject> data;

        public ShortContactResponseGSONObject() {}
    }

    private class ShortContactGSONObject {
        @SerializedName("id")
        String id;

        public ShortContactGSONObject() {}
    }

    private class ContactGSONOjbect {
        @SerializedName("id")
        String id;
        @SerializedName("display_name")
        String displayName;
        @SerializedName("facebook_id")
        String facebookID;
        @SerializedName("university_id")
        String universityID;
        @SerializedName("speciality_id")
        String majorID;
        @SerializedName("start_year")
        String startYear;
        @SerializedName("gender")
        String gender;
        @SerializedName("profile_image_url")
        String avatar;

        public ContactGSONOjbect() {}
    }

    public interface FilterContactsCallback {
        void onFilterSuccess(List<FB_User> data);
        void onFail();
    }

    private static ContactService instance;
    private ContactService() {}

    public static ContactService getInstance() {
        if (instance == null) {
            instance = new ContactService();
        }

        return instance;
    }

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference REF_UNIVERSITIES = database.getReference().child("universities");
    Network mNetwork = Network.getInstance();
    long currentCallbackID = 0;

    String mFilterName;
    String mFilterUniversityID;
    String mFilterMajorID;
    String mFilterStartYear;
    String mFilterGender;

    public void setCurrentSearchQuery(String data) {
        mFilterName = data;
    }

    public String getCurrentFilterUniversityID() {
        return mFilterUniversityID;
    }

    public void applyFilters(String uid, String mid, String sy, String gd) {
        mFilterUniversityID = uid;
        mFilterMajorID = mid;
        mFilterStartYear = sy;
        mFilterGender = gd;
    }

    public void getContactsIDWithCurrentFilter(String univerID, final FilterContactsCallback callback) {
        List<Param> params = new ArrayList<>();
        params.add(new Param("request", "get_contact_keys_with_search"));
        params.add(new Param("name", mFilterName));
        if (mFilterUniversityID != null && !mFilterUniversityID.isEmpty()) {
            params.add(new Param("university_id", mFilterUniversityID));
        }
        else {
            params.add(new Param("university_id", univerID));
        }

        if (mFilterMajorID != null && !mFilterMajorID.isEmpty()) {
            params.add(new Param("speciality_id", mFilterMajorID));
        }

        if (mFilterStartYear != null && !mFilterStartYear.isEmpty()) {
            params.add(new Param("start_year", mFilterStartYear));
        }

        if (mFilterGender != null && !mFilterGender.isEmpty()) {
            params.add(new Param("gender", mFilterGender));
        }

        String link = "http://baggaarm.tk/api/v1/rest_api.php";
        mNetwork.execute(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                try {
                    Gson gson = new Gson();

                    ShortContactResponseGSONObject contacts = gson.fromJson(response, ShortContactResponseGSONObject.class);

                    List<String> ids = new ArrayList<>();
                    for (ShortContactGSONObject scgo : contacts.data) {
                        ids.add(scgo.id);
                    }

                    FollowService.getInstance().getFollower(new ArrayList<FB_User>(), ids, new FollowService.GetFollowersCallback() {
                        @Override
                        public void onGetFollowersSuccess(List<FB_User> data) {
                            callback.onFilterSuccess(data);
                        }

                        @Override
                        public void onGetFollowersFail() {
                            callback.onFail();
                        }
                    });
                }
                catch (Exception ex) {
                    callback.onFail();
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail();
            }
        });
    }

    public void getContactsWithAppliedFilters(final FilterContactsCallback callback) {
        List<Param> params = new ArrayList<>();
        params.add(new Param("request", "search_with_filter"));
        params.add(new Param("university_id", mFilterUniversityID));
        params.add(new Param("speciality_id", mFilterMajorID));
        params.add(new Param("start_year", mFilterStartYear));
        params.add(new Param("gender", mFilterGender));

        String link = "http://baggaarm.tk/api/v1/rest_api.php";
        mNetwork.execute(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                try {
                    Gson gson = new Gson();

                    List<FB_User> returnValues = new ArrayList<FB_User>();
                    ContactResponseGSONObject contacts = gson.fromJson(response, ContactResponseGSONObject.class);

                    for (ContactGSONOjbect o : contacts.data) {
                        FB_User user = new FB_User();
                        user.id = o.id;
                        user.displayName = o.displayName;
                        user.gender = Integer.parseInt(o.gender);
                        user.profileImageURL = o.avatar;
                        user.facebookID = o.facebookID;

                        FB_StudyInformation si = new FB_StudyInformation();
                        si.startYear = Integer.parseInt(o.startYear);
                        si.specialityID = o.majorID;
                        si.universityID = o.universityID;

                        user.studyingInformation = si;

                        returnValues.add(user);
                    }

                    callback.onFilterSuccess(returnValues);
                }
                catch (Exception ex) {
                    callback.onFail();
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail();
            }
        });
    }

    public void getContactsWithName(final long callbackID, String id, String name, final FilterContactsCallback callback) {
        currentCallbackID = callbackID;
        List<Param> params = new ArrayList<>();
        params.add(new Param("request", "search_with_filter"));
        params.add(new Param("name", name));

        if (mFilterUniversityID != null && !mFilterUniversityID.isEmpty()) {
            params.add(new Param("university_id", mFilterUniversityID));
        }
        else {
            params.add(new Param("university_id", id));
        }

        if (mFilterMajorID != null && !mFilterMajorID.isEmpty()) {
            params.add(new Param("speciality_id", mFilterMajorID));
        }

        if (mFilterStartYear != null && !mFilterStartYear.isEmpty()) {
            params.add(new Param("start_year", mFilterStartYear));
        }

        if (mFilterGender != null && !mFilterGender.isEmpty()) {
            params.add(new Param("gender", mFilterGender));
        }


        String link = "http://baggaarm.tk/api/v1/rest_api.php";
        mNetwork.execute(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                if (callbackID != currentCallbackID) {
                    return;
                }
                try {
                    Gson gson = new Gson();

                    List<FB_User> returnValues = new ArrayList<FB_User>();
                    ContactResponseGSONObject contacts = gson.fromJson(response, ContactResponseGSONObject.class);

                    for (ContactGSONOjbect o : contacts.data) {
                        FB_User user = new FB_User();
                        user.id = o.id;
                        user.displayName = o.displayName;
                        user.gender = Integer.parseInt(o.gender);
                        user.profileImageURL = o.avatar;
                        user.facebookID = o.facebookID;

                        FB_StudyInformation si = new FB_StudyInformation();
                        si.startYear = Integer.parseInt(o.startYear);
                        si.specialityID = o.majorID;
                        si.universityID = o.universityID;

                        user.studyingInformation = si;

                        returnValues.add(user);
                    }

                    callback.onFilterSuccess(returnValues);
                }
                catch (Exception ex) {
                    callback.onFail();
                }
            }

            @Override
            public void onFail(String error) {
                if (callbackID != currentCallbackID) {
                    return;
                }
                callback.onFail();
            }
        });
    }

    public void getContactsWithUniversityID(String id, String page, final FilterContactsCallback callback) {
        List<Param> params = new ArrayList<>();
        params.add(new Param("request", "get_contacts"));
        params.add(new Param("university_id", id));
        params.add(new Param("off_set", page));

        String link = "http://baggaarm.tk/api/v1/rest_api.php";
        mNetwork.execute(link, params, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                try {
                    Gson gson = new Gson();

                    List<FB_User> returnValues = new ArrayList<FB_User>();
                    ContactResponseGSONObject contacts = gson.fromJson(response, ContactResponseGSONObject.class);

                    for (ContactGSONOjbect o : contacts.data) {
                        FB_User user = new FB_User();
                        user.id = o.id;
                        user.displayName = o.displayName;
                        user.gender = Integer.parseInt(o.gender);
                        user.profileImageURL = o.avatar;
                        user.facebookID = o.facebookID;

                        FB_StudyInformation si = new FB_StudyInformation();
                        si.startYear = Integer.parseInt(o.startYear);
                        si.specialityID = o.majorID;
                        si.universityID = o.universityID;

                        user.studyingInformation = si;

                        returnValues.add(user);
                    }

                    callback.onFilterSuccess(returnValues);
                }
                catch (Exception ex) {
                    callback.onFail();
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail();
            }
        });
    }
}