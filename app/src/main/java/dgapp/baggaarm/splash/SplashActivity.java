package dgapp.baggaarm.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;

import dgapp.baggaarm.MainActivity;
import dgapp.baggaarm.R;
import dgapp.baggaarm.login.LoginActivity;
import dgapp.baggaarm.service.JobService;
import dgapp.baggaarm.service.UniversityService;
import dgapp.baggaarm.service.callback.CommonCallback;

import static dgapp.baggaarm.service.Configs.SPLASH_DISPLAY_LENGTH;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        initData(new CommonCallback() {
            @Override
            public void onCompleted() {
                SplashActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        FirebaseAuth mAuth = FirebaseAuth.getInstance();
                        if(mAuth.getCurrentUser() != null) {
                            Intent mainIntent = new Intent(SplashActivity.this, MainActivity.class);
                            SplashActivity.this.startActivity(mainIntent);
                            SplashActivity.this.finish();
                            return;
                        }
                        Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
                        SplashActivity.this.startActivity(mainIntent);
                        SplashActivity.this.finish();
                    }
                });
            }

            @Override
            public void onFail() {
                //TODO
            }
        });
    }

    void initData(final CommonCallback callback) {
        final UniversityService universityService = UniversityService.getInstance();
        final JobService jobService = JobService.getInstance();
        universityService.getListAllUniversities(new CommonCallback() {
            @Override
            public void onCompleted() {
                universityService.getListAllSpecialities(new CommonCallback() {
                    @Override
                    public void onCompleted() {
                        jobService.getJobCategories(new CommonCallback() {
                            @Override
                            public void onCompleted() {
                                callback.onCompleted();
                            }

                            @Override
                            public void onFail() {
                                callback.onFail();
                            }
                        });
                    }

                    @Override
                    public void onFail() {
                        callback.onFail();
                    }
                });
            }

            @Override
            public void onFail() {
                callback.onFail();
            }
        });
    }
}
