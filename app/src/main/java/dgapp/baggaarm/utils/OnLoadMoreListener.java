package dgapp.baggaarm.utils;

public interface OnLoadMoreListener {
    void onLoadMore();
}
