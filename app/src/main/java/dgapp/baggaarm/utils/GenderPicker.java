package dgapp.baggaarm.utils;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import dgapp.baggaarm.R;

public class GenderPicker extends LinearLayout {

    public interface OnGenderChangeListener {
        void onGenderChange(GENDER gender);
    }

    public enum GENDER {
        male,
        female,
        other,
        none
    }

    private Button mMale;
    private Button mFemale;
    private Button mOther;

    private GENDER gender = GENDER.none;

    private Context mContext;
    private OnGenderChangeListener listener;

    public void setOnGenderChangeListener(OnGenderChangeListener listener) {
        this.listener = listener;
    }

    public GenderPicker(Context context) {
        this(context, null);
    }

    public GenderPicker(final Context context, AttributeSet attrs) {
        super(context, attrs);

        mContext = context;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.layout_gender_picker, this, true);

        bindingControls();
        setupControlEvents();
        updateUI();
    }

    void bindingControls() {
        mMale = (Button) findViewById(R.id.btn_male);
        mFemale = (Button) findViewById(R.id.btn_female);
        mOther = (Button) findViewById(R.id.btn_other);
    }

    void setupControlEvents() {
        mMale.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                gender = GENDER.male;
                listener.onGenderChange(gender);
                updateUI();
            }
        });

        mFemale.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                gender = GENDER.female;
                listener.onGenderChange(gender);
                updateUI();
            }
        });

        mOther.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                gender = GENDER.other;
                listener.onGenderChange(gender);
                updateUI();
            }
        });
    }

    void updateUI() {
        mMale.setTextColor(Color.GRAY);
        mFemale.setTextColor(Color.GRAY);
        mOther.setTextColor(Color.GRAY);

        switch (gender) {
            case male:
                mMale.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
                break;
            case female:
                mFemale.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
                break;
            case other:
                mOther.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
                break;
            default: break;
        }
    }
}
