package dgapp.baggaarm.utils;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import dgapp.baggaarm.R;
import dgapp.baggaarm.service.EventService;
import dgapp.baggaarm.service.callback.CommonCallback;
import dgapp.baggaarm.service.model.firebase.FB_Event;

public class EventItem extends LinearLayout {

    public interface EventItemDelegate {
        void onInterestingPrepareToChange(int index, boolean value);
        void onSavesPrepareToChange(int index, boolean value);
        void onWillGoPrepareToChange(int index, boolean value);

        void onInterestingChanged(int index, boolean value);
        void onSavesChanged(int index, boolean value);
        void onWillGoChanged(int index, boolean value);

        void onInterestingChangeFail(int index, boolean oldValue);
        void onSavesChangedFail(int index, boolean oldValue);
        void onWillGoChangedFail(int index, boolean oldValue);
    }

    ImageView image;
    TextView title;
    TextView address;
    TextView date;
    Button price;
    TextView interestingCount;
    TextView savedCount;
    TextView goCount;
    ImageView interestingImage;
    ImageView savedImage;
    ImageView willGoImage;

    LinearLayout interestingContainer;
    LinearLayout savesContainer;
    LinearLayout willGoContainer;

    private Context mContext;
    private FB_Event mData;
    private int mIndex;

    private EventItemDelegate mDelegate;

    public EventItem(Context context) {
        this(context, null);
    }

    public EventItem(final Context context, AttributeSet attrs) {
        super(context, attrs);

        mContext = context;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.layout_event_item, this, true);

        bindingControls();
        setupControlEvents();
        init();
    }

    public void setDelegate(EventItemDelegate delegate) {
        mDelegate = delegate;
    }

    void bindingControls() {
        image = (ImageView) findViewById(R.id.iv_image);
        title = (TextView) findViewById(R.id.tv_title);
        address = (TextView) findViewById(R.id.tv_address);
        date = (TextView) findViewById(R.id.tv_date);
        price = (Button) findViewById(R.id.btn_price);
        interestingCount = (TextView) findViewById(R.id.tv_interesting_count);
        savedCount = (TextView) findViewById(R.id.tv_saved_count);
        goCount = (TextView) findViewById(R.id.tv_go_count);
        interestingImage = (ImageView) findViewById(R.id.iv_interesting_count);
        savedImage = (ImageView) findViewById(R.id.iv_saved_count);
        willGoImage = (ImageView) findViewById(R.id.iv_will_go_count);

        interestingContainer = (LinearLayout) findViewById(R.id.ll_interesting_container);
        savesContainer = (LinearLayout) findViewById(R.id.ll_saves_container);
        willGoContainer = (LinearLayout) findViewById(R.id.ll_will_go_container);
    }

    void setupControlEvents() {
        interestingContainer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                EventService mService = EventService.getInstance();
                if (mData.isInteresting) {
                    mDelegate.onInterestingPrepareToChange(mIndex, false);
                    mService.unInterestEvent(mData.key, new CommonCallback() {
                        @Override
                        public void onCompleted() {
                            mDelegate.onInterestingChanged(mIndex, false);
                        }

                        @Override
                        public void onFail() {
                            mDelegate.onInterestingChangeFail(mIndex, false);
                        }
                    });
                }
                else {
                    mDelegate.onInterestingPrepareToChange(mIndex, true);
                    mService.interestEvent(mData.key, new CommonCallback() {
                        @Override
                        public void onCompleted() {
                            mDelegate.onInterestingChanged(mIndex, true);
                        }

                        @Override
                        public void onFail() {
                            mDelegate.onInterestingChangeFail(mIndex, true);
                        }
                    });
                }
            }
        });

        savesContainer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                EventService mService = EventService.getInstance();
                if (mData.isSaved) {
                    mDelegate.onSavesPrepareToChange(mIndex, false);
                    mService.unSaveEvent(mData.key, new CommonCallback() {
                        @Override
                        public void onCompleted() {
                            mDelegate.onSavesChanged(mIndex, false);
                        }

                        @Override
                        public void onFail() {
                            mDelegate.onSavesChangedFail(mIndex, false);
                        }
                    });
                }
                else {
                    mDelegate.onSavesPrepareToChange(mIndex, true);
                    mService.saveEvent(mData.extend.hostID, mData.key, new CommonCallback() {
                        @Override
                        public void onCompleted() {
                            mDelegate.onSavesChanged(mIndex, true);
                        }

                        @Override
                        public void onFail() {
                            mDelegate.onSavesChangedFail(mIndex, true);
                        }
                    });
                }
            }
        });

        willGoContainer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                EventService mService = EventService.getInstance();
                if (mData.isWillGo) {
                    mDelegate.onWillGoPrepareToChange(mIndex, false);
                    mService.unWillGoEvent(mData.key, new CommonCallback() {
                        @Override
                        public void onCompleted() {
                            mDelegate.onWillGoChanged(mIndex, false);
                        }

                        @Override
                        public void onFail() {
                            mDelegate.onWillGoChangedFail(mIndex, false);
                        }
                    });
                }
                else {
                    mDelegate.onWillGoPrepareToChange(mIndex, true);
                    mService.willGoEvent(mData.key, new CommonCallback() {
                        @Override
                        public void onCompleted() {
                            mDelegate.onWillGoChanged(mIndex, true);
                        }

                        @Override
                        public void onFail() {
                            mDelegate.onWillGoChangedFail(mIndex, true);
                        }
                    });
                }
            }
        });
    }

    void init() {
        //TODO
    }

    public void setData(int index, FB_Event data) {
        mIndex = index;
        mData = data;

        image.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.placeholder_image));
        Picasso.with(mContext).load(data.imgURL).into(image);
        title.setText(data.title);
        address.setText(data.address);
        date.setText(data.time.displayTime);
        price.setText(data.ticket.price + " VND");
        interestingCount.setText(data.interestingCount  + "");
        savedCount.setText(data.saveCount  + "");
        goCount.setText(data.willGoCount  + "");

        if (data.isInteresting) {
            interestingImage.setColorFilter(ContextCompat.getColor(mContext, R.color.colorPrimary));
        }
        else {
            interestingImage.setColorFilter(ContextCompat.getColor(mContext, android.R.color.darker_gray));
        }

        if (data.isSaved) {
            savedImage.setColorFilter(ContextCompat.getColor(mContext, R.color.colorPrimary));
        }
        else {
            savedImage.setColorFilter(ContextCompat.getColor(mContext, android.R.color.darker_gray));
        }

        if (data.isWillGo) {
            willGoImage.setColorFilter(ContextCompat.getColor(mContext, R.color.colorPrimary));
        }
        else {
            willGoImage.setColorFilter(ContextCompat.getColor(mContext, android.R.color.darker_gray));
        }
    }
}
