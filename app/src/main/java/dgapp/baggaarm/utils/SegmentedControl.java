package dgapp.baggaarm.utils;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import dgapp.baggaarm.R;

public class SegmentedControl extends LinearLayout {

    public static interface OnSegmentedChangedListener {
        void onSegmentedChanged(int index);
    }

    private Button mSegmentedA;
    private Button mSegmentedB;

    private Context mContext;

    private int selected = 0;

    private OnSegmentedChangedListener onSegmentedChangedListener;

    public void setOnSegmentedChangedListener(OnSegmentedChangedListener onSegmentedChangedListener) {
        this.onSegmentedChangedListener = onSegmentedChangedListener;
    }

    public SegmentedControl(Context context) {
        this(context, null);
    }

    public SegmentedControl(final Context context, AttributeSet attrs) {
        super(context, attrs);

        mContext = context;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.layout_segmented, this, true);

        bindingControls();
        setupControlEvents();
        init();
    }

    void bindingControls() {
        mSegmentedA = (Button) findViewById(R.id.btn_segmented_a);
        mSegmentedB = (Button) findViewById(R.id.btn_segmented_b);
    }

    void setupControlEvents() {
        mSegmentedA.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                selected = 0;
                onSegmentedChangedListener.onSegmentedChanged(0);
                updateUI();
            }
        });

        mSegmentedB.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                selected = 1;
                onSegmentedChangedListener.onSegmentedChanged(1);
                updateUI();
            }
        });
    }

    void init() {
        updateUI();
    }

    void updateUI() {
        if (selected == 0) {
            mSegmentedA.setTextColor(Color.WHITE);
            mSegmentedA.setBackground(ContextCompat.getDrawable(mContext, R.drawable.button_border_stroke_left_bottom_fill));
            mSegmentedB.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
            mSegmentedB.setBackground(ContextCompat.getDrawable(mContext, R.drawable.button_border_stroke_right_bottom));
        }
        else {
            mSegmentedB.setTextColor(Color.WHITE);
            mSegmentedB.setBackground(ContextCompat.getDrawable(mContext, R.drawable.button_border_stroke_right_bottom_fill));
            mSegmentedA.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
            mSegmentedA.setBackground(ContextCompat.getDrawable(mContext, R.drawable.button_border_stroke_left_bottom));
        }
    }

    public void setSelected(int index) {
        selected = index;
        updateUI();
    }

    public int getSelected() {
        return selected;
    }

    public void setTextA(String data) {
        mSegmentedA.setText(data);
    }

    public void setTextB(String data) {
        mSegmentedB.setText(data);
    }
}
