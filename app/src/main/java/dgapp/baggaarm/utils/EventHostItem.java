package dgapp.baggaarm.utils;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import dgapp.baggaarm.R;
import dgapp.baggaarm.service.EventService;
import dgapp.baggaarm.service.callback.CommonCallback;
import dgapp.baggaarm.service.model.firebase.FB_Event;
import dgapp.baggaarm.service.model.firebase.FB_EventHost;

public class EventHostItem extends LinearLayout {

    public static interface OnEventHostItemViewMoreClick {
        void onViewMoreClick();
    }

    ImageView image;
    TextView title;
    TextView shortDescription;
    TextView description;
    Button viewMore;

    private Context mContext;
    private FB_EventHost mData;

    private OnEventHostItemViewMoreClick callback;

    public EventHostItem(Context context) {
        this(context, null);
    }

    public EventHostItem(final Context context, AttributeSet attrs) {
        super(context, attrs);

        mContext = context;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.layout_event_host_item, this, true);

        bindingControls();
        setupControlEvents();
        init();
    }

    public void setOnEventHostItemViewMoreClickListenter(OnEventHostItemViewMoreClick callback) {
        this.callback = callback;
    }

    void bindingControls() {
        image = (ImageView) findViewById(R.id.iv_image);
        title = (TextView) findViewById(R.id.tv_title);
        shortDescription = (TextView) findViewById(R.id.tv_short_description);
        description = (TextView) findViewById(R.id.tv_description);
        viewMore = (Button) findViewById(R.id.btn_view_more);
    }

    void setupControlEvents() {
        viewMore.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                EventHostItem.this.callback.onViewMoreClick();
            }
        });
    }

    void init() {
        //TODO
    }

    public void setData(FB_EventHost data) {
        mData = data;

        if (data.profilePhotoURL.isEmpty()) {
            image.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.placeholder_image));
        }
        else {
            Picasso.with(mContext).load(data.profilePhotoURL).into(image);
        }

        title.setText(data.displayName);
        shortDescription.setText(data.fullName);
        description.setText(data.hostDescription);
    }
}
