package dgapp.baggaarm.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.google.gson.Gson;

import java.util.List;
import java.util.Locale;

import dgapp.baggaarm.R;
import dgapp.baggaarm.service.model.firebase.FB_User;

import static android.content.Context.MODE_PRIVATE;

public class Utils {
    public static class GeoPoint {
        public double latitude;
        public double longitude;

        public GeoPoint(double latitude, double longitude) {
            this.latitude = latitude;
            this.longitude = longitude;
        }
    }

    public static void hideKeyboard(@NonNull AppCompatActivity activity) {
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public static void closeKeyboard(Context c, IBinder windowToken) {
        InputMethodManager mgr = (InputMethodManager) c.getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(windowToken, 0);
    }

    public static void showErrorDialog(Context context, String message) {
        final AlertDialog dialog = new AlertDialog.Builder(context)
                .setTitle(R.string.button_error)
                .setMessage(message)
                .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    public static void showDialog(Context context, String title, String message) {
        final AlertDialog dialog = new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create();
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public static String numberToVND(String input) {
        char[] chars = input.toCharArray();

        int count = chars.length;
        int checkPoint = 0;
        String returnValue = "";

        for (int i = count - 1; i >= 0; i--) {
            checkPoint++;
            returnValue += chars[i];
            if (checkPoint == 3) {
                returnValue += ".";
                checkPoint = 0;
            }
        }

        return new StringBuffer(returnValue).reverse().toString();
    }

    private static final String APP_PREFS_NAME = "PLANT_FACTORY_PREFS_NAME";
    private static final String APP_USER_SAVED_PREFS = "APP_USER_SAVED_PREFS";
    public static void savePreference(Context context, String key, String data) {
        SharedPreferences.Editor editor = context.getSharedPreferences(APP_PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString(key, data);
        editor.commit();
    }

    public static String getPreference(Context context, String key) {
        SharedPreferences prefs = context.getSharedPreferences(APP_PREFS_NAME, MODE_PRIVATE);
        String restoredText = prefs.getString(key, "");

        if (restoredText.trim().isEmpty()) {
            return null;
        }

        return restoredText;
    }

    public static void saveUser(Context context, FB_User user) {
        Gson gson = new Gson();
        String userString = gson.toJson(user);
        savePreference(context, APP_USER_SAVED_PREFS, userString);
    }

    public static FB_User getSavedUser(Context context) {
        String userString = getPreference(context, APP_USER_SAVED_PREFS);
        if (userString == null) {
            return null;
        }

        Gson gson = new Gson();
        FB_User user = gson.fromJson(userString, FB_User.class);
        return user;
    }
}
