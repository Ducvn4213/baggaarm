package dgapp.baggaarm.utils;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import dgapp.baggaarm.R;

public class WebViewActivity extends BaseActivity {

    ImageButton mClose;
    TextView mTitle;
    WebView mWebView;
    ProgressBar mProgress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);

        bindingControls();
        setupControlEvents();
        init();
    }

    void bindingControls() {
        mWebView = (WebView) findViewById(R.id.wv_web_view);
        mProgress = (ProgressBar) findViewById(R.id.pb_progress);
        mClose = (ImageButton) findViewById(R.id.ib_close);
        mTitle = (TextView) findViewById(R.id.tv_title);
    }

    void setupControlEvents() {
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mProgress.setVisibility(View.GONE);
            }
        });

        mClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WebViewActivity.this.finish();
            }
        });
    }

    void init() {
        String url = getIntent().getStringExtra("url");
        String title = getIntent().getStringExtra("title");
        mWebView.loadUrl(url);
        mTitle.setText(title);
    }
}
