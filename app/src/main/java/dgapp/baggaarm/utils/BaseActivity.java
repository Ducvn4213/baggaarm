package dgapp.baggaarm.utils;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import dgapp.baggaarm.R;

public class BaseActivity extends AppCompatActivity {

    private static final int PERMISSIONS_CALENDAR_REQUEST = 5487;

    @Override
    protected void onResume() {
        super.onResume();
        Utils.hideKeyboard(BaseActivity.this);
    }

    ProgressDialog mProgressDialog;

    protected void showLoading() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(BaseActivity.this);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage(getString(R.string.progress_bar_message));
        }

        mProgressDialog.show();
    }

    protected void hideLoading() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

    protected void showErrorDialog(String message) {
        Utils.showErrorDialog(BaseActivity.this, message);
    }

    protected void showDialog(String title, String message) {
        Utils.showDialog(BaseActivity.this, title, message);
    }

    protected void getCalendarPersmissionAndContinue() {
        checkCalendarPermission();
    }

    private void checkCalendarPermission() {
        List<String> permissions = new ArrayList<>();
        if (ContextCompat.checkSelfPermission(BaseActivity.this, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.WRITE_CALENDAR);
        }

        if (ContextCompat.checkSelfPermission(BaseActivity.this, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.READ_CALENDAR);
        }

        if (permissions.size() == 0) {
            doContinue();
            return;
        }

        String[] perArr = new String[permissions.size()];
        perArr = permissions.toArray(perArr);
        ActivityCompat.requestPermissions(BaseActivity.this,
        perArr,
        PERMISSIONS_CALENDAR_REQUEST);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSIONS_CALENDAR_REQUEST) {
            if (grantResults.length > 0) {
                boolean isGrant = true;
                for (int gr : grantResults) {
                    if (gr != PackageManager.PERMISSION_GRANTED) {
                        isGrant = false;
                    }
                }

                if (isGrant) {
                    doContinue();
                }
                else {
                    showErrorDialog(getString(R.string.dialog_message_missing_calendar_permission));
                    cantContinue();
                }
            }
            else {
                showErrorDialog(getString(R.string.dialog_message_missing_calendar_permission));
                cantContinue();
            }
        }
    }

    protected void doContinue() {
        //TODO
    }

    protected void cantContinue() {
        //TODO
    }

//    protected void getCurrentLocationAndContinue() {
//        checkLocationPermission();
//    }
//
//    private void checkLocationPermission() {
//        List<String> permissions = new ArrayList<>();
//        if (ContextCompat.checkSelfPermission(BaseActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
//        }
//        else {
//            doGetCurrentLocation();
//            return;
//        }
//
//        if (ContextCompat.checkSelfPermission(BaseActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
//        }
//        else {
//            doGetCurrentLocation();
//            return;
//        }
//
//        String[] perArr = new String[permissions.size()];
//        perArr = permissions.toArray(perArr);
//        ActivityCompat.requestPermissions(BaseActivity.this,
//                perArr,
//                Config.PERMISSIONS_LOCATION_REQUEST);
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        if (requestCode == Config.PERMISSIONS_LOCATION_REQUEST) {
//            if (grantResults.length > 0) {
//                boolean isGrant = false;
//                for (int gr : grantResults) {
//                    if (gr == PackageManager.PERMISSION_GRANTED) {
//                        isGrant = true;
//                        break;
//                    }
//                }
//
//                if (isGrant) {
//                    doGetCurrentLocation();
//                }
//                else {
//                    showErrorDialog(getString(R.string.dialog_message_missing_location_permission));
//                    afterGetCurrentLocationFail();
//                }
//            }
//            else {
//                showErrorDialog(getString(R.string.dialog_message_missing_location_permission));
//                afterGetCurrentLocationFail();
//            }
//        }
//    }
//
//    private void doGetCurrentLocation() {
//        TrackerGPS mTrackerGPS = new TrackerGPS(BaseActivity.this);
//        if(mTrackerGPS.canGetLocation()){
//
//            double longitude = mTrackerGPS.getLongitude();
//            double latitude = mTrackerGPS.getLatitude();
//
//            if (longitude == 0 && latitude == 0) {
//                doGetCurrentLocation();
//                return;
//            }
//
//            afterGotCurrentLocation(latitude, longitude);
//        }
//        else
//        {
//            mTrackerGPS.showSettingsAlert();
//        }
//    }
//
//    protected void afterGotCurrentLocation(double lat, double lon) {
//        //TODO
//    }
//
//    protected void afterGetCurrentLocationFail() {
//        //TODO
//    }
}
